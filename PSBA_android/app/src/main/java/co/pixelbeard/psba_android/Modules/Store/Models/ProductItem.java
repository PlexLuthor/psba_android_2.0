package co.pixelbeard.psba_android.Modules.Store.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by AE1 on 8/27/17.
 */

public class ProductItem implements Parcelable {

    public String name;
    public String details;
    public int points;
    public String imageUrl;

    public ProductItem() {
        //empty constr
    }

    public ProductItem(String name, String details, int points, String imageUrl) {
        this.name = name;
        this.details = details;
        this.points = points;
        this.imageUrl = imageUrl;
    }

    protected ProductItem(Parcel in) {
        name = in.readString();
        details = in.readString();
        points = in.readInt();
        imageUrl = in.readString();
    }

    public static final Creator<ProductItem> CREATOR = new Creator<ProductItem>() {
        @Override
        public ProductItem createFromParcel(Parcel in) {
            return new ProductItem(in);
        }

        @Override
        public ProductItem[] newArray(int size) {
            return new ProductItem[size];
        }
    };

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(details);
        dest.writeInt(points);
        dest.writeString(imageUrl);
    }
}
