package co.pixelbeard.psba_android.Modules.Creds.Acts;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.pixelbeard.psba_android.HQ;
import co.pixelbeard.psba_android.Modules.Creds.Adapters.BranchAdapter;
import co.pixelbeard.psba_android.Modules.Creds.Adapters.CompanyAdapter;
import co.pixelbeard.psba_android.Modules.Creds.Adapters.ExtrasAdapter;
import co.pixelbeard.psba_android.Modules.Creds.Models.Branch;
import co.pixelbeard.psba_android.Modules.Creds.Models.ExtraOption;
import co.pixelbeard.psba_android.R;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class OptionsActivity extends AppCompatActivity implements CompanyAdapter.ItemClickCallback, BranchAdapter.ItemClickCallback {

    private Button buttonBack;
    private SearchView searchStore;
    private RecyclerView recycStore;
    private RecyclerView recycBranch;
    private TextView textSearchStoreTitle;
    private TextView textHint;

    private RelativeLayout relayNodeStore;
    private View relayNodeStoreCircleHolder;
    private TextView textNodeStoreIcon;
    private TextView textNodeStoreInfo;

    private RelativeLayout relayNodeBranch;
    private View relayNodeBranchCircleHolder;
    private TextView textNodeBranchIcon;
    private TextView textNodeBranchInfo;

    private RelativeLayout relayOptionsAddress;
    private EditText editTextAddressOne;
    private EditText editTextAddressTwo;
    private EditText editTextAddressThree;
    private EditText editTextAddressFour;
    private EditText editTextAddressFive;
    private EditText editTextAddressSix;
    private Button buttonSaveAddress;

    private RelativeLayout relayOptionsExtras;
    private RecyclerView recycExtras;
    private Button buttonSaveExtras;
    private JSONArray optionsList;
    private ArrayList optionsArrayList;

    private boolean onBackOverride = false;

    CompanyAdapter storeAdapter;
    BranchAdapter branchAdapter;
    ExtrasAdapter extrasAdapter;

    private static int widthPixels = 0;
    private static int heightPixels = 0;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);

        // pixels, dpi //
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        heightPixels = metrics.heightPixels;
        widthPixels = metrics.widthPixels;


        findViews();
        setCustomFont();

        setUpOptionsAddressRelay();

        List<String> companyNames = HQ.getInstance().getCompanyNameList();
        List<String> companyImages = HQ.getInstance().getCompanyImageList();
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.options_store_recyc);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        storeAdapter = new CompanyAdapter(this, companyNames, companyImages);
        recyclerView.setAdapter(storeAdapter);
        storeAdapter.setItemClickCallback(this);

        setEditTextListeners();

        changeNodeToSelected("store", "off");
        changeNodeToSelected("branch", "off");
        textNodeStoreInfo.setText("Company");
        textNodeBranchInfo.setText("Branch");

        relayNodeStore.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                if (recycExtras.getX() == 0) {
                    return;
                }
                changeNodeToSelected("store", "off");
                changeNodeToSelected("branch", "off");
                textNodeStoreInfo.setText("Company");
                textNodeBranchInfo.setText("Branch");
                recycStore.animate().translationX(0);
                recycBranch.animate().translationX(widthPixels);
                relayOptionsAddress.animate().translationX(widthPixels);
                relayOptionsExtras.animate().translationX(widthPixels);
            }
        });

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                onBackOverride = false;
                HQ.getInstance().setUserCompanyInfoToNil();
                HQ.getInstance().positionExtrasAdapterMain = 0;
                HQ.getInstance().dropChosen = false;
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.slide_out_down);
            }
        });
        buttonSaveAddress.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                if (optionsList.length() == 0) {
                    onBackOverride = true;
                    HQ.getInstance().positionExtrasAdapterMain = 0;
                    HQ.getInstance().dropChosen = false;
                    onBackPressed();
                    overridePendingTransition(R.anim.fade_in, R.anim.slide_out_down);
                } else {
                    relayOptionsExtras.animate().translationX(0);
                }

                textNodeBranchInfo.setText(editTextAddressOne.getText().toString());
                changeNodeToSelected("branch", "on");
            }
        });
        buttonSaveExtras.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                onBackOverride = true;
                HQ.getInstance().positionExtrasAdapterMain = 0;
                HQ.getInstance().dropChosen = false;
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.slide_out_down);
            }
        });

    }


    public void findViews() {
        buttonBack = (Button) findViewById(R.id.options_store_back_button);
        textSearchStoreTitle = (TextView) findViewById(R.id.options_store_title);

        relayNodeStore = (RelativeLayout) findViewById(R.id.options_node_store);
        relayNodeStoreCircleHolder = findViewById(R.id.options_node_store_circle_holder);
        textNodeStoreIcon = (TextView) findViewById(R.id.options_node_store_icon);
        textNodeStoreInfo = (TextView) findViewById(R.id.options_node_store_text);

        relayNodeBranch = (RelativeLayout) findViewById(R.id.options_node_branch);
        relayNodeBranchCircleHolder = findViewById(R.id.options_node_branch_circle_holder);
        textNodeBranchIcon = (TextView) findViewById(R.id.options_node_branch_icon);
        textNodeBranchInfo = (TextView) findViewById(R.id.options_node_branch_text);

        relayOptionsAddress = (RelativeLayout) findViewById(R.id.options_relay_address);
        editTextAddressOne = (EditText) findViewById(R.id.options_edit_text_address_one);
        editTextAddressTwo = (EditText) findViewById(R.id.options_edit_text_address_two);
        editTextAddressThree = (EditText) findViewById(R.id.options_edit_text_address_three);
        editTextAddressFour = (EditText) findViewById(R.id.options_edit_text_address_four);
        editTextAddressFive = (EditText) findViewById(R.id.options_edit_text_address_five);
        editTextAddressSix = (EditText) findViewById(R.id.options_edit_text_address_six);
        buttonSaveAddress = (Button) findViewById(R.id.options_button_save_address);

        relayOptionsExtras = (RelativeLayout) findViewById(R.id.options_relay_extras);
        buttonSaveExtras = (Button) findViewById(R.id.options_button_save_extras);


        recycStore = (RecyclerView) findViewById(R.id.options_store_recyc);
        recycBranch = (RecyclerView) findViewById(R.id.options_branch_recyc);
        recycExtras = (RecyclerView) findViewById(R.id.options_extras_recyc);


        recycBranch.setX(widthPixels);
        relayOptionsAddress.setX(widthPixels);
        relayOptionsExtras.setX(widthPixels);
    }

    public void setCustomFont() {
        buttonBack.setTypeface(HQ.getInstance().fontAwesome);
        textSearchStoreTitle.setTypeface(HQ.getInstance().sstFontBold);

        textNodeStoreIcon.setTypeface(HQ.getInstance().fontAwesome);
        textNodeStoreInfo.setTypeface(HQ.getInstance().sstFontBold);
        textNodeBranchIcon.setTypeface(HQ.getInstance().fontAwesome);
        textNodeBranchInfo.setTypeface(HQ.getInstance().sstFontBold);

        editTextAddressOne.setTypeface(HQ.getInstance().sstFontRoman);
        editTextAddressTwo.setTypeface(HQ.getInstance().sstFontRoman);
        editTextAddressThree.setTypeface(HQ.getInstance().sstFontRoman);
        editTextAddressFour.setTypeface(HQ.getInstance().sstFontRoman);
        editTextAddressFive.setTypeface(HQ.getInstance().sstFontRoman);
        editTextAddressSix.setTypeface(HQ.getInstance().sstFontRoman);

        buttonSaveAddress.setTypeface(HQ.getInstance().sstFontRoman);

    }

    @Override public void onItemClick(int pos) {
        HQ.getInstance().theUser.setCompanyName(HQ.getInstance().companies.get(pos).getCompanyName());
        HQ.getInstance().theUser.setCompanyId(HQ.getInstance().companies.get(pos).getCompanyId());
        Log.v("XYZ_FUTURE::: ", "on item click user changed company name to: " + HQ.getInstance().theUser.getCompanyName() + " : and company id to: " + HQ.getInstance().theUser.getCompanyId());
        changeNodeToSelected("store", "on");
        textNodeStoreInfo.setText(HQ.getInstance().theUser.getCompanyName());
        recycStore.animate().translationX(-widthPixels);
        recycBranch.animate().translationX(0);
        psbaAPIGetCompanyExtrasByID(this);
    }

    public void changeNodeToSelected(String node, String state) {
        RelativeLayout relay = null;
        View circle = null;
        TextView icon = null;
        TextView info = null;
        if (node == "store") {
            relay = relayNodeStore;
            circle = relayNodeStoreCircleHolder;
            icon = textNodeStoreIcon;
            info = textNodeStoreInfo;
        } else if (node == "branch") {
            relay = relayNodeBranch;
            circle = relayNodeBranchCircleHolder;
            icon = textNodeBranchIcon;
            info = textNodeBranchInfo;
        }

        if (state == "on") {
            relay.setBackground(getResources().getDrawable(R.drawable.node_shape));
            relay.setClickable(true);
            circle.setBackground(getResources().getDrawable(R.drawable.node_shape_dark_blue));
            icon.setTextColor(getResources().getColor(R.color.colorWhite));
            info.setTextColor(getResources().getColor(R.color.colorWhite));
            icon.setText(getResources().getString(R.string.font_awesome_check));
        } else if (state == "off") {
            relay.setClickable(false);
            relay.setBackground(getResources().getDrawable(R.drawable.node_shape_gray));
            circle.setBackground(getResources().getDrawable(R.drawable.node_shape_gray_light));
            icon.setTextColor(getResources().getColor(R.color.colorWhiteSmoke));
            info.setTextColor(getResources().getColor(R.color.colorMagnesium));
            icon.setText("");
        }

    }

    public void psbaAPIGetCompanyExtrasByID(final Context context) {

        final String coId = HQ.getInstance().theUser.getCompanyId();
        Log.v("XYZ_FUTURE::: ", "coId is: " + coId);

        AsyncTask asyncTask = new AsyncTask() {
            @Override protected Object doInBackground(Object[] params) {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://psba.pixelbeard.co/api/companies/" + coId)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    JSONObject JSONResponse = new JSONObject(response.body().string());

                    if (JSONResponse.getString("success").matches("true")) {

                        // branches retrieval //
                        JSONArray branchList = JSONResponse.getJSONArray("branches");

                        HQ.getInstance().branches.clear();
                        HQ.getInstance().branches.add(new Branch("Not Listed", "Not Listed"));

                        for (int i = 0; i < branchList.length(); i++) {
                            JSONObject individualBranch = (JSONObject) branchList.get(i);
                            HashMap<String, String> singleBranchHash = HQ.getInstance().jsonToMap(individualBranch.toString());
                            Branch br = new Branch();
                            br.setBranchName(singleBranchHash.get("name"));
                            br.setBranchId(singleBranchHash.get("id"));
                            HQ.getInstance().branches.add(br);
                        }

                        // extras retrieval //
                        HQ.getInstance().extraOptions.clear();

                        optionsList = JSONResponse.getJSONArray("options");
                        for (int i = 0; i < optionsList.length(); i++) {
                            ExtraOption extraOption = new ExtraOption();
                            JSONObject individualOption = (JSONObject) optionsList.get(i);
                            HashMap<String, String> singleOptionHash = HQ.getInstance().jsonToMap(individualOption.toString());
                            extraOption.setType(singleOptionHash.get("type"));
                            extraOption.setIdentifier(singleOptionHash.get("identifier"));

                            JSONArray singleOptionValues = individualOption.getJSONArray("values");

                            ArrayList<String> listdata = new ArrayList<>();
                            if (singleOptionValues != null) {
                                for (int z = 0; z < singleOptionValues.length(); z++){
                                    listdata.add(singleOptionValues.getString(z));
                                }
                            }
                            extraOption.setValues(listdata);
                            HQ.getInstance().extraOptions.add(extraOption);
                        }
                        return "success";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return "fail";
            }
            @Override protected void onPostExecute(Object o) {
                if (o.equals("success")) {

                    if (HQ.getInstance().branches.size() <= 1) {
                        Log.v("XYZ_FUTURE::: ", "AUTOMATICALLY GO TO BRANCH ADDRESS INPUT");
                        recycBranch.setX(-widthPixels);
                        relayOptionsAddress.animate().translationX(0);
                    }

                    RecyclerView recyclerView2 = (RecyclerView) findViewById(R.id.options_branch_recyc);
                    recyclerView2.setLayoutManager(new LinearLayoutManager(context));
                    branchAdapter = new BranchAdapter(context, HQ.getInstance().getBranchNameList());
                    recyclerView2.setAdapter(branchAdapter);
                    branchAdapter.setItemClickCallback(new BranchAdapter.ItemClickCallback() {
                        @Override public void onItemClick(int pos) {
                            if (pos != 0) {
                                HQ.getInstance().theUser.setBranchName(HQ.getInstance().branches.get(pos).getBranchName());
                                HQ.getInstance().theUser.setBranch_id(HQ.getInstance().branches.get(pos).getBranchId());
                                changeNodeToSelected("branch", "on");
                                textNodeBranchInfo.setText(HQ.getInstance().theUser.getBranchName());
                                if (optionsList.length() == 0) {
                                    onBackOverride = true;
                                    HQ.getInstance().positionExtrasAdapterMain = 0;
                                    HQ.getInstance().dropChosen = false;
                                    onBackPressed();
                                    overridePendingTransition(R.anim.fade_in, R.anim.slide_out_down);
                                } else {
                                    relayOptionsExtras.animate().translationX(0);
                                }
                            } else {
                                relayOptionsAddress.animate().translationX(0);
                            }
                            recycBranch.animate().translationX(-widthPixels);
                        }
                    });

                    RecyclerView recyclerView3 = (RecyclerView) findViewById(R.id.options_extras_recyc);
                    recyclerView3.setLayoutManager(new LinearLayoutManager(context));
                    extrasAdapter = new ExtrasAdapter(context, HQ.getInstance().extraOptions);
                    recyclerView3.setAdapter(extrasAdapter);
                    extrasAdapter.setItemClickCallback(new ExtrasAdapter.ItemClickCallback() {
                        @Override public void onItemClick(int p) {
                            Log.v("XYZ_FUTURE::: ", "onItemClick extrasAdapter p: " + p);
                        }
                    });
                }
            }
        };
        asyncTask.execute();
    }


    public void setUpOptionsAddressRelay() {
        editTextAddressOne.getLayoutParams().width = (int) (widthPixels * 0.5) - (dpToPx(40));
        editTextAddressOne.requestLayout();
        editTextAddressTwo.getLayoutParams().width = (int) (widthPixels * 0.5) - (dpToPx(40));
        editTextAddressTwo.requestLayout();
        editTextAddressThree.getLayoutParams().width = (int) (widthPixels * 0.5) - (dpToPx(40));
        editTextAddressThree.requestLayout();
        editTextAddressFour.getLayoutParams().width = (int) (widthPixels * 0.5) - (dpToPx(40));
        editTextAddressFour.requestLayout();
        editTextAddressFive.getLayoutParams().width = (int) (widthPixels * 0.5) - (dpToPx(40));
        editTextAddressFive.requestLayout();
        editTextAddressSix.getLayoutParams().width = (int) (widthPixels * 0.5) - (dpToPx(40));
        editTextAddressSix.requestLayout();
//        editTextAddressSix.setY(dpToPx());

        editTextAddressOne.setX(getResources().getDimension(R.dimen.alpha_margin));
        editTextAddressThree.setX(getResources().getDimension(R.dimen.alpha_margin));
        editTextAddressFive.setX(getResources().getDimension(R.dimen.alpha_margin));

        editTextAddressTwo.setX(widthPixels * 0.5f + dpToPx(8));
        editTextAddressFour.setX(widthPixels * 0.5f + dpToPx(8));
        editTextAddressSix.setX(widthPixels * 0.5f + dpToPx(8));
    }

    public void setEditTextListeners() {
        editTextAddressOne.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!editTextAddressOne.getText().toString().matches("")) {
                        HQ.getInstance().theUser.setBranchName(editTextAddressOne.getText().toString());
                        Log.v("XYZ_FUTURE::: ", "user changed company name to: " + HQ.getInstance().theUser.getCompanyName());
                    }
                }
            }
        });
        editTextAddressTwo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!editTextAddressTwo.getText().toString().matches("")) {
                        HQ.getInstance().theUser.setAddress1(editTextAddressTwo.getText().toString());
                        Log.v("XYZ_FUTURE::: ", "user changed address 1 name to: " + HQ.getInstance().theUser.getAddress1());
                    }
                }
            }
        });
        editTextAddressThree.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!editTextAddressThree.getText().toString().matches("")) {
                        HQ.getInstance().theUser.setAddress2(editTextAddressThree.getText().toString());
                        Log.v("XYZ_FUTURE::: ", "user changed address 2 name to: " + HQ.getInstance().theUser.getAddress2());
                    }
                }
            }
        });
        editTextAddressFour.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!editTextAddressFour.getText().toString().matches("")) {
                        HQ.getInstance().theUser.setTown(editTextAddressFour.getText().toString());
                        Log.v("XYZ_FUTURE::: ", "user changed city name to: " + HQ.getInstance().theUser.getTown());
                    }
                }
            }
        });
        editTextAddressFive.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!editTextAddressFive.getText().toString().matches("")) {
                        HQ.getInstance().theUser.setCountry(editTextAddressFive.getText().toString());
                        Log.v("XYZ_FUTURE::: ", "user changed county name to: " + HQ.getInstance().theUser.getCountry());
                    }
                }
            }
        });
        editTextAddressSix.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!editTextAddressSix.getText().toString().matches("")) {
                        HQ.getInstance().theUser.setPostCode(editTextAddressSix.getText().toString());
                        Log.v("XYZ_FUTURE::: ", "user changed postcode name to: " + HQ.getInstance().theUser.getPostCode());
                    }
                }
            }
        });
    }

    @Override public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

//    @Override public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
//        Log.v("XYZ_FUTURE::: ", "onEditorAction");
//        long downTime = SystemClock.uptimeMillis();
//        long eventTime = SystemClock.uptimeMillis() + 100;
//        float x = 60.0f;
//        float y = 60.0f;
//        // List of meta states found here:     developer.android.com/reference/android/view/KeyEvent.html#getMetaState()
//        int metaState = 0;
//        MotionEvent motionEvent = MotionEvent.obtain(downTime, eventTime, MotionEvent.ACTION_DOWN, x, y, metaState);
//
//        // Dispatch touch event to view
//        this.dispatchTouchEvent(motionEvent);
//
//        return true;
//    }

    // wizard apprentices //
    private int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    private int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

//    @Override public void onBackPressed() {
//        super.onBackPressed();
//        if (onBackOverride == false) {
//            HQ.getInstance().setUserCompanyInfoToNil();
//        }
//        onBackOverride = false;
//        Intent intent = new Intent(OptionsActivity.this, CredentialsActivity.class);
//        startActivity(intent);
//        overridePendingTransition(R.anim.fade_in, R.anim.slide_out_down);
//    }
}
























