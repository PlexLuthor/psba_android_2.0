package co.pixelbeard.psba_android.Modules.Training.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by AE1 on 8/29/17.
 */

public class TrainingModule implements Parcelable {
    String id;
    String name;
    String category_id;
    String category_name;
    String category_order;
    String points;
    String version;
    String created_date;
    String modified_date;
    String status;
    String order;
    String featured;
    String image;
    int userProgressRefTotalQuestions;
    int userProgressRefCorrectAnswers;
    int userProgressRefQuestionsAnswered;
    int userProgressRefTotalAttempts;
    boolean userProgressRefPassed;

    public TrainingModule() {

    }

    public TrainingModule(String name, String image) {
        this.name = name;
        this.image = "https://c1.staticflickr.com/1/188/417924629_6832e79c98_z.jpg?zz=1";
    }

    public TrainingModule(String id, String name, String category_id, String category_name, String category_order, String points, String version, String created_date, String modified_date, String status, String order, String featured,
                          String image, int userProgressRefTotalQuestions, int userProgressRefCorrectAnswers, int userProgressRefQuestionsAnswered, int userProgressRefTotalAttempts, boolean userProgressRefPassed) {
        this.id = id;
        this.name = name;
        this.category_id = category_id;
        this.category_name = category_name;
        this.category_order = category_order;
        this.points = points;
        this.version = version;
        this.created_date = created_date;
        this.modified_date = modified_date;
        this.status = status;
        this.order = order;
        this.featured = featured;
        this.image = image;
        this.userProgressRefTotalQuestions = userProgressRefTotalQuestions;
        this.userProgressRefCorrectAnswers = userProgressRefCorrectAnswers;
        this.userProgressRefQuestionsAnswered = userProgressRefQuestionsAnswered;
        this.userProgressRefTotalAttempts = userProgressRefTotalAttempts;
        this.userProgressRefPassed = userProgressRefPassed;
    }


    protected TrainingModule(Parcel in) {
        id = in.readString();
        name = in.readString();
        category_id = in.readString();
        category_name = in.readString();
        category_order = in.readString();
        points = in.readString();
        version = in.readString();
        created_date = in.readString();
        modified_date = in.readString();
        status = in.readString();
        order = in.readString();
        featured = in.readString();
        image = in.readString();
    }

    public static final Parcelable.Creator<TrainingModule> CREATOR = new Parcelable.Creator<TrainingModule>() {
        @Override public TrainingModule createFromParcel(Parcel in) {
            return new TrainingModule(in);
        }
        @Override public TrainingModule[] newArray(int size) {
            return new TrainingModule[size];
        }
    };

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(category_id);
        dest.writeString(category_name);
        dest.writeString(category_order);
        dest.writeString(points);
        dest.writeString(version);
        dest.writeString(created_date);
        dest.writeString(modified_date);
        dest.writeString(status);
        dest.writeString(order);
        dest.writeString(featured);
        dest.writeString(image);
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCategory_order() {
        return category_order;
    }

    public void setCategory_order(String category_order) {
        this.category_order = category_order;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getModified_date() {
        return modified_date;
    }

    public void setModified_date(String modified_date) {
        this.modified_date = modified_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getFeatured() {
        return featured;
    }

    public void setFeatured(String featured) {
        this.featured = featured;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getUserProgressRefTotalQuestions() {
        return userProgressRefTotalQuestions;
    }

    public void setUserProgressRefTotalQuestions(int userProgressRefTotalQuestions) {
        this.userProgressRefTotalQuestions = userProgressRefTotalQuestions;
    }

    public int getUserProgressRefCorrectAnswers() {
        return userProgressRefCorrectAnswers;
    }

    public void setUserProgressRefCorrectAnswers(int userProgressRefCorrectAnswers) {
        this.userProgressRefCorrectAnswers = userProgressRefCorrectAnswers;
    }

    public int getUserProgressRefQuestionsAnswered() {
        return userProgressRefQuestionsAnswered;
    }

    public void setUserProgressRefQuestionsAnswered(int userProgressRefQuestionsAnswered) {
        this.userProgressRefQuestionsAnswered = userProgressRefQuestionsAnswered;
    }


    public int getUserProgressRefTotalAttempts() {
        return userProgressRefTotalAttempts;
    }

    public void setUserProgressRefTotalAttempts(int userProgressRefTotalAttempts) {
        this.userProgressRefTotalAttempts = userProgressRefTotalAttempts;
    }

    public boolean isUserProgressRefPassed() {
        return userProgressRefPassed;
    }

    public void setUserProgressRefPassed(boolean userProgressRefPassed) {
        this.userProgressRefPassed = userProgressRefPassed;
    }

}
