package co.pixelbeard.psba_android.Modules.Creds.Models;

import java.util.List;

/**
 * Created by aaroneckhart on 8/9/17.
 */

public class ExtraOption {
    String type;
    String identifier;
    List values;

    public ExtraOption(String type, String identifier, List values) {
        this.type = type;
        this.identifier = identifier;
        this.values = values;
    }

    public ExtraOption() {

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public List getValues() {
        return values;
    }

    public void setValues(List values) {
        this.values = values;
    }

}
