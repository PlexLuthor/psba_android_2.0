package co.pixelbeard.psba_android;

import android.widget.ImageView;

import co.pixelbeard.psba_android.Modules.Store.Models.ProductItem;

/**
 * Created by AE1 on 8/27/17.
 */

public interface ProductItemClickListener {
    void onProductItemClick(int pos, ProductItem productItem, ImageView shareImageView);
}
