package co.pixelbeard.psba_android.Modules.Store.Frags;

import android.app.FragmentManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import co.pixelbeard.psba_android.Modules.Store.Models.ProductItem;
import co.pixelbeard.psba_android.R;

public class ProductFragment extends Fragment {

    private ViewPager viewPagerProduct;
    private View pagerIndicator;
    private PagerAdapter pagerAdapterProductMain;
    private ArrayList<String> IMAGES = new ArrayList<>();

    private TextView textNavTitle;
    private TextView textNavSubtitle;
    private Button buttonLeftNav;
    private Button buttonInsideRightNav;
    private Button buttonRightNav;

    private static final String EXTRA_PRODUCT_ITEM = "product_item";
    private static final String EXTRA_TRANSITION_NAME = "transition_name";

//    private static final String EXTRA_INITIAL_ITEM_POS = "initial_item_pos";
//    private static final String EXTRA_PRODUCT_ITEMS = "product_items";

    private NestedScrollView scrollProductMain;

    private static int widthPixels = 0;
    private static int heightPixels = 0;

    private ProductFragment.FragmentListener mListener;
    public ProductFragment() {
        // Required empty public constructor
    }

    public static ProductFragment newInstance(int width, int height, ProductItem productItem, String transitionName) {
        ProductFragment fragment = new ProductFragment();
        Bundle args = new Bundle();
        args.putInt("width", width);
        args.putInt("height", height);
        args.putParcelable(EXTRA_PRODUCT_ITEM, productItem);
        args.putString(EXTRA_TRANSITION_NAME, transitionName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            widthPixels = getArguments().getInt("width");
            heightPixels = getArguments().getInt("height");
            Log.v("XYZ", "SMF widthPix = " + widthPixels + " ::: heightPix = " + heightPixels);
        }

//        IMAGES.add("https://c1.staticflickr.com/9/8796/17158681740_a6caa5099f_z.jpg");
//        IMAGES.add("https://c2.staticflickr.com/4/3774/9377370000_6a57d1cfec_z.jpg");
//        IMAGES.add("https://c1.staticflickr.com/5/4010/4210875342_7cb06a9b62_z.jpg?zz=1");

        getActivity().postponeEnterTransition();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSharedElementEnterTransition(TransitionInflater.from(getContext()).inflateTransition(android.R.transition.move));
        }
//        setSharedElementReturnTransition(null);
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product, container, false);
        findViews(view);
        setUpNavButtons();


        return view;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ProductItem animalItem = getArguments().getParcelable(EXTRA_PRODUCT_ITEM);
        String transitionName = getArguments().getString(EXTRA_TRANSITION_NAME);

//        TextView detailTextView = (TextView) view.findViewById(R.id.animal_detail_text);
//        detailTextView.setText(animalItem.detail);

        ImageView imageView = (ImageView) view.findViewById(R.id.image_view_product_detail_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageView.setTransitionName(transitionName);
        }

        Picasso.with(getContext())
                .load(animalItem.imageUrl)
                .noFade()
                .fit()
                .centerCrop()
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        startPostponedEnterTransition();
                    }

                    @Override
                    public void onError() {
                        startPostponedEnterTransition();
                    }
                });
    }


    public void onButtonPressed(String str1, String str2) {
        if (mListener != null) {
            mListener.removeTabBar();
        }
    }

    @Override public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof ProductFragment.FragmentListener))
            throw new AssertionError();
        mListener = (ProductFragment.FragmentListener) context;
        if (mListener != null) {
            mListener.removeTabBar();
        }
    }

    @Override public void onDetach() {
        super.onDetach();
        mListener = null;
        if (mListener != null) {
            mListener.displayTabBar();
        }
    }

    public interface FragmentListener {
        void onFragmentFinish(String testString1, String testString2);
        void removeTabBar();
        void displayTabBar();
    }

    public void findViews(View v) {

        textNavTitle = (TextView) v.findViewById(R.id.text_store_nav_bar_title);
        textNavSubtitle = (TextView) v.findViewById(R.id.text_store_nav_bar_sub_title);
        buttonLeftNav = (Button) v.findViewById(R.id.button_left_nav_bar);
        buttonInsideRightNav = (Button) v.findViewById(R.id.button_inside_right_nav_bar);
        buttonRightNav = (Button) v.findViewById(R.id.button_right_nav_bar);

        pagerIndicator = v.findViewById(R.id.pager_indicator_mover);


        viewPagerProduct = (ViewPager) v.findViewById(R.id.pager_product_main);
//        pagerAdapterProductMain = new ProductMainPagerAdapter(getActivity(), IMAGES);
//        viewPagerProduct.setAdapter(pagerAdapterProductMain);
//        viewPagerProduct.setCurrentItem(1);

//        supportPostponeEnterTransition();
//
//        Bundle extras = getIntent().getExtras();
//        ProductItem productItem = extras.getParcelable(StoreModuleFragment.EXTRA_PRODUCT_ITEM);
//
//        ImageView imageView = (ImageView) v.findViewById(R.id.image_view_product_detail_main);
//        TextView textView = (TextView) v.findViewById(R.id.text_product_name);
////        textView.setText(productItem.detail);
//
//        ImageView pagerImageView = (ImageView) v.findViewById(R.id.pager_image_view_product_main);
//
//        String imageUrl = productItem.imageUrl;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            String imageTransitionName = extras.getString(StoreModuleFragment.EXTRA_PRODUCT_IMAGE_TRANSITION_NAME);
//            imageView.setTransitionName(imageTransitionName);
//        }
//
//        Picasso.with(getActivity())
//                .load(imageUrl)
//                .noFade()
//                .fit()
//                .centerCrop()
//                .into(imageView, new Callback() {
//                    @Override
//                    public void onSuccess() {
//                        supportStartPostponedEnterTransition();
//                    }
//
//                    @Override
//                    public void onError() {
//                        supportStartPostponedEnterTransition();
//                    }
//                });
    }

    private void setUpNavButtons() {
//        buttonLeftNav.setOnClickListener(new View.OnClickListener() {
//            @Override public void onClick(View v) {
//                onBackPressed();
//            }
//        });
        buttonInsideRightNav.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {

            }
        });
        buttonRightNav.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {

            }
        });
    }

    public void onBackPressed() {
        FragmentManager fm = getActivity().getFragmentManager();
        fm.popBackStack();
        mListener.displayTabBar();
    }
}