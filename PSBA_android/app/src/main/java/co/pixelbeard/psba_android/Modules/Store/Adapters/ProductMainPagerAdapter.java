package co.pixelbeard.psba_android.Modules.Store.Adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import co.pixelbeard.psba_android.Modules.Store.Models.ProductItem;
import co.pixelbeard.psba_android.R;

/**
 * Created by AE1 on 8/28/17.
 */

public class ProductMainPagerAdapter extends FragmentStatePagerAdapter {
    private Context context;
    private ArrayList<String> IMAGES = new ArrayList<>();
    private ArrayList<ProductItem> productItems;

    public ProductMainPagerAdapter(FragmentManager fm, ArrayList<ProductItem> productItems) {
        super(fm);
        this.productItems = productItems;
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public void destroyItem(View arg0, int arg1, Object arg2) {
        ((ViewPager) arg0).removeView((View) arg2);
    }

    @Override
    public Fragment getItem(int position) {
        return null;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }


    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public Object instantiateItem(View collection, int position) {
        LayoutInflater inflater = (LayoutInflater) collection.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.pager_layout_product_main,null);
        ((ViewPager) collection).addView(view);
        final ImageView img = (ImageView) view.findViewById(R.id.pager_image_view_product_main);
        Picasso.with(context)
                .load(IMAGES.get(position))
                .placeholder(R.drawable.psba_logo_3x)
                .centerCrop()
                .fit()
                .into(img);
        return view;
    }
}
