package co.pixelbeard.psba_android.Modules.Creds.Models;

/**
 * Created by aaroneckhart on 7/26/17.
 */

public class Company {
    String companyId;
    String companyName;
    String companyOrder;
    String companyActive;
    String companyImage;

    public Company(String companyId, String companyName, String companyOrder, String companyActive, String companyImage) {
        this.companyId = companyId;
        this.companyName = companyName;
        this.companyOrder = companyOrder;
        this.companyActive = companyActive;
        this.companyImage = companyImage;
    }

    public Company() {

    }

    public void setCompanyId(String id) {
        this.companyId = id;
    }
    public void setCompanyName(String name) {
        this.companyName = name;
    }
    public void setCompanyOrder(String order) {
        this.companyOrder = order;
    }
    public void setCompanyActive(String active) {
        this.companyActive = active;
    }
    public void setCompanyImage(String image) {
        this.companyImage = image;
    }


    public String getCompanyId() {
        return this.companyId;
    }
    public String getCompanyName() {
        return this.companyName;
    }
    public String getCompanyOrder() {
        return this.companyOrder;
    }
    public String getCompanyActive() {
        return this.companyActive;
    }
    public String getCompanyImage() {
        return this.companyImage;
    }
}
