package co.pixelbeard.psba_android;

import android.widget.ImageView;

import co.pixelbeard.psba_android.Modules.Training.Models.TrainingModule;

/**
 * Created by AE1 on 8/29/17.
 */

public interface TrainingItemClickListener {
    void onTrainingItemClick(int pos, TrainingModule trainingItem, ImageView shareImageView, String recycFlag);
}