package co.pixelbeard.psba_android.Modules.Creds.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;
import org.joda.time.Minutes;

import java.util.Collections;
import java.util.List;

import co.pixelbeard.psba_android.R;
import co.pixelbeard.psba_android.HQ;
import co.pixelbeard.psba_android.Wizards.RoundedTransformation;

/**
 * Created by AE1 on 8/8/17.
 */

public class CompanyAdapter extends RecyclerView.Adapter<CompanyAdapter.CompanyHolder> {

    private LayoutInflater inflater;
    private String timeString;

    private List<String> nameData = Collections.emptyList();
    private List<String> imageData = Collections.emptyList();
    private LayoutInflater mInflater;

    private ItemClickCallback itemClickCallback;
    public interface ItemClickCallback {
        void onItemClick(int p);
    }

    public void setItemClickCallback (final ItemClickCallback itemClickCallback) {
        this.itemClickCallback = itemClickCallback;
    }

    public CompanyAdapter(Context context, List<String> data, List<String> imageData) {
        this.inflater.from(context);
        this.nameData = data;
        this.imageData = imageData;

    }
    @Override public CompanyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyc_element_company_search, parent, false);//inflater.inflate(R.layout.beep_list_item, parent, false);
        return new CompanyHolder(view);
    }
    @Override public void onBindViewHolder(final CompanyHolder holder, int position) {
        String companyName = nameData.get(position);
        holder.storeText.setText(companyName);
        Picasso.with(holder.storeImageView.getContext()).load(HQ.getInstance().theStoreLogoURL + imageData.get(position)).fit().centerInside().transform(new RoundedTransformation(16, 0)).into(holder.storeImageView);
    }
    @Override public int getItemCount() {
        if (HQ.getInstance().companies == null) {
            return 0;
        }
        return HQ.getInstance().companies.size();
    }
    public String timeFromBeepMinutes(String beepInceptionDate) {
        DateTime now = DateTime.now();
        DateTime beepTime = DateTime.parse(beepInceptionDate);
        int amountOfPastMins = Minutes.minutesBetween(beepTime, now).getMinutes();
        if (amountOfPastMins == 0) {
            timeString = "just now";
        } else if (amountOfPastMins == 1) {
            timeString = "1 min";
        } else if (amountOfPastMins < 60) {
            timeString = amountOfPastMins + " mins";
        } else if (amountOfPastMins < 120) {
            timeString = "1 hour";
        } else if (amountOfPastMins < 1440) {
            int adjustedHours = amountOfPastMins / 60;
            timeString = adjustedHours + " hours";
        } else if (amountOfPastMins < 2880) {
            timeString = "1 day";
        } else if (amountOfPastMins < 43200) {
            int adjustedDays = amountOfPastMins / 1440;
            timeString = adjustedDays + " days";
        } else if (amountOfPastMins < 525600) {
            int adjustedMonths = amountOfPastMins / 43200;
            timeString = adjustedMonths + " months";
        } else if (amountOfPastMins < 1051200) {
            timeString = "1 year";
        } else {
            timeString = "many moons ago";
        }
        return timeString;
    }

    public class CompanyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView storeText;
        public ImageView storeImageView;
        public RelativeLayout storeRowRelay;
        public CompanyHolder(View itemView) {
            super(itemView);
            storeRowRelay = (RelativeLayout) itemView.findViewById(R.id.recyc_element_store_row_relay);
            storeImageView = (ImageView) itemView.findViewById(R.id.recyc_element_store_image_view);
            storeText = (TextView) itemView.findViewById(R.id.recyc_element_store_text);
            itemView.setOnClickListener(this);

            storeText.setTypeface(HQ.getInstance().sstFontRoman);
            storeRowRelay.setOnClickListener(this);
        }
        @Override public void onClick(View v) {
            if (v.getId() == R.id.recyc_element_store_row_relay) {
                Log.v("XYZ", "recyc");
                itemClickCallback.onItemClick(getAdapterPosition());
            } else {
                Log.v("XYZ", "NOT recyc");
            }
        }
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }
    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
}

