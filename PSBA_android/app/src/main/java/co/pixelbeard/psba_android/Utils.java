package co.pixelbeard.psba_android;

import android.content.Context;

import java.util.ArrayList;

import co.pixelbeard.psba_android.Modules.Store.Models.ProductItem;

/**
 * Created by msc10 on 19/02/2017.
 */

public class Utils {

    public static ArrayList<ProductItem> generateProductItems(Context context) {
        ArrayList<ProductItem> productItems = new ArrayList<>();
        productItems.add(new ProductItem("Dog", "a dog", 100, "https://c1.staticflickr.com/1/188/417924629_6832e79c98_z.jpg?zz=1"));
        productItems.add(new ProductItem("Penguin", "a penguin", 100, "https://c1.staticflickr.com/9/8616/16237154608_c5489cae31_z.jpg"));
        productItems.add(new ProductItem("Eagle", "a eagle", 100, "https://c1.staticflickr.com/5/4010/4210875342_7cb06a9b62_z.jpg?zz=1"));
        productItems.add(new ProductItem("Rabbit", "a rabbit", 100, "https://c2.staticflickr.com/4/3285/2819978026_175072995a_z.jpg?zz=1"));
        productItems.add(new ProductItem("Dolphin", "a dolphin", 100, "https://c1.staticflickr.com/8/7619/16124006043_60bc4d8ca5_z.jpg"));
        productItems.add(new ProductItem("Snek", "a snek", 100, "https://c1.staticflickr.com/9/8796/17158681740_a6caa5099f_z.jpg"));
        productItems.add(new ProductItem("Seal", "a seal", 100, "https://c1.staticflickr.com/4/3852/14729534910_62b338dd72_z.jpg"));
        productItems.add(new ProductItem("Rhino", "a rhino", 100, "https://c1.staticflickr.com/1/335/18040640224_f56f05f8dc_z.jpg"));
        productItems.add(new ProductItem("Leopard", "a leopard", 100, "https://c1.staticflickr.com/9/8678/16645189230_b0e96e7af9_z.jpg"));
        productItems.add(new ProductItem("Hippo", "a hippo", 100, "https://c2.staticflickr.com/4/3774/9377370000_6a57d1cfec_z.jpg"));
        return productItems;
    }
}
