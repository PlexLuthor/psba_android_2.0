package co.pixelbeard.psba_android.Modules.Store.Acts;

import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import co.pixelbeard.psba_android.Modules.Profile.Frags.ProfileModuleFragment;
import co.pixelbeard.psba_android.Modules.Store.Frags.StoreModuleFragment;
import co.pixelbeard.psba_android.Modules.Store.Models.ProductItem;
import co.pixelbeard.psba_android.R;

public class ProductDetailActivity extends AppCompatActivity {

    private ViewPager viewPagerProduct;
    private View pagerIndicator;
    private PagerAdapter pagerAdapterProductMain;
    private ArrayList<String> IMAGES = new ArrayList<>();

    private TextView textNavTitle;
    private TextView textNavSubtitle;
    private Button buttonLeftNav;
    private Button buttonInsideRightNav;
    private Button buttonRightNav;

    private static final String EXTRA_PRODUCT_ITEM = "product_item";
    private static final String EXTRA_TRANSITION_NAME = "transition_name";

    int heightPixels;
    int widthPixels;

    private FrameLayout fragFrame;
    ProfileModuleFragment frag;
    FragmentTransaction transaction;

    private NestedScrollView scrollProductMain;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        // pixels, dpi //
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        heightPixels = metrics.heightPixels;
        widthPixels = metrics.widthPixels;
        int densityDpi = metrics.densityDpi;
        float xdpi = metrics.xdpi;
        float ydpi = metrics.ydpi;
        Log.i("XYZ", "widthPixels  = " + widthPixels);
        Log.i("XYZ", "heightPixels = " + heightPixels);

//        getSupportFragmentManager()
//                .beginTransaction()
//                .add(R.id.fragment_frame_product, ProductFragment.newInstance(widthPixels, heightPixels,))
//                .commit();

        IMAGES.add("https://c1.staticflickr.com/9/8796/17158681740_a6caa5099f_z.jpg");
        IMAGES.add("https://c2.staticflickr.com/4/3774/9377370000_6a57d1cfec_z.jpg");
        IMAGES.add("https://c1.staticflickr.com/5/4010/4210875342_7cb06a9b62_z.jpg?zz=1");

        findViews();

        setUpNavButtons();
    }

    private void findViews() {
        textNavTitle = (TextView) findViewById(R.id.text_store_nav_bar_title);
        textNavSubtitle = (TextView) findViewById(R.id.text_store_nav_bar_sub_title);
        buttonLeftNav = (Button) findViewById(R.id.button_left_nav_bar);
        buttonInsideRightNav = (Button) findViewById(R.id.button_inside_right_nav_bar);
        buttonRightNav = (Button) findViewById(R.id.button_right_nav_bar);

        pagerIndicator = findViewById(R.id.pager_indicator_mover);

        supportPostponeEnterTransition();

        Bundle extras = getIntent().getExtras();
        ProductItem productItem = extras.getParcelable(StoreModuleFragment.EXTRA_PRODUCT_ITEM);

        ImageView imageView = (ImageView) findViewById(R.id.image_view_product_detail_main);
        TextView textView = (TextView) findViewById(R.id.text_product_name);
//        textView.setText(productItem.detail);


        String imageUrl = productItem.imageUrl;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String imageTransitionName = extras.getString(StoreModuleFragment.EXTRA_PRODUCT_IMAGE_TRANSITION_NAME);
            imageView.setTransitionName(imageTransitionName);
        }

        Picasso.with(this)
                .load(imageUrl)
                .noFade()
                .fit()
                .centerCrop()
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        supportStartPostponedEnterTransition();
                    }

                    @Override
                    public void onError() {
                        supportStartPostponedEnterTransition();
                    }
                });
    }

    private void setUpNavButtons() {
        buttonLeftNav.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {

            }
        });
        buttonInsideRightNav.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {

            }
        });
        buttonRightNav.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {

            }
        });
    }
}
