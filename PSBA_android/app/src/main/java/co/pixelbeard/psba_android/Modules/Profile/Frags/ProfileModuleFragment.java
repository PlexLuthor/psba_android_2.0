package co.pixelbeard.psba_android.Modules.Profile.Frags;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import co.pixelbeard.psba_android.HQ;
import co.pixelbeard.psba_android.Modules.Creds.Acts.CredentialsActivity;
import co.pixelbeard.psba_android.Modules.Profile.Acts.ProfileEditActivity;
import co.pixelbeard.psba_android.R;


public class ProfileModuleFragment extends android.support.v4.app.Fragment {

    //views
    private RelativeLayout relay_nav_sandbox;
    private Button left_nav_button;
    private TextView nav_title;
    private Button right_nav_button;

    private RelativeLayout relay_user_info_sandbox;
    private ImageView imageview_avatar;
    private TextView text_name;
    private TextView text_position;
    private TextView text_store;
    private View view_points_line;
    private TextView text_my_points_title;
    private TextView text_points_score_number;

    private RelativeLayout relay_recyc_sandbox;
    private Button button_modules;
    private Button button_purchases;

    private static int widthPixels = 0;
    private static int heightPixels = 0;

    private FragmentListener mListener;

    public ProfileModuleFragment() {
        // Required empty public constructor
    }

    public static ProfileModuleFragment newInstance(int width, int height) {
        ProfileModuleFragment fragment = new ProfileModuleFragment();
        Bundle args = new Bundle();
        args.putInt("width", width);
        args.putInt("height", height);
        fragment.setArguments(args);
        return fragment;
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            widthPixels = getArguments().getInt("width");
            heightPixels = getArguments().getInt("height");
        }
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        findViews(view);
        setCustomFont();
        setUIInitialValues();

        // do something about this!
        text_name.setText(HQ.getInstance().theUser.getUsername());
        text_position.setText(HQ.getInstance().theUser.getOccupation());
        text_store.setText(HQ.getInstance().theUser.getBranchName());
        text_points_score_number.setText(HQ.getInstance().theUser.getPoints());

        left_nav_button.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                HQ.getInstance().logoutUser(getActivity());
                Intent logoutIntent = new Intent(getActivity(), CredentialsActivity.class);
                logoutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// | Intent.FLAG_ACTIVITY_NO_HISTORY);
                getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.fade_out);
                startActivity(logoutIntent);

                // do something about this!
                text_name.setText(HQ.getInstance().theUser.getUsername());
                text_position.setText(HQ.getInstance().theUser.getOccupation());
                text_store.setText(HQ.getInstance().theUser.getBranchName());
                text_points_score_number.setText(HQ.getInstance().theUser.getPoints());
            }
        });
        right_nav_button.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                HQ.getInstance().customAnimationForButton(v);
                Intent intentEditProfile = new Intent(getActivity(), ProfileEditActivity.class);
                startActivity(intentEditProfile);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.fade_out);
            }
        });
        button_modules.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                HQ.getInstance().customAnimationForButton(v);
            }
        });
        button_purchases.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                HQ.getInstance().customAnimationForButton(v);
            }
        });
        Picasso.with(getActivity()).load(HQ.getInstance().theAvatarURL + HQ.getInstance().theUser.getAvatar()).fit().centerCrop().into(imageview_avatar);
        return view;
    }

    @Override public void onStart() {
        super.onStart();
        if (HQ.getInstance().theUser.getAvatar().matches("") || HQ.getInstance().theUser.getAvatar() == null || HQ.getInstance().theUser.getAvatar().matches("default.png")) {
            Picasso.with(getActivity()).load(HQ.getInstance().theAvatarURL + "default-avatar.jpg").fit().centerCrop().into(imageview_avatar);
        } else {
            Picasso.with(getActivity()).load(HQ.getInstance().theAvatarURL + HQ.getInstance().theUser.getAvatar()).fit().centerCrop().into(imageview_avatar);
        }
    }

    @Override public void onResume() {
        super.onResume();
        Log.v("XYZ_FUTURE::: ", "onResume profile frag HQ.getInstance().trainingModulesSoftware.size() is: " + HQ.getInstance().trainingModulesSoftware.size());
    }

    public void findViews(View view) {
        relay_nav_sandbox = (RelativeLayout) view.findViewById(R.id.profile_relay_nav_sandbox);
        left_nav_button = (Button) view.findViewById(R.id.profile_left_nav_button);
        nav_title = (TextView) view.findViewById(R.id.profile_nav_title);
        right_nav_button = (Button) view.findViewById(R.id.profile_right_nav_button);

        relay_user_info_sandbox = (RelativeLayout) view.findViewById(R.id.profile_relay_user_info_sandbox);
        imageview_avatar = (ImageView) view.findViewById(R.id.profile_imageview_avatar);
        text_name = (TextView) view.findViewById(R.id.profile_text_name);
        text_position = (TextView) view.findViewById(R.id.profile_text_position);
        text_store = (TextView) view.findViewById(R.id.profile_text_store);
        view_points_line = view.findViewById(R.id.profile_view_points_line);
        text_my_points_title = (TextView) view.findViewById(R.id.profile_text_my_points_title);
        text_points_score_number = (TextView) view.findViewById(R.id.profile_text_points_score_number);

//        relay_recyc_sandbox = (RelativeLayout) view.findViewById(R.id.profile_relay_recyc_sandbox);
        button_modules = (Button) view.findViewById(R.id.profile_button_modules);
        button_purchases = (Button) view.findViewById(R.id.profile_button_purchases);
    }

    public void setCustomFont() {
        left_nav_button.setTypeface(HQ.getInstance().fontAwesome);
        nav_title.setTypeface(HQ.getInstance().sstFontRoman);
        right_nav_button.setTypeface(HQ.getInstance().fontAwesome);
        text_name.setTypeface(HQ.getInstance().sstFontBold);
        text_position.setTypeface(HQ.getInstance().sstFontRoman);
        text_store.setTypeface(HQ.getInstance().sstFontRoman);
        text_my_points_title.setTypeface(HQ.getInstance().sstFontRoman);
        text_points_score_number.setTypeface(HQ.getInstance().sstFontLight);
        button_modules.setTypeface(HQ.getInstance().sstFontRoman);
        button_purchases.setTypeface(HQ.getInstance().sstFontRoman);
    }

    public void setUIInitialValues() {
        button_modules.getLayoutParams().width = (int) (widthPixels * 0.5);
        button_modules.requestLayout();
        button_modules.setX(0);

        button_purchases.getLayoutParams().width = (int) (widthPixels * 0.5);
        button_purchases.requestLayout();
        button_purchases.setX(widthPixels * 0.5f);
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String str1, String str2) {
        if (mListener != null) {
            mListener.onFragmentFinish("lick", "deez");
            mListener.onTest(5);
        }
    }

    @Override public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof FragmentListener))
            throw new AssertionError();
        mListener = (FragmentListener) context;
    }

    @Override public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface FragmentListener {
        void onFragmentFinish(String testString1, String testString2);

        void onTest(int intTest);
    }


    // wizard apprentices //
    private int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    private int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

}
