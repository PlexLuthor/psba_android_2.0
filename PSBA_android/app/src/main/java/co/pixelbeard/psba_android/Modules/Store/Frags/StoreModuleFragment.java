package co.pixelbeard.psba_android.Modules.Store.Frags;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import co.pixelbeard.psba_android.HQ;
import co.pixelbeard.psba_android.Modules.Profile.Frags.ProfileModuleFragment;
import co.pixelbeard.psba_android.Modules.Store.Adapters.StoreFeaturedAdapter;
import co.pixelbeard.psba_android.Modules.Store.Adapters.StoreMainAdapter;
import co.pixelbeard.psba_android.Modules.Store.Models.ProductItem;
import co.pixelbeard.psba_android.ProductItemClickListener;
import co.pixelbeard.psba_android.R;
import co.pixelbeard.psba_android.Utils;

public class StoreModuleFragment extends android.support.v4.app.Fragment implements ProductItemClickListener {

    private Button buttonLeftNav;
    private Button buttonInsideRightNav;
    private Button buttonRightNav;

    private NestedScrollView scrollStoreMain;

    private StoreMainAdapter adapterStoreMain;
    private StoreFeaturedAdapter adapterStoreFeat;
    private RecyclerView recycViewMain;
    private LinearLayoutManager linlayManMainStore;
    private RecyclerView recycViewFeatured;
    private LinearLayoutManager linlayManFeaturedStore;

    private static int widthPixels = 0;
    private static int heightPixels = 0;

    private ImageView mProductImage;

    public static final String TAG = StoreModuleFragment.class.getSimpleName();
    public static final String EXTRA_PRODUCT_ITEM = "product_image_url";
    public static final String EXTRA_PRODUCT_IMAGE_TRANSITION_NAME = "product_image_transition_name";

    private StoreModuleFragment.FragmentListener mListener;

    public StoreModuleFragment() {
        // Required empty public constructor
    }

    public static StoreModuleFragment newInstance(int width, int height) {
        StoreModuleFragment fragment = new StoreModuleFragment();
        Bundle args = new Bundle();
        args.putInt("width", width);
        args.putInt("height", height);
        fragment.setArguments(args);
        return fragment;
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            widthPixels = getArguments().getInt("width");
            heightPixels = getArguments().getInt("height");
        }
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_store, container, false);

        findViews(view);
        setUpNavButtons();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        StoreMainAdapter storeMainAdapter = new StoreMainAdapter(Utils.generateProductItems(getContext()), this);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyc_store_main);
        linlayManMainStore = new LinearLayoutManager(getContext());
//        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(linlayManMainStore);
        recyclerView.setAdapter(storeMainAdapter);
    }

    public void onButtonPressed(String str1, String str2) {
        if (mListener != null) {
            mListener.onFragmentFinish("laeck", "deez");
            mListener.onTest(5);
        }
    }

    @Override public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof ProfileModuleFragment.FragmentListener))
            throw new AssertionError();
        mListener = (StoreModuleFragment.FragmentListener) context;
    }

    @Override public void onDetach() {
        super.onDetach();
        mListener = null;
    }

//    @Override public void onStoreMainItemClick(int p, ProductItem productItem, ImageView sharedImageView) {
//        Log.v("XYZ_FUTURE::: ", "store main onItemClick and p is: " + p);

        // replace fragment no anim //
//        ProductFragment productFragment = new ProductFragment().newInstance(widthPixels, heightPixels);
//        productFragment.newInstance(widthPixels, heightPixels);
//        if(getFragmentManager().findFragmentById(R.id.fragment_frame) != null) {
//            getFragmentManager().beginTransaction().detach(getFragmentManager().findFragmentById(R.id.fragment_frame)).addToBackStack(null).commit();
//        }
//        getFragmentManager().beginTransaction().replace(R.id.fragment_frame, productFragment).commit();
//    }

//    @Override public void onStoreFeatureItemClick(int p) {
//        Log.v("XYZ_FUTURE::: ", "store feature onItemClick and p is: " + p);
//    }

    @Override public void onProductItemClick(int pos, ProductItem productItem, ImageView sharedImageView) {
        // anim attempts frag //
        ProductFragment productFragment = ProductFragment.newInstance(widthPixels, heightPixels, productItem, ViewCompat.getTransitionName(sharedImageView));
        getFragmentManager()
                .beginTransaction()
                .addSharedElement(sharedImageView, ViewCompat.getTransitionName(sharedImageView))
                .addToBackStack(TAG)
                .replace(R.id.fragment_frame, productFragment)
                .commit();


        // anim attempts act //
//        Intent intent = new Intent(getActivity(), ProductDetailActivity.class);
//        intent.putExtra(EXTRA_PRODUCT_ITEM, productItem);
//        intent.putExtra(EXTRA_PRODUCT_IMAGE_TRANSITION_NAME, ViewCompat.getTransitionName(sharedImageView));
//
//        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
//                getActivity(),
//                sharedImageView,
//                ViewCompat.getTransitionName(sharedImageView));
//
//        startActivity(intent, options.toBundle());
    }


    public interface FragmentListener {
        void onFragmentFinish(String testString1, String testString2);
        void onTest(int intTest);
    }



    public void findViews(View view) {

        buttonLeftNav = (Button) view.findViewById(R.id.button_left_nav_bar);
        buttonInsideRightNav = (Button) view.findViewById(R.id.button_inside_right_nav_bar);
        buttonRightNav = (Button) view.findViewById(R.id.button_right_nav_bar);


        scrollStoreMain = (NestedScrollView) view.findViewById(R.id.scroll_store_main);
        scrollStoreMain.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                Log.v("XYZ_FUTURE::: ", "scrollY for main scroll is: " + scrollY);
//                if (scrollY < 1154) {
//                    recycViewMain.setNestedScrollingEnabled(false);
//                } else {
//                    recycViewMain.setNestedScrollingEnabled(true);
//                }
            }
        });

        recycViewFeatured = (RecyclerView) view.findViewById(R.id.recyc_store_featured);
        linlayManFeaturedStore = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recycViewFeatured.setLayoutManager(linlayManFeaturedStore);
        adapterStoreFeat = new StoreFeaturedAdapter(getActivity(), "test string store feature");
        recycViewFeatured.setAdapter(adapterStoreFeat);
//        adapterStoreFeat.setItemClickCallback(this);

        recycViewMain = (RecyclerView) view.findViewById(R.id.recyc_store_main);
//        linlayManMainStore = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
//        recycViewMain.setLayoutManager(linlayManMainStore);
//        adapterStoreMain = new StoreMainAdapter(getActivity(), "test string store main");
//        recycViewMain.setAdapter(adapterStoreMain);
//        adapterStoreMain.setItemClickCallback(this);


        recycViewMain.setNestedScrollingEnabled(false);
        recycViewMain.getLayoutParams().height = heightPixels - HQ.getInstance().dpToPx(162);
        recycViewMain.requestLayout();
        recycViewMain.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                Log.v("XYZ_FUTURE::: ", "scrollY for store scroll is: " + scrollY);
            }
        });
    }

    private void setUpNavButtons() {
        buttonLeftNav.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {

            }
        });
        buttonInsideRightNav.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {

            }
        });
        buttonRightNav.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {

            }
        });
    }
}
