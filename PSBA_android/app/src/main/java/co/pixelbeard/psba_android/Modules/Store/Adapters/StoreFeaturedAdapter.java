package co.pixelbeard.psba_android.Modules.Store.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import co.pixelbeard.psba_android.HQ;
import co.pixelbeard.psba_android.R;

/**
 * Created by aaroneckhart on 8/22/17.
 */

public class StoreFeaturedAdapter extends RecyclerView.Adapter<StoreFeaturedAdapter.StoreFeaturedHolder> {
    private LayoutInflater inflater;
    private String timeString;

    private String nameData;

    private StoreFeaturedAdapter.storeFeaturedItemClickCallback storeFeaturedItemClickCallback;
    public interface storeFeaturedItemClickCallback {
        void onStoreFeatureItemClick(int p);
    }

    public void setItemClickCallback (final StoreFeaturedAdapter.storeFeaturedItemClickCallback itemClickCallback) {
        this.storeFeaturedItemClickCallback = itemClickCallback;
    }

    public StoreFeaturedAdapter(Context context, String data) {
        this.inflater.from(context);
        this.nameData = data;
    }
    @Override public StoreFeaturedAdapter.StoreFeaturedHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyc_element_featured_store_object, parent, false);
        return new StoreFeaturedAdapter.StoreFeaturedHolder(view);
    }
    @Override public void onBindViewHolder(final StoreFeaturedAdapter.StoreFeaturedHolder holder, int position) {

    }

    @Override public int getItemCount() {
        return 10;
    }

    public class StoreFeaturedHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public CardView cardView;
        public ImageView productImage;

        public StoreFeaturedHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.recyc_store_featured_cardview);
            cardView.setOnClickListener(this);
            productImage = (ImageView) itemView.findViewById(R.id.recyc_element_featured_image);

            Picasso.with(itemView.getContext()).load(HQ.getInstance().theAvatarURL + "default-avatar.jpg").fit().centerCrop().into(productImage);

            itemView.setOnClickListener(this);
        }
        @Override public void onClick(View v) {
            if (v.getId() == R.id.recyc_store_featured_cardview) {
                Log.v("XYZ", "recyc_store_featured_cardview");
                storeFeaturedItemClickCallback.onStoreFeatureItemClick(getAdapterPosition());
            } else {
                Log.v("XYZ", "NOT cardView");
            }
        }
    }

    // check here for test //

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }
    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
}
