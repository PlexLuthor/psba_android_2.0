package co.pixelbeard.psba_android.Modules.Creds.Acts;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import co.pixelbeard.psba_android.MainActivity;
import co.pixelbeard.psba_android.R;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import pl.droidsonroids.gif.GifImageView;

public class ResetPasswordActivity extends AppCompatActivity {

    int heightPixels;
    int widthPixels;

    float inputHeightOnFocus;
    float inputHeightSlotOne;
    float inputHeightSlotTwo;
    float inputHeightSlotThree;
    float inputHintInX;
    float inputHintOutX;
    float inputHintAboveHeight;
    float editTextHeight;
    float editTextWidth;
    float loginButtonheight;
    float mainButtonHeight;
    float mainButtonSlot;
    float titleSlot;
    float mainButtonScaleDownValue;
    float hintTextViewWidth;
    float backButtonWidth;

    private Button buttonBack;
    private TextView textNavTitle;
    private TextView textInstructionsTitle;
    private TextView textInstructionsBody;
    private EditText editTextOne;
    private TextView textViewOne;
    private TextView inputIconOne;
    private EditText editTextTwo;
    private TextView textViewTwo;
    private TextView inputIconTwo;
    private EditText editTextThree;
    private TextView textViewThree;
    private TextView inputIconThree;

    private Button buttonResetPassword;

    private Typeface sstFontLight;
    private Typeface sstFontRoman;
    private Typeface sstFontHeavy;
    private Typeface sstFontBold;
    private Typeface sstFontCondensed;
    private Typeface sstFontCondensedBd;
    private Typeface fontAwesome;

    private int editTextFocus;

    private boolean succesIntentFlag = false;

    Spanned lockIcon = Html.fromHtml("&#xf023;");
    Spanned envelopeIcon = Html.fromHtml("&#xf0e0;");
    Spanned profileIcon = Html.fromHtml("&#xf007;");

    String lockIconString = " " + lockIcon + " ";
    String envelopeIconString = "" + envelopeIcon + " ";
    String profileIconString = " " + profileIcon + " ";

    private RelativeLayout psbaAlert;
    private View psbaAlertOverlay;
    private GifImageView psbaAlertGif;
    private TextView psbaAlertTitle;
    private TextView psbaAlertBody;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        // pixels, dpi //
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        heightPixels = metrics.heightPixels;
        widthPixels = metrics.widthPixels;

        findViews();
        setInitialValues();
        setCustomFont();
        setInitialPosition();

        setupEditTextOneListeners();
        setupEditTextTwoListeners();
        setupEditTextThreeListeners();

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.list_slide_out);
            }
        });

        editTextOne.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE) {
                    Log.v("XYZ_FUTURE::: ", "CHECK CHECK");
                    View v = getCurrentFocus();
                    if ( v instanceof EditText) {
                        Rect outRect = new Rect();
                        v.getGlobalVisibleRect(outRect);
                        v.clearFocus();
                        editTextFocus = 0;
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
                return false;
            }
        });
        editTextTwo.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE) {
                    Log.v("XYZ_FUTURE::: ", "CHECK CHECK");
                    View v = getCurrentFocus();
                    if ( v instanceof EditText) {
                        Rect outRect = new Rect();
                        v.getGlobalVisibleRect(outRect);
                        v.clearFocus();
                        editTextFocus = 0;
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
                return false;
            }
        });
        editTextThree.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE) {
                    Log.v("XYZ_FUTURE::: ", "CHECK CHECK");
                    View v = getCurrentFocus();
                    if ( v instanceof EditText) {
                        Rect outRect = new Rect();
                        v.getGlobalVisibleRect(outRect);
                        v.clearFocus();
                        editTextFocus = 0;
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
                return false;
            }
        });

        buttonResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override  public void onClick(View v) {
                if (editTextOne.getText().toString().matches("") || editTextTwo.getText().toString().matches("")  || editTextThree.getText().toString().matches("")) {
                    slideInAlert("Unable to Continue", "Please fill in all three forms", true, 2000);
                } else {
                    psbaAPIResetPassword();
                }

            }
        });
    }

    public void setupEditTextOneListeners() {
        editTextOne.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v1, boolean hasFocus) {
                if (hasFocus) {
                    Log.v("XYZ_FUTURE::: ", "EditTextTwo has focus");
                    editTextFocus = 1;

                    editTextOne.setCursorVisible(true);
//                    editTextOne.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
                    textViewOne.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotOne - (inputHintAboveHeight)).translationX(-textViewOne.getWidth() * 0.5f * 0.35f);

                } else if (!hasFocus) {
                    Log.v("XYZ_FUTURE::: ", "EditTextTwo NO focus and text is: " + editTextOne.getText().toString());
                    editTextFocus = 0;

                    if (editTextOne.getText().toString().matches("")) {
                        editTextOne.setCursorVisible(false);
//                        editTextOne.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border_gray));
                        textViewOne.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotOne).translationX(inputHintInX);
                    } else {
                        editTextOne.setCursorVisible(false);
//                        editTextOne.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
                        textViewOne.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotOne - (inputHintAboveHeight)).translationX(-textViewOne.getWidth() * 0.5f * 0.35f);
                    }
                    if (editTextTwo.getText().toString().matches("")) {
                        editTextTwo.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotTwo);
                        textViewTwo.animate().scaleX(1.0f).scaleY(1.0f);
                    } else {
                        editTextTwo.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotTwo);
                        textViewTwo.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotTwo - (inputHintAboveHeight)).translationX(-textViewTwo.getWidth() * 0.5f * 0.35f);
                    }
                }
            }
        });
    }

    public void setupEditTextTwoListeners() {
        editTextTwo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v2, boolean hasFocus) {
                if (hasFocus) {
                    Log.v("XYZ_FUTURE::: ", "EditTextTwo has focus");
                    editTextFocus = 2;
//                    focusOnInput(editTextFocus);

                    editTextTwo.setCursorVisible(true);
//                    editTextTwo.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
                    textViewTwo.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotTwo - (inputHintAboveHeight)).translationX(-textViewOne.getWidth() * 0.5f * 0.35f);

                } else if (!hasFocus) {
                    Log.v("XYZ_FUTURE::: ", "EditTextTwo NO focus");
                    editTextFocus = 0;

                    if (editTextTwo.getText().toString().matches("")) {
                        editTextTwo.setCursorVisible(false);
//                        editTextTwo.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border_gray));
                        textViewTwo.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotTwo).translationX(inputHintInX);
                    } else {
                        editTextTwo.setCursorVisible(false);
//                        editTextTwo.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
                        textViewTwo.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotTwo - (inputHintAboveHeight)).translationX(-textViewTwo.getWidth() * 0.5f * 0.35f);
                    }


                    if (editTextTwo.getText().toString().matches("")) {
                        editTextTwo.setCursorVisible(false);
//                        editTextTwo.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border_gray));
                        textViewTwo.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotTwo).translationX(inputHintInX);
                    } else {
                        editTextTwo.setCursorVisible(false);
//                        editTextTwo.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
                        textViewTwo.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotTwo - (inputHintAboveHeight)).translationX(-textViewTwo.getWidth() * 0.5f * 0.35f);
                    }
                }
            }
        });
    }

    public void setupEditTextThreeListeners() {
        editTextThree.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v2, boolean hasFocus) {
                if (hasFocus) {
                    Log.v("XYZ_FUTURE::: ", "EditTextThree has focus");
                    editTextFocus = 3;
//                    focusOnInput(editTextFocus);

                    editTextThree.setCursorVisible(true);
//                    editTextThree.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
                    textViewThree.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotThree - (inputHintAboveHeight)).translationX(-textViewOne.getWidth() * 0.5f * 0.35f);

                } else if (!hasFocus) {
                    Log.v("XYZ_FUTURE::: ", "EditTextThree NO focus");
                    editTextFocus = 0;

                    if (editTextThree.getText().toString().matches("")) {
                        editTextThree.setCursorVisible(false);
//                        editTextThree.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border_gray));
                        textViewThree.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotThree).translationX(inputHintInX);
                    } else {
                        editTextThree.setCursorVisible(false);
//                        editTextThree.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
                        textViewThree.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotThree - (inputHintAboveHeight)).translationX(-textViewThree.getWidth() * 0.5f * 0.35f);
                    }


                    if (editTextThree.getText().toString().matches("")) {
                        editTextThree.setCursorVisible(false);
//                        editTextThree.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border_gray));
                        textViewThree.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotThree).translationX(inputHintInX);
                    } else {
                        editTextThree.setCursorVisible(false);
//                        editTextThree.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
                        textViewThree.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotThree - (inputHintAboveHeight)).translationX(-textViewTwo.getWidth() * 0.5f * 0.35f);
                    }
                }
            }
        });
    }

    public void findViews() {
        buttonBack = (Button) findViewById(R.id.reset_pass_button_back);
        textNavTitle = (TextView) findViewById(R.id.reset_pass_nav_title_creds);
        textInstructionsTitle = (TextView) findViewById(R.id.reset_pass_instructions_title);
        textInstructionsBody = (TextView) findViewById(R.id.reset_pass_instructions_body);
        editTextOne = (EditText) findViewById(R.id.reset_pass_edit_text_one);
        textViewOne = (TextView) findViewById(R.id.reset_pass_text_view_one);
        inputIconOne = (TextView) findViewById(R.id.reset_pass_input_icon_1);
        editTextTwo = (EditText) findViewById(R.id.reset_pass_edit_text_two);
        textViewTwo = (TextView) findViewById(R.id.reset_pass_text_view_two);
        inputIconTwo = (TextView) findViewById(R.id.reset_pass_input_icon_2);
        editTextThree = (EditText) findViewById(R.id.reset_pass_edit_text_three);
        textViewThree = (TextView) findViewById(R.id.reset_pass_text_view_three);
        inputIconThree = (TextView) findViewById(R.id.reset_pass_input_icon_3);

        buttonResetPassword = (Button) findViewById(R.id.reset_pass_button_send_recovery_email);

        psbaAlertOverlay = findViewById(R.id.psba_alert_overlay);
        psbaAlert = (RelativeLayout) findViewById(R.id.psba_alert);
        psbaAlertGif = (GifImageView) findViewById(R.id.psba_alert_gif);
        psbaAlertTitle = (TextView) findViewById(R.id.psba_alert_title);
        psbaAlertBody = (TextView) findViewById(R.id.psba_alert_body);
//        psbaAlertOverlay = findViewById(R.id.psba_alert_overlay_reset_password);
//        psbaAlert = (RelativeLayout) findViewById(R.id.psba_alert_reset_password);
//        psbaAlertGif = (GifImageView) findViewById(R.id.psba_alert_gif_reset_password);
//        psbaAlertTitle = (TextView) findViewById(R.id.psba_alert_title_reset_password);
//        psbaAlertBody = (TextView) findViewById(R.id.psba_alert_body_reset_password);
    }

    public void setInitialValues() {
        titleSlot = heightPixels * 0.005f;
        inputHeightOnFocus = heightPixels * 0.38f;
        inputHeightSlotOne = heightPixels * 0.3f;
        inputHeightSlotTwo = heightPixels * 0.41f;
        inputHeightSlotThree = heightPixels * 0.52f;
        editTextHeight = heightPixels * 0.079f;
        editTextWidth = widthPixels - dpToPx(64);
        loginButtonheight = heightPixels * 0.069f;
        inputHintInX = widthPixels - (widthPixels - dpToPx(48));
        inputHintOutX = -dpToPx(14);
        inputHintAboveHeight = editTextHeight * 0.65f;
        mainButtonHeight = heightPixels * 0.071f;
        mainButtonSlot = heightPixels * 0.74f;
        backButtonWidth = widthPixels * 0.5f - dpToPx(38);
        mainButtonScaleDownValue = (backButtonWidth) / (widthPixels - dpToPx(64));
        hintTextViewWidth = widthPixels - (int)(inputHintInX * 2) - (dpToPx(96));
    }

    public void setInitialPosition() {
        editTextOne.getLayoutParams().height = (int) editTextHeight;
        editTextOne.requestLayout();
        editTextOne.setY(inputHeightSlotOne);

        textViewOne.getLayoutParams().height = (int) editTextHeight;
        textViewOne.getLayoutParams().width = (int) hintTextViewWidth;
        textViewOne.requestLayout();
        textViewOne.setX(inputHintInX);
        textViewOne.setY(inputHeightSlotOne);

        inputIconOne.getLayoutParams().height = (int) editTextHeight;
        inputIconOne.getLayoutParams().width = (int) editTextWidth;
        inputIconOne.requestLayout();
        inputIconOne.setText(profileIconString);
        inputIconOne.setY(inputHeightSlotOne);

        editTextTwo.getLayoutParams().height = (int) editTextHeight;
        editTextTwo.requestLayout();
        editTextTwo.setY(inputHeightSlotTwo);

        textViewTwo.getLayoutParams().height = (int) editTextHeight;
        textViewTwo.getLayoutParams().width = (int) hintTextViewWidth;
        textViewTwo.requestLayout();
        textViewTwo.setX(inputHintInX);
        textViewTwo.setY(inputHeightSlotTwo);

        inputIconTwo.getLayoutParams().height = (int) editTextHeight;
        inputIconTwo.getLayoutParams().width = (int) editTextWidth;
        inputIconTwo.requestLayout();
        inputIconTwo.setText(lockIconString);
        inputIconTwo.setY(inputHeightSlotTwo);

        editTextThree.getLayoutParams().height = (int) editTextHeight;
        editTextThree.requestLayout();
        editTextThree.setY(inputHeightSlotThree);

        textViewThree.getLayoutParams().height = (int) editTextHeight;
        textViewThree.getLayoutParams().width = (int) hintTextViewWidth;
        textViewThree.requestLayout();
        textViewThree.setX(inputHintInX);
        textViewThree.setY(inputHeightSlotThree);

        inputIconThree.getLayoutParams().height = (int) editTextHeight;
        inputIconThree.getLayoutParams().width = (int) editTextWidth;
        inputIconThree.requestLayout();
        inputIconThree.setText(lockIconString);
        inputIconThree.setY(inputHeightSlotThree);

        psbaAlert.getLayoutParams().width = widthPixels - dpToPx(50);
        psbaAlert.setY(100);
        psbaAlert.setX(widthPixels);
        psbaAlert.setZ(102);

        psbaAlertOverlay.setZ(101);
    }

    public void setCustomFont() {
        // fonts //
        sstFontLight = Typeface.createFromAsset(getAssets(), "fonts/SST-Light.ttf");
        sstFontRoman = Typeface.createFromAsset(getAssets(), "fonts/SST-Roman.ttf");
        sstFontHeavy = Typeface.createFromAsset(getAssets(), "fonts/SST-Heavy.ttf");
        sstFontBold = Typeface.createFromAsset(getAssets(), "fonts/SST-Bold.ttf");
        sstFontCondensed = Typeface.createFromAsset(getAssets(), "fonts/SST-Condensed.ttf");
        sstFontCondensedBd = Typeface.createFromAsset(getAssets(), "fonts/SST-CondensedBd.ttf");
        fontAwesome = Typeface.createFromAsset(getAssets(), "fonts/fontawesome-webfont.ttf");

        textNavTitle.setTypeface(sstFontBold);
        textInstructionsTitle.setTypeface(sstFontBold);
        textInstructionsBody.setTypeface(sstFontRoman);
        editTextOne.setTypeface(sstFontRoman);
        textViewOne.setTypeface(sstFontRoman);
        inputIconOne.setTypeface(fontAwesome);
        editTextTwo.setTypeface(sstFontRoman);
        textViewTwo.setTypeface(sstFontRoman);
        inputIconTwo.setTypeface(fontAwesome);
        editTextThree.setTypeface(sstFontRoman);
        textViewThree.setTypeface(sstFontRoman);
        inputIconThree.setTypeface(fontAwesome);
        buttonResetPassword.setTypeface(sstFontBold);
    }

    private int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
    private int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static HashMap<String, String> jsonToMap(String jsonString) throws JSONException {
        HashMap<String, String> map = new HashMap<String, String>();
        JSONObject jObject = new JSONObject(jsonString);
        Iterator<?> keys = jObject.keys();

        while (keys.hasNext()) {
            String key = (String)keys.next();
            String value = jObject.getString(key);
            map.put(key, value);
        }
//        Log.v("XYZ", "json : "+jObject);
//        Log.v("XYZ", "map : "+map);
        return map;
    }

    @Override public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    editTextFocus = 0;
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }


    // api //
    public void psbaAPIResetPassword() {
        final String username = editTextOne.getText().toString();
        final String code = editTextTwo.getText().toString();
        final String password = editTextThree.getText().toString();

        AsyncTask asyncTask = new AsyncTask() {
            @Override protected Object doInBackground(Object[] params) {
                OkHttpClient client = new OkHttpClient();

                RequestBody formBody = new FormBody.Builder()
                        .add("username", username)
                        .add("code", code)
                        .add("password", password)
                        .build();
                Request request = new Request.Builder()
                        .url("http://psba.pixelbeard.co/api/users/recover/reset")
                        .post(formBody)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    JSONObject JSONResponse = new JSONObject(response.body().string());

                    if (JSONResponse.getString("success").matches("true")) {
                        Log.v("XYZ_FUTURE::: ", "success true");

                        return "success";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return "fail";
            }
            @Override protected void onPostExecute(Object o) {
                if (o.equals("success")) {
                    succesIntentFlag = true;
                    slideInAlert("SUCCESS", "Your account has been reset.", true, 2000);

                } else {
                    slideInAlert("FAILURE", "We could not reset your account with the information provided.", true, 2000);
                }
            }
        };
        asyncTask.execute();
    }

    public void psbaAPICheckRecoveryCode() {
        final String username = editTextOne.getText().toString();
        final String code = editTextTwo.getText().toString();

        AsyncTask asyncTask = new AsyncTask() {
            @Override protected Object doInBackground(Object[] params) {
                OkHttpClient client = new OkHttpClient();

                RequestBody formBody = new FormBody.Builder()
                        .add("username", username)
                        .add("code", code)
                        .build();
                Request request = new Request.Builder()
                        .url("http://psba.pixelbeard.co/api/users/recover/check")
                        .post(formBody)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    JSONObject JSONResponse = new JSONObject(response.body().string());

                    if (JSONResponse.getString("success").matches("true")) {
                        Log.v("XYZ_FUTURE::: ", "success true in psbaAPICheckRecoveryCode");

                        return "success";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return "fail";
            }
            @Override protected void onPostExecute(Object o) {
                if (o.equals("success")) {

                } else {

                }
            }
        };
        asyncTask.execute();
    }

    public void slideInAlert(String title, String body, final boolean bgVisible, int timeOpen) {
        if (bgVisible == true) {
            psbaAlertOverlay.animate().alpha(0.4f);
            psbaAlertOverlay.setClickable(true);
        }

        psbaAlert.animate().translationX(widthPixels - psbaAlert.getWidth());
        psbaAlertTitle.setText(title);
        psbaAlertBody.setText(body);

        Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {
                slideOutAlert(bgVisible);
            }
        };
        handler.postDelayed(r, timeOpen);
    }

    public void slideOutAlert(boolean bgVisible) {
        if (bgVisible == true) {
            psbaAlertOverlay.animate().alpha(0.0f);
            psbaAlertOverlay.setClickable(false);
        }
        if (succesIntentFlag == true) {
            Intent intent = new Intent(ResetPasswordActivity.this, MainActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.list_slide_out);
            succesIntentFlag = false;
        }
        psbaAlert.animate().translationX(widthPixels);
    }
}
