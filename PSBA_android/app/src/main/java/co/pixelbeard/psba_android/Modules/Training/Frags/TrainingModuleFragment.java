package co.pixelbeard.psba_android.Modules.Training.Frags;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import co.pixelbeard.psba_android.HQ;
import co.pixelbeard.psba_android.Modules.Training.Acts.TrainingModuleDetailActivity;
import co.pixelbeard.psba_android.Modules.Training.Adapters.TrainingHomeFourAdapter;
import co.pixelbeard.psba_android.Modules.Training.Adapters.TrainingHomeOneAdapter;
import co.pixelbeard.psba_android.Modules.Training.Adapters.TrainingHomeThreeAdapter;
import co.pixelbeard.psba_android.Modules.Training.Adapters.TrainingHomeTwoAdapter;
import co.pixelbeard.psba_android.Modules.Training.Models.TrainingModule;
import co.pixelbeard.psba_android.R;
import co.pixelbeard.psba_android.TrainingItemClickListener;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class TrainingModuleFragment extends Fragment implements TrainingItemClickListener {

    private NestedScrollView scrollTrainingMain;

    private TextView textNavTitle;
    private TextView textNavSubtitle;
    private Button buttonLeftNav;
    private Button buttonInsideRightNav;
    private Button buttonRightNav;

    private ViewPager pagerFeatured;
    private TextView textFeaturedTitle;
    private TextView textFeaturedSubtitle;
    private TextView textFeaturedPoints;

    private TextView textTrainingLabelOne;
    private RecyclerView recycTrainingOneProgress;
    private TrainingHomeOneAdapter adapterTrainingOne;
    private LinearLayoutManager linlayManTrainingOne;

    private TextView textTrainingLabelTwo;
    private RecyclerView recycTrainingTwo;
    private TrainingHomeTwoAdapter adapterTrainingTwo;
    private LinearLayoutManager linlayManTrainingTwo;

    private TextView textTrainingLabelThree;
    private RecyclerView recycTrainingThree;
    private TrainingHomeThreeAdapter adapterTrainingThree;
    private LinearLayoutManager linlayManTrainingThree;

    private TextView textTrainingLabelFour;
    private RecyclerView recycTrainingFour;
    private TrainingHomeFourAdapter adapterTrainingFour;
    private LinearLayoutManager linlayManTrainingFour;





    private static int widthPixels = 0;
    private static int heightPixels = 0;

    private TrainingModuleFragment.FragmentListener mListener;

    public static final String EXTRA_TRAINING_ITEM = "training_image_url";
    public static final String EXTRA_TRAINING_IMAGE_TRANSITION_NAME = "training_image_transition_name";

    public TrainingModuleFragment() {
        // Required empty public constructor
    }

    public static TrainingModuleFragment newInstance(int width, int height) {
        TrainingModuleFragment fragment = new TrainingModuleFragment();
        Bundle args = new Bundle();
        args.putInt("width", width);
        args.putInt("height", height);
        fragment.setArguments(args);
        return fragment;
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            widthPixels = getArguments().getInt("width");
            heightPixels = getArguments().getInt("height");
            Log.v("XYZ", "SMF widthPix = " + widthPixels + " ::: heightPix = " + heightPixels);
        }
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_training_module, container, false);

        findViews(view);
        setUpNavButtons();
        assignFonts();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        psbaAPIGetTrainingModules();

//        adapterTrainingOne = new TrainingHomeOneAdapter(Utils2.generateTrainingItems(getContext()), this);
        linlayManTrainingOne = new LinearLayoutManager(getContext());
        linlayManTrainingOne.setOrientation(0);
        recycTrainingOneProgress.setLayoutManager(linlayManTrainingOne);
        recycTrainingOneProgress.setAdapter(adapterTrainingOne);

//        adapterTrainingTwo = new TrainingHomeTwoAdapter(new ArrayList<TrainingModule>(), this);
        linlayManTrainingTwo = new LinearLayoutManager(getContext());
        linlayManTrainingTwo.setOrientation(0);
        recycTrainingTwo.setLayoutManager(linlayManTrainingTwo);
        recycTrainingTwo.setAdapter(adapterTrainingTwo);

//        adapterTrainingThree = new TrainingHomeThreeAdapter(new ArrayList<TrainingModule>(), this);
        linlayManTrainingThree = new LinearLayoutManager(getContext());
        linlayManTrainingThree.setOrientation(0);
        recycTrainingThree.setLayoutManager(linlayManTrainingThree);
        recycTrainingThree.setAdapter(adapterTrainingThree);

//        adapterTrainingFour = new TrainingHomeFourAdapter(new ArrayList<TrainingModule>(), this);
        linlayManTrainingFour = new LinearLayoutManager(getContext());
        linlayManTrainingFour.setOrientation(0);
        recycTrainingFour.setLayoutManager(linlayManTrainingFour);
        recycTrainingFour.setAdapter(adapterTrainingFour);


    }

    public void onButtonPressed(String str1, String str2) {
        if (mListener != null) {
            mListener.onFragmentFinish("laeck", "deez");
            mListener.onTest(5);
        }
    }

    @Override public void onAttach(Context context) {
        super.onAttach(context);

        if (!(context instanceof TrainingModuleFragment.FragmentListener))
            throw new AssertionError();
        mListener = (TrainingModuleFragment.FragmentListener) context;
    }

    @Override public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override public void onTrainingItemClick(int pos, TrainingModule trainingItem, ImageView sharedImageView, String recycFlag) {

        // anim attempts act //
        Intent intent = new Intent(getActivity(), TrainingModuleDetailActivity.class);
        intent.putExtra(EXTRA_TRAINING_ITEM, String.valueOf(trainingItem));
        intent.putExtra(EXTRA_TRAINING_IMAGE_TRANSITION_NAME, ViewCompat.getTransitionName(sharedImageView));

        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                getActivity(),
                sharedImageView,
                ViewCompat.getTransitionName(sharedImageView));

        startActivity(intent, options.toBundle());


        Log.v("XYZ_FUTURE::: ", "training clicked and pos is: " + pos + " :: and recyc is: " + recycFlag);
    }


    public interface FragmentListener {
        void onFragmentFinish(String testString1, String testString2);
        void onTest(int intTest);
    }


    public void findViews(View view) {
        textNavTitle = (TextView) view.findViewById(R.id.text_training_nav_bar_title);
        textNavSubtitle = (TextView) view.findViewById(R.id.text_training_nav_bar_sub_title);
        buttonLeftNav = (Button) view.findViewById(R.id.button_left_nav_bar);
        buttonInsideRightNav = (Button) view.findViewById(R.id.button_inside_right_nav_bar);
        buttonRightNav = (Button) view.findViewById(R.id.button_right_nav_bar);


        scrollTrainingMain = (NestedScrollView) view.findViewById(R.id.scroll_training_main);
        scrollTrainingMain.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                Log.v("XYZ_FUTURE::: ", "scrollY for main scroll is: " + scrollY);
//                if (scrollY < 1154) {
//                    recycViewMain.setNestedScrollingEnabled(false);
//                } else {
//                    recycViewMain.setNestedScrollingEnabled(true);
//                }
            }
        });

        pagerFeatured = (ViewPager) view.findViewById(R.id.pager_training_featured);
        textFeaturedTitle = (TextView) view.findViewById(R.id.text_training_home_featured_title);
        textFeaturedSubtitle = (TextView) view.findViewById(R.id.text_training_home_featured_subtitle);
        textFeaturedPoints = (TextView) view.findViewById(R.id.text_training_home_featured_points);

        textTrainingLabelOne = (TextView) view.findViewById(R.id.text_training_label_one);
        textTrainingLabelTwo = (TextView) view.findViewById(R.id.text_training_label_two);
        textTrainingLabelThree = (TextView) view.findViewById(R.id.text_training_label_three);
        textTrainingLabelFour = (TextView) view.findViewById(R.id.text_training_label_four);

        recycTrainingOneProgress = (RecyclerView) view.findViewById(R.id.recyc_training_one_progress);
        recycTrainingTwo = (RecyclerView) view.findViewById(R.id.recyc_training_two);
        recycTrainingThree = (RecyclerView) view.findViewById(R.id.recyc_training_three);
        recycTrainingFour = (RecyclerView) view.findViewById(R.id.recyc_training_four);
    }

    private void assignFonts() {
        textNavTitle.setTypeface(HQ.getInstance().sstFontLight);
        textNavSubtitle.setTypeface(HQ.getInstance().sstFontLight);
        buttonLeftNav.setTypeface(HQ.getInstance().fontAwesome);
        buttonInsideRightNav.setTypeface(HQ.getInstance().fontAwesome);
        buttonRightNav.setTypeface(HQ.getInstance().fontAwesome);

        textFeaturedTitle.setTypeface(HQ.getInstance().sstFontLight);
        textFeaturedSubtitle.setTypeface(HQ.getInstance().sstFontRoman);
        textFeaturedPoints.setTypeface(HQ.getInstance().sstFontRoman);

        textTrainingLabelOne.setTypeface(HQ.getInstance().sstFontRoman);
        textTrainingLabelTwo.setTypeface(HQ.getInstance().sstFontRoman);
        textTrainingLabelThree.setTypeface(HQ.getInstance().sstFontRoman);
        textTrainingLabelFour.setTypeface(HQ.getInstance().sstFontRoman);
    }


    private void setUpNavButtons() {
        buttonLeftNav.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {

            }
        });
        buttonInsideRightNav.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {

            }
        });
        buttonRightNav.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {

            }
        });
    }

    public void psbaAPIGetTrainingModules() {
        AsyncTask asyncTask = new AsyncTask() {
            @Override protected Object doInBackground(Object[] params) {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(HQ.getInstance().theApiURL + "training/modules/user/" + HQ.getInstance().theUser.getUser_id())//3287110239")
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    JSONObject responseJSON = new JSONObject(response.body().string());
                    boolean success = responseJSON.getBoolean("success");

                    if (success) {
                        JSONArray dataList = responseJSON.getJSONArray("data");
                        HQ.getInstance().trainingModules.clear();
//                        HQ.getInstance().trainingModulesSoftware.clear();
//                        HQ.getInstance().trainingModulesEssentials.clear();
//                        HQ.getInstance().trainingModulesDigitalContent.clear();
                        for (int i = 0; i < dataList.length(); i++) {

                            JSONObject dataObject = (JSONObject) dataList.get(i);
                            int total_questions = dataObject.getInt("total_questions");
                            int correct_answers = dataObject.getInt("correct_answers");
                            int total_attempts = dataObject.getInt("total_attempts");
                            boolean passed = dataObject.getBoolean("passed");

                            HashMap<String, String> singleModuleHash = HQ.getInstance().jsonToMap(dataObject.getString("module"));
                            TrainingModule tm = new TrainingModule();

                            tm.setId(singleModuleHash.get("id"));
                            tm.setName(singleModuleHash.get("name"));
                            tm.setCategory_id(singleModuleHash.get("category_id"));
                            tm.setCategory_name(singleModuleHash.get("category_name"));
                            tm.setCategory_order(singleModuleHash.get("category_order"));
                            tm.setPoints(singleModuleHash.get("points"));
                            tm.setVersion(singleModuleHash.get("version"));
                            tm.setCreated_date(singleModuleHash.get("created_date"));
                            tm.setModified_date(singleModuleHash.get("modified_date"));
                            tm.setStatus(singleModuleHash.get("status"));
                            tm.setOrder(singleModuleHash.get("order"));
                            tm.setFeatured(singleModuleHash.get("featured"));

                            tm.setUserProgressRefTotalQuestions(total_questions);
                            tm.setUserProgressRefCorrectAnswers(correct_answers);
                            tm.setUserProgressRefTotalAttempts(total_attempts);
                            tm.setUserProgressRefPassed(passed);

                            HQ.getInstance().trainingModules.add(tm);
                        }

                        HQ.getInstance().categorizeTrainingModules();

                        return "success";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return "fail";
            }
            @Override protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if (o.equals("success")) {
                    setUpAdapters();
                } else {
                    Toast toastLogin = Toast.makeText(getActivity(), "Failed to connect to server. Please try again.", Toast.LENGTH_LONG);
                    toastLogin.show();
                }
            }
        };
        asyncTask.execute();
    }

    public void setUpAdapters() {
        adapterTrainingOne = new TrainingHomeOneAdapter(HQ.getInstance().trainingModulesProgress, this);
        recycTrainingOneProgress.setAdapter(adapterTrainingOne);
        adapterTrainingOne.notifyDataSetChanged();

        adapterTrainingTwo = new TrainingHomeTwoAdapter(HQ.getInstance().trainingModulesSoftware, this);
        recycTrainingTwo.setAdapter(adapterTrainingTwo);
        adapterTrainingTwo.notifyDataSetChanged();

        adapterTrainingThree = new TrainingHomeThreeAdapter(HQ.getInstance().trainingModulesEssentials, this);
        recycTrainingThree.setAdapter(adapterTrainingThree);
        adapterTrainingThree.notifyDataSetChanged();

        adapterTrainingFour = new TrainingHomeFourAdapter(HQ.getInstance().trainingModulesDigitalContent, this);
        recycTrainingFour.setAdapter(adapterTrainingFour);
        adapterTrainingFour.notifyDataSetChanged();
    }
}