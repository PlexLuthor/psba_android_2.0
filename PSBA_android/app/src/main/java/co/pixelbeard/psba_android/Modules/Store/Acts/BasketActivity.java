package co.pixelbeard.psba_android.Modules.Store.Acts;

import android.app.Activity;
import android.os.Bundle;

import co.pixelbeard.psba_android.R;

public class BasketActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basket);
    }
}
