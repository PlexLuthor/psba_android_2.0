package co.pixelbeard.psba_android.Modules.Profile.Models;

/**
 * Created by aaroneckhart on 7/23/17.
 */

public class User {

    String user_id;
    String username;
    String email;
    String auth_level;
    String banned;
    String passwd;
    String passwd_recovery_code;
    String passwd_recovery_date;
    String passwd_modified_at;
    String last_login;
    String created_at;
    String modified_at;
    String psnid;
    String firstname;
    String lastname;
    String occupation;
    String companyName;
    String companyId;
    String branchName;
    String address1;
    String address2;
    String town;
    String country;
    String postCode;
    String avatar;
    String allpoints;
    String points;
    String modulescompleted;
    String branch_id;
    String extras;

    public User() {
        //empty constr
    }


    //login
    public User(String firstname, String lastname, String username, String psnid, String email, String company_id, String branchname, String occupation, String points, String avatar, String user_id, String modulescompleted, String company_name) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.psnid = psnid;
        this.email = email;
        this.companyId = company_id;
        this.branchName = branchname;
        this.occupation = occupation;
        this.points = points;
        this.avatar = avatar;
        this.user_id = user_id;
        this.modulescompleted = modulescompleted;
        this.companyName = company_name;
    }

    //registration
    public User(String firstname, String lastname, String username, String psnid, String email, String password, String company_id, String branchname, String addressone, String addresstwo, String town, String county, String postcode, String occupation, String branch_id) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.psnid = psnid;
        this.email = email;
        this.passwd = password;
        this.companyId = company_id;
        this.branchName = branchname;
        this.address1 = addressone;
        this.address2 = addresstwo;
        this.town = town;
        this.country = county;
        this.postCode = postcode;
        this.occupation = occupation;
        this.branch_id = branch_id;
    }

    public User(String user_id, String username, String email, String auth_level, String banned, String passwd, String passwd_recovery_code, String passwd_recovery_date, String passwd_modified_at, String last_login, String created_at, String modified_at, String psnid, String firstname, String lastname, String occupation, String company_name, String company_id, String branch, String address1, String address2, String town, String country, String postCode, String extras, String allpoints, String points, String modulescompleted, String branch_id, String avatar) {
        this.user_id = user_id;
        this.username = username;
        this.email = email;
        this.auth_level = auth_level;
        this.banned = banned;
        this.passwd = passwd;
        this.passwd_recovery_code = passwd_recovery_code;
        this.passwd_recovery_date = passwd_recovery_date;
        this.passwd_modified_at = passwd_modified_at;
        this.last_login = last_login;
        this.created_at = created_at;
        this.modified_at = modified_at;
        this.psnid = psnid;
        this.firstname = firstname;
        this.lastname = lastname;
        this.occupation = occupation;
        this.companyName = company_name;
        this.companyId = company_id;
        this.branchName = branch;
        this.address1 = address1;
        this.address2 = address2;
        this.town = town;
        this.country = country;
        this.postCode = postCode;
        this.avatar = avatar;
        this.allpoints = allpoints;
        this.points = points;
        this.modulescompleted = modulescompleted;
        this.branch_id = branch_id;
        this.extras = extras;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAuth_level() {
        return auth_level;
    }

    public void setAuth_level(String auth_level) {
        this.auth_level = auth_level;
    }

    public String getBanned() {
        return banned;
    }

    public void setBanned(String banned) {
        this.banned = banned;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getPasswd_recovery_code() {
        return passwd_recovery_code;
    }

    public void setPasswd_recovery_code(String passwd_recovery_code) {
        this.passwd_recovery_code = passwd_recovery_code;
    }

    public String getPasswd_recovery_date() {
        return passwd_recovery_date;
    }

    public void setPasswd_recovery_date(String passwd_recovery_date) {
        this.passwd_recovery_date = passwd_recovery_date;
    }

    public String getPasswd_modified_at() {
        return passwd_modified_at;
    }

    public void setPasswd_modified_at(String passwd_modified_at) {
        this.passwd_modified_at = passwd_modified_at;
    }

    public String getLast_login() {
        return last_login;
    }

    public void setLast_login(String last_login) {
        this.last_login = last_login;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getModified_at() {
        return modified_at;
    }

    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    public String getPsnid() {
        return psnid;
    }

    public void setPsnid(String psnid) {
        this.psnid = psnid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAllpoints() {
        return allpoints;
    }

    public void setAllpoints(String allpoints) {
        this.allpoints = allpoints;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getModulescompleted() {
        return modulescompleted;
    }

    public void setModulescompleted(String modulescompleted) {
        this.modulescompleted = modulescompleted;
    }

    public String getBranch_id() {
        return branch_id;
    }

    public void setBranch_id(String branch_id) {
        this.branch_id = branch_id;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }
}
