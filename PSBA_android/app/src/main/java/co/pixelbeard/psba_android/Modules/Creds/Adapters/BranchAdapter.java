package co.pixelbeard.psba_android.Modules.Creds.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import co.pixelbeard.psba_android.R;
import co.pixelbeard.psba_android.HQ;

/**
 * Created by AE1 on 8/8/17.
 */

public class BranchAdapter extends RecyclerView.Adapter<BranchAdapter.BranchHolder> {

    private LayoutInflater inflater;
//    private String timeString;

    private List<String> nameData = Collections.emptyList();
//    private List<String> imageData = Collections.emptyList();
//    private LayoutInflater mInflater;

    private BranchAdapter.ItemClickCallback itemClickCallback;
    public interface ItemClickCallback {
        void onItemClick(int p);
    }

    public void setItemClickCallback (final BranchAdapter.ItemClickCallback itemClickCallback) {
        this.itemClickCallback = itemClickCallback;
    }

    public BranchAdapter(Context context, List<String> data) {
        this.inflater.from(context);
        this.nameData = data;
    }
    @Override public BranchAdapter.BranchHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyc_element_branch_search, parent, false);//inflater.inflate(R.layout.beep_list_item, parent, false);
        return new BranchAdapter.BranchHolder(view);
    }
    @Override public void onBindViewHolder(final BranchAdapter.BranchHolder holder, int position) {
        String branchName = nameData.get(position);
        holder.branchText.setText(branchName);
    }

    @Override public int getItemCount() {
        return HQ.getInstance().branches.size();
    }

    public class BranchHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView branchText;
        public RelativeLayout branchRowRelay;
        public BranchHolder(View itemView) {
            super(itemView);
            branchRowRelay = (RelativeLayout) itemView.findViewById(R.id.recyc_element_branch_row_relay);
            branchText = (TextView) itemView.findViewById(R.id.recyc_element_branch_text);
            itemView.setOnClickListener(this);

            branchText.setTypeface(HQ.getInstance().sstFontRoman);
            branchRowRelay.setOnClickListener(this);
        }
        @Override public void onClick(View v) {
            if (v.getId() == R.id.recyc_element_branch_row_relay) {
                Log.v("XYZ", "recyc");
                itemClickCallback.onItemClick(getAdapterPosition());
            } else {
                Log.v("XYZ", "NOT recyc");
            }
        }
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }
    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
}
