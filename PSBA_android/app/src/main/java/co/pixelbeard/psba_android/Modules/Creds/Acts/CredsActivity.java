package co.pixelbeard.psba_android.Modules.Creds.Acts;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import co.pixelbeard.psba_android.HQ;
import co.pixelbeard.psba_android.MainActivity;
import co.pixelbeard.psba_android.Modules.Creds.Models.Company;
import co.pixelbeard.psba_android.Modules.Profile.Models.User;
import co.pixelbeard.psba_android.R;
import co.pixelbeard.psba_android.Wizards.KeyboardWizard;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import pl.droidsonroids.gif.GifImageView;


public class CredsActivity extends AppCompatActivity {

    private RelativeLayout relay;
    private ImageView imageViewBrandTitle;
    private Button buttonLogin;
    private TextView buttonLoginText;
    private Button buttonSignUp;
    private View viewLoginUnderline;
    private View viewSignupUnderline;
    private View viewMovingUnderline;
    private TextView textViewStepPrompt;
    private EditText editTextOne;
    private TextView textViewOne;
    private TextView inputIconOne;
    private EditText editTextTwo;
    private TextView textViewTwo;
    private TextView inputIconTwo;
    private EditText editTextThree;
    private TextView textViewThree;
    private TextView inputIconThree;
    private Button buttonMainCreds;
    private Button buttonBackCreds;
    private TextView textForMainButton;
    private EditText editTextHidden2;
    private EditText editTextHidden3;
    private TextView textViewHidden2;
    private TextView textViewHidden3;
    private Spinner spinner1;
    private Spinner spinner2;
    private Spinner spinner3;
    private TextView customSpinnerRow;
    private TextView triangle1;
    private TextView triangle2;
    private TextView triangle3;

    private RelativeLayout psbaAlert;
    private View psbaAlertOverlay;
    private GifImageView psbaAlertGif;
    private TextView psbaAlertTitle;
    private TextView psbaAlertBody;

    int heightPixels;
    int widthPixels;

    float inputHeightOnFocus;
    float inputHeightSlotOne;
    float inputHeightSlotTwo;
    float inputHeightSlotThree;
    float inputHintInX;
    float inputHintOutX;
    float inputHintAboveHeight;
    float editTextHeight;
    float editTextWidth;
    float loginButtonheight;
    float mainButtonHeight;
    float mainButtonSlot;
    float titleSlot;
    float mainButtonScaleDownValue;
    float hintTextViewWidth;
    float backButtonWidth;

    boolean isFlagFromAvatar = false;

//    DBWizard dbWizard;

    private int credState;
    private int editTextFocus;
    private boolean isKeyboardDisplayed;

    private Typeface sstFontLight;
    private Typeface sstFontRoman;
    private Typeface sstFontHeavy;
    private Typeface sstFontBold;
    private Typeface sstFontCondensed;
    private Typeface sstFontCondensedBd;
    private Typeface fontAwesome;

    Spanned lockIcon = Html.fromHtml("&#xf023;");
    Spanned envelopeIcon = Html.fromHtml("&#xf0e0;");
    Spanned profileIcon = Html.fromHtml("&#xf007;");
    Spanned xIcon = Html.fromHtml("&#xf00d;");
    Spanned groupProfileIcon = Html.fromHtml("&#xf0c0;");
    Spanned briefcaseIcon = Html.fromHtml("&#xf0f2;");

    String lockIconString = " " + lockIcon + " ";
    String envelopeIconString = "" + envelopeIcon + " ";
    String profileIconString = " " + profileIcon + " ";
    String xIconString = " " + xIcon + " ";
    String groupProfileIconString = "" + groupProfileIcon + " ";
    String briefcaseIconString = "" + briefcaseIcon + " ";

    String[] items = new String[] {"Chai Latte", "Green Tea", "Black Tea",
            "shdsjgfdshj sdhjbfdhs fhjs sdj fhjds h hdjsfsdkfsjd fj", "dhjbfhdsj",
            "hdsbfdsjbhfsdjk", "bjfbhjfbs", "sfhsuhdsuicn", "bdvhsjbdhvj", "jbdfbk", "jdfuisbv", "jdbiucbds"};


    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creds);

        addKeyboardWizard();

        // pixels, dpi //
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        heightPixels = metrics.heightPixels;
        widthPixels = metrics.widthPixels;
        int densityDpi = metrics.densityDpi;
        float xdpi = metrics.xdpi;
        float ydpi = metrics.ydpi;
        Log.i("XYZ", "widthPixels  = " + widthPixels);
        Log.i("XYZ", "heightPixels = " + heightPixels);

        // orientation (either ORIENTATION_LANDSCAPE, ORIENTATION_PORTRAIT) //
        int orientation = getResources().getConfiguration().orientation;
        Log.i("XYZ", "orientation  = " + orientation);

        findViews();
        setInitialValues();
        setInitialUIPosAndSize();
        setCustomFont();

        credState = 0;

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View buttonLogin) {
                if (credState != 0 ) {
                    credUIState0();
                }
            }
        });

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View buttonLogin) {
                switch (credState) {
                    case 0:
                        credUIState1();
                        psbaAPIGetCompanies();
                        break;
                    case 1:case 2:case 3:case 4:case 5:
                        break;
                    default:
                        break;
                }
            }
        });

        buttonBackCreds.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                switch (credState) {
                    case 0:
                        break;
                    case 1:
                        credUIState0();
                        break;
                    case 2:
                        credUIState1();
                        break;
                    case 3:
                        credUIState2();
                        break;
                    case 4:
                        credUIState3();
                        break;
                    case 5:
                        break;
                    default:
                        break;
                }
            }
        });

        buttonMainCreds.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                switch (credState) {
                    case 0:
                        Log.v("XYZ", "buttonMainCreds credState1");
                        psbaAPILogin();
                        break;
                    case 1:
                        credUIState2();
                        Log.v("XYZ", "buttonMainCreds credState2");
                        break;
                    case 2:
                        credUIState3();
                        Log.v("XYZ", "buttonMainCreds credState3");
                        break;
                    case 3:
                        credUIState4();
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    default:
                        break;
                }
            }
        });

        setupEditTextOneListeners();
        setupEditTextTwoListeners();
        setupEditTextThreeListeners();
    }

    @Override protected void onDestroy() { super.onDestroy(); }

    @Override protected void onRestart() {
        super.onRestart();
    }

    @Override protected void onStart() {
        super.onStart();
    }

    @Override protected void onResume() { super.onResume(); }

    @Override protected void onStop() { super.onStop(); }

    @Override protected void onPause() { super.onPause(); }

    public void credUIState0() {
        imageViewBrandTitle.setAlpha(1.0f);

        buttonLogin.setAlpha(1.0f);
        buttonLogin.setText("");

        buttonSignUp.setAlpha(1.0f);
        //string
        buttonSignUp.setText("SIGN UP");

        //string
        buttonLoginText.setText("LOGIN");
        buttonLoginText.setX(dpToPx(35));

        viewMovingUnderline.animate().translationX(dpToPx(35));

        textViewStepPrompt.setAlpha(0.0f);

        editTextOne.setAlpha(0.0f);
        editTextOne.setClickable(false);
        textViewOne.setAlpha(0.0f);
        inputIconOne.setAlpha(0.0f);

        slide(false, "USERNAME", "PASSWORD", profileIconString, lockIconString);

        buttonMainCreds.setAlpha(0.3f);
        buttonMainCreds.animate().xBy((-backButtonWidth * 0.5f) - dpToPx(6));
        buttonMainCreds.animate().scaleX(1);

        buttonBackCreds.setClickable(false);
        ScaleAnimation anim = new ScaleAnimation(1.0f, 0.0f, 1.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        anim.setDuration(200);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override public void onAnimationStart(Animation animation) {
            }
            @Override public void onAnimationEnd(Animation animation) {
                buttonBackCreds.setAlpha(0.0f);
            }
            @Override public void onAnimationRepeat(Animation animation) {
            }
        });
        buttonBackCreds.startAnimation(anim);

        textForMainButton.animate().xBy(-backButtonWidth * 0.5f - dpToPx(6));
        //string
        textForMainButton.setText("LOGIN");

        buttonMainCreds.setAlpha(0.3f);

        credState = 0;
    }

    public void credUIState1() {
        imageViewBrandTitle.setAlpha(1.0f);

        buttonLogin.setAlpha(1.0f);
        //string
        buttonLogin.setText("LOGIN");

        buttonSignUp.setAlpha(1.0f);
        buttonSignUp.setText("");

        //string
        buttonLoginText.setText("SIGN UP");
        buttonLoginText.setX((widthPixels * 0.5f) + dpToPx(6));

        viewMovingUnderline.animate().translationX((widthPixels * 0.5f) + dpToPx(6));

        textViewStepPrompt.setAlpha(0.0f);

        editTextOne.setAlpha(0.0f);
        editTextOne.setClickable(false);
        textViewOne.setAlpha(0.0f);

        buttonMainCreds.setAlpha(0.3f);
        buttonMainCreds.setText("");

        buttonBackCreds.setAlpha(1.0f);
        buttonBackCreds.setClickable(false);

        if (credState == 0) {
            //string
            slide(true, "First Name", "Last Name", profileIconString, profileIconString);
            ScaleAnimation animeForBackButt = new ScaleAnimation(0.0f, 1.0f, 1.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f);
            animeForBackButt.setDuration(300);
            buttonBackCreds.startAnimation(animeForBackButt);

            buttonMainCreds.animate().xBy((backButtonWidth * 0.5f) + dpToPx(6));
            buttonMainCreds.animate().scaleX(mainButtonScaleDownValue);

            textForMainButton.animate().xBy(backButtonWidth * 0.5f + dpToPx(6));
        } else {
            //string
            slide(false, "First Name", "Last Name", profileIconString, profileIconString);
        }

        //string
        textForMainButton.setText("NEXT");

        credState = 1;
    }

    public void credUIState2() {
        imageViewBrandTitle.setAlpha(1.0f);

        buttonLogin.setAlpha(1.0f);

        //string
        buttonLogin.setText("LOGIN");

        buttonSignUp.setAlpha(1.0f);
        buttonSignUp.setText("");

        //string
        buttonLoginText.setText("SIGN UP");
        buttonLoginText.setX((widthPixels * 0.5f) + dpToPx(6));

        viewMovingUnderline.animate().translationX((widthPixels * 0.5f) + dpToPx(6));

        textViewStepPrompt.setAlpha(0.0f);

        editTextOne.setAlpha(0.0f);
        editTextOne.setClickable(false);
        textViewOne.setAlpha(0.0f);


        //slide direction
        if (credState == 1) {
            //string
            slide(true, "Enter A Username", "PSN (Optional)", profileIconString, xIconString);

        } else {
            //string
            slide(false, "Enter A Username", "PSN (Optional)", profileIconString, xIconString);

        }

        buttonMainCreds.setAlpha(0.3f);
        buttonMainCreds.setText("");

        buttonBackCreds.setAlpha(1.0f);
        buttonBackCreds.setClickable(true);

        //string
        textForMainButton.setText("NEXT");

        credState = 2;
    }

    public void credUIState3() {
        imageViewBrandTitle.setAlpha(1.0f);

        buttonLogin.setAlpha(1.0f);

        //string
        buttonLogin.setText("LOGIN");

        buttonSignUp.setAlpha(1.0f);
        buttonSignUp.setText("");

        //string
        buttonLoginText.setText("SIGN UP");
        buttonLoginText.setX((widthPixels * 0.5f) + dpToPx(6));

        viewMovingUnderline.animate().translationX((widthPixels * 0.5f) + dpToPx(6));

        textViewStepPrompt.setAlpha(0.0f);

        editTextOne.setAlpha(0.0f);
        editTextOne.setClickable(false);
        textViewOne.setAlpha(0.0f);


        //slide direction
        if (credState == 2) {
            //string
            slide(true, "Email", "Confirm Email", envelopeIconString, envelopeIconString);

        } else {
            //string
            slide(false, "Email", "Confirm Email", envelopeIconString, envelopeIconString);

        }


        buttonMainCreds.setAlpha(0.3f);
        buttonMainCreds.setText("");

        buttonBackCreds.setAlpha(1.0f);
        buttonBackCreds.setClickable(true);

        //string
        textForMainButton.setText("NEXT");

        credState = 3;
    }

    public void credUIState4() {
        imageViewBrandTitle.setAlpha(1.0f);

        buttonLogin.setAlpha(1.0f);

        //string
        buttonLogin.setText("LOGIN");

        buttonSignUp.setAlpha(1.0f);
        buttonSignUp.setText("");

        //string
        buttonLoginText.setText("SIGN UP");
        buttonLoginText.setX((widthPixels * 0.5f) + dpToPx(6));

        viewMovingUnderline.animate().translationX((widthPixels * 0.5f) + dpToPx(6));

        textViewStepPrompt.setAlpha(0.0f);

//        editTextOne.setAlpha(1.0f);
//        editTextOne.setClickable(true);
//        textViewOne.setAlpha(1.0f);


        //slide direction
        if (credState == 3) {
            //string

            slide(true, "Password", "Confirm Password", lockIconString, lockIconString);
        } else {
            //string
            slide(false, "Password", "Confirm Password", lockIconString, lockIconString);
        }

        buttonMainCreds.setAlpha(0.3f);
        buttonMainCreds.setText("");

        buttonBackCreds.setAlpha(1.0f);
        buttonBackCreds.setClickable(true);

        //string
        textForMainButton.setText("NEXT");

//        spinner1.setAlpha(1.0f);

        credState = 4;
    }

    public void credUIState5() {
        credState = 5;
    }


    public void addKeyboardWizard() {
        KeyboardWizard.addKeyboardToggleListener(this, new KeyboardWizard.SoftKeyboardToggleListener() {
            public void onToggleSoftKeyboard(boolean isVisible) {
                Log.v("XYZ", "keyboard visible: " + isVisible);
                isKeyboardDisplayed = isVisible;

                if (isVisible == true) {

                } else if (isVisible == false) {
//                    Log.v("XYZ", "focus is on : " + editTextFocus);

                    if (editTextFocus == 1) {
                        editTextOne.animate().translationY(inputHeightSlotOne);
                        editTextOne.clearFocus();
                    } else if (editTextFocus == 2) {
                        editTextTwo.animate().translationY(inputHeightSlotTwo);
                        editTextTwo.clearFocus();
                        if (credState == 1) {
                            Log.v("XYZ", "check username");
                            psbaAPICheckUsername();
                        }
                    } else if (editTextFocus == 3) {
                        editTextThree.animate().translationY(inputHeightSlotThree);
                        editTextThree.clearFocus();
                    }

                    editTextFocus = 0;

                    if (!editTextOne.getText().toString().matches("") && !editTextTwo.getText().toString().matches("") && !editTextThree.getText().toString().matches("")) {
                        Log.v("XYZ", "maybe force login now???");
                    }
                }
            }
        });
    }


    public void setupEditTextOneListeners() {
        editTextOne.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v1, boolean hasFocus) {
                if (hasFocus) {
                    editTextFocus = 1;
                    switch (credState) {
                        case 0:
                            imageViewBrandTitle.animate().translationY(heightPixels * 0.09f).scaleX(1.2f).scaleY(1.2f);
                            buttonLogin.animate().scaleX(0.0f).scaleY(0.0f);
                            buttonSignUp.animate().scaleX(0.0f).scaleY(0.0f);
                            viewLoginUnderline.animate().scaleX(0.0f).scaleY(0.0f);
                            viewSignupUnderline.animate().scaleX(0.0f).scaleY(0.0f);
                            textViewStepPrompt.animate().scaleX(0.0f).scaleY(0.0f);

                            editTextOne.setCursorVisible(true);
                            editTextOne.setBackground(ContextCompat.getDrawable(CredsActivity.this, R.drawable.edit_text_border));
                            editTextOne.animate().translationY(inputHeightOnFocus);
                            textViewOne.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightOnFocus - (inputHintAboveHeight)).translationX(-textViewOne.getWidth() * 0.5f * 0.35f);

                            editTextTwo.animate().scaleX(0.0f).scaleY(0.0f);
                            textViewTwo.animate().scaleX(0.0f).scaleY(0.0f);

                            editTextThree.animate().scaleX(0.0f).scaleY(0.0f);
                            textViewThree.animate().scaleX(0.0f).scaleY(0.0f);

                            buttonMainCreds.animate().scaleX(0.0f).scaleY(0.0f);
                            break;
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        case 4:
                            break;
                        case 5:
                            break;
                        default:
                            break;
                    }
                } else if (!hasFocus) {
                    editTextFocus = 0;
                    mainButtonAlpha();
                    switch (credState) {
                        case 0:
                            imageViewBrandTitle.animate().translationY(titleSlot).scaleX(1.0f).scaleY(1.0f);
                            buttonLogin.animate().scaleX(1.0f).scaleY(1.0f);
                            buttonSignUp.animate().scaleX(1.0f).scaleY(1.0f);
                            viewLoginUnderline.animate().scaleX(1.0f).scaleY(1.0f);
                            viewSignupUnderline.animate().scaleX(1.0f).scaleY(1.0f);
                            textViewStepPrompt.animate().scaleX(1.0f).scaleY(1.0f);

                            if (editTextOne.getText().toString().matches("")) {
                                editTextOne.setCursorVisible(false);
                                editTextOne.setBackground(ContextCompat.getDrawable(CredsActivity.this, R.drawable.edit_text_border_gray));
                                editTextOne.animate().translationY(inputHeightSlotOne);
                                textViewOne.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotOne).translationX(inputHintInX);
                            } else {
                                editTextOne.setCursorVisible(false);
                                editTextOne.setBackground(ContextCompat.getDrawable(CredsActivity.this, R.drawable.edit_text_border));
                                editTextOne.animate().translationY(inputHeightSlotOne);
                                textViewOne.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotOne - (inputHintAboveHeight)).translationX(-textViewOne.getWidth() * 0.5f * 0.35f);
                            }

                            if (editTextTwo.getText().toString().matches("")) {
                                editTextTwo.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotTwo);
                                textViewTwo.animate().scaleX(1.0f).scaleY(1.0f);
                            } else {
                                editTextTwo.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotTwo);
                                textViewTwo.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotTwo - (inputHintAboveHeight)).translationX(-textViewTwo.getWidth() * 0.5f * 0.35f);
                            }

                            if (editTextThree.getText().toString().matches("")) {
                                editTextThree.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotThree);
                                textViewThree.animate().scaleX(1.0f).scaleY(1.0f);
                            } else {
                                editTextThree.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotThree);
                                textViewThree.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotThree - (inputHintAboveHeight)).translationX(-textViewThree.getWidth() * 0.5f * 0.35f);
                            }

                            buttonMainCreds.animate().scaleX(1.0f).scaleY(1.0f);
                            break;
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        case 4:
                            break;
                        case 5:
                            break;
                        default:
                            break;
                    }
                }
            }
        });
    }

    public void setupEditTextTwoListeners() {
        editTextTwo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v2, boolean hasFocus) {
                if (hasFocus) {
                    editTextFocus = 2;
                    focusOnInput(editTextFocus);

                } else if (!hasFocus) {
                    editTextFocus = 0;
                    mainButtonAlpha();
                    switch (credState) {
                        case 0:
                            imageViewBrandTitle.animate().translationY(titleSlot).scaleX(1.0f).scaleY(1.0f);
                            buttonLogin.animate().scaleX(1.0f).scaleY(1.0f);
                            buttonSignUp.animate().scaleX(1.0f).scaleY(1.0f);
                            buttonLoginText.animate().scaleX(1.0f).scaleY(1.0f);
                            viewMovingUnderline.animate().scaleX(1.0f).scaleY(1.0f);
                            viewLoginUnderline.animate().scaleX(1.0f).scaleY(1.0f);
                            viewSignupUnderline.animate().scaleX(1.0f).scaleY(1.0f);

                            if (editTextTwo.getText().toString().matches("")) {
                                editTextTwo.setCursorVisible(false);
                                editTextTwo.setBackground(ContextCompat.getDrawable(CredsActivity.this, R.drawable.edit_text_border_gray));
                                editTextTwo.animate().translationY(inputHeightSlotTwo);
                                textViewTwo.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotTwo).translationX(inputHintInX);
                            } else {
                                editTextTwo.setCursorVisible(false);
                                editTextTwo.setBackground(ContextCompat.getDrawable(CredsActivity.this, R.drawable.edit_text_border));
                                editTextTwo.animate().translationY(inputHeightSlotTwo);
                                textViewTwo.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotTwo - (inputHintAboveHeight)).translationX(-textViewTwo.getWidth() * 0.5f * 0.35f);
                            }

                            if (editTextThree.getText().toString().matches("")) {
                                editTextThree.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotThree);
                                textViewThree.animate().scaleX(1.0f).scaleY(1.0f);
                            } else {
                                editTextThree.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotThree);
                                textViewThree.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotThree - (inputHintAboveHeight)).translationX(-textViewThree.getWidth() * 0.5f * 0.35f);
                            }

                            inputIconTwo.animate().translationY(inputHeightSlotTwo);
                            inputIconThree.animate().scaleX(1.0f).scaleY(1.0f);

                            buttonMainCreds.animate().scaleX(1.0f).scaleY(1.0f);
                            buttonBackCreds.animate().scaleX(1.0f).scaleY(1.0f);
                            break;
                        case 1:case 2:case 3:case 4:
                            imageViewBrandTitle.animate().translationY(titleSlot).scaleX(1.0f).scaleY(1.0f);
                            buttonLogin.animate().scaleX(1.0f).scaleY(1.0f);
                            buttonSignUp.animate().scaleX(1.0f).scaleY(1.0f);
                            buttonLoginText.animate().scaleX(1.0f).scaleY(1.0f);
                            viewMovingUnderline.animate().scaleX(1.0f).scaleY(1.0f);
                            viewLoginUnderline.animate().scaleX(1.0f).scaleY(1.0f);
                            viewSignupUnderline.animate().scaleX(1.0f).scaleY(1.0f);

//                            textViewStepPrompt.animate().scaleX(1.0f).scaleY(1.0f);

//                            if (editTextOne.getText().toString().matches("")) {
//                                editTextOne.animate().scaleX(1.0f).scaleY(1.0f);
//                                textViewOne.animate().scaleX(1.0f).scaleY(1.0f);
//                            } else {
//                                editTextOne.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotOne);
//                                textViewOne.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotOne - (inputHintAboveHeight)).translationX(-textViewOne.getWidth() * 0.5f * 0.35f);
//                            }

                            if (editTextTwo.getText().toString().matches("")) {
                                editTextTwo.setCursorVisible(false);
                                editTextTwo.setBackground(ContextCompat.getDrawable(CredsActivity.this, R.drawable.edit_text_border_gray));
                                editTextTwo.animate().translationY(inputHeightSlotTwo);
                                textViewTwo.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotTwo).translationX(inputHintInX);
                            } else {
                                editTextTwo.setCursorVisible(false);
                                editTextTwo.setBackground(ContextCompat.getDrawable(CredsActivity.this, R.drawable.edit_text_border));
                                editTextTwo.animate().translationY(inputHeightSlotTwo);
                                textViewTwo.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotTwo - (inputHintAboveHeight)).translationX(-textViewTwo.getWidth() * 0.5f * 0.35f);
                            }

                            if (editTextThree.getText().toString().matches("")) {
                                editTextThree.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotThree);
                                textViewThree.animate().scaleX(1.0f).scaleY(1.0f);
                            } else {
                                editTextThree.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotThree);
                                textViewThree.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotThree - (inputHintAboveHeight)).translationX(-textViewThree.getWidth() * 0.5f * 0.35f);
                            }

                            inputIconTwo.animate().translationY(inputHeightSlotTwo);
                            inputIconThree.animate().scaleX(1.0f).scaleY(1.0f);

                            buttonMainCreds.animate().scaleX(mainButtonScaleDownValue).scaleY(1.0f);
                            buttonBackCreds.animate().scaleX(1.0f).scaleY(1.0f);
                            break;
                        case 5:
                            break;
                        default:
                            break;
                    }
                }
            }
        });
    }

    public void setupEditTextThreeListeners() {
        editTextThree.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v3, boolean hasFocus) {
                if (hasFocus) {
                    editTextFocus = 3;
                    focusOnInput(editTextFocus);

                } else if (!hasFocus) {
                    editTextFocus = 0;
                    mainButtonAlpha();
                    switch (credState) {
                        case 0:
                            imageViewBrandTitle.animate().translationY(titleSlot).scaleX(1.0f).scaleY(1.0f);
                            buttonLogin.animate().scaleX(1.0f).scaleY(1.0f);
                            buttonSignUp.animate().scaleX(1.0f).scaleY(1.0f);
                            buttonLoginText.animate().scaleX(1.0f).scaleY(1.0f);
                            viewMovingUnderline.animate().scaleX(1.0f).scaleY(1.0f);
                            viewLoginUnderline.animate().scaleX(1.0f).scaleY(1.0f);
                            viewSignupUnderline.animate().scaleX(1.0f).scaleY(1.0f);

//                            textViewStepPrompt.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotTwo - textViewStepPrompt.getHeight() - heightPixels * 0.045f);
//                            Log.v("XYZ", "textPrompt height is: "+ pxToDp(textViewStepPrompt.getHeight()));

//                            if (editTextOne.getText().toString().matches("")) {
//                                editTextOne.animate().scaleX(1.0f).scaleY(1.0f);
//                                textViewOne.animate().scaleX(1.0f).scaleY(1.0f);
//                            } else {
//                                editTextOne.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotOne);
//                                textViewOne.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotOne - (inputHintAboveHeight)).translationX(-textViewOne.getWidth() * 0.5f * 0.35f);
//                            }

                            if (editTextTwo.getText().toString().matches("")) {
                                editTextTwo.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotTwo);
                                textViewTwo.animate().scaleX(1.0f).scaleY(1.0f);
                            } else {
                                editTextTwo.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotTwo);
                                textViewTwo.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotTwo - (inputHintAboveHeight)).translationX(-textViewTwo.getWidth() * 0.5f * 0.35f);
                            }

                            if (editTextThree.getText().toString().matches("")) {
                                editTextThree.setCursorVisible(false);
                                editTextThree.setBackground(ContextCompat.getDrawable(CredsActivity.this, R.drawable.edit_text_border_gray));
                                editTextThree.animate().translationY(inputHeightSlotThree);
                                textViewThree.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotThree).translationX(inputHintInX);
                            } else {
                                editTextThree.setCursorVisible(false);
                                editTextThree.setBackground(ContextCompat.getDrawable(CredsActivity.this, R.drawable.edit_text_border));
                                editTextThree.animate().translationY(inputHeightSlotThree);
                                textViewThree.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotThree - (inputHintAboveHeight)).translationX(-textViewThree.getWidth() * 0.5f * 0.35f);
                            }

                            inputIconThree.animate().translationY(inputHeightSlotThree);
                            inputIconTwo.animate().scaleX(1.0f).scaleY(1.0f);

                            buttonMainCreds.animate().scaleX(1.0f).scaleY(1.0f);
                            buttonBackCreds.animate().scaleX(1.0f).scaleY(1.0f);

                            break;
                        case 1:case 2:case 3:case 4:
                            imageViewBrandTitle.animate().translationY(titleSlot).scaleX(1.0f).scaleY(1.0f);
                            buttonLogin.animate().scaleX(1.0f).scaleY(1.0f);
                            buttonSignUp.animate().scaleX(1.0f).scaleY(1.0f);
                            buttonLoginText.animate().scaleX(1.0f).scaleY(1.0f);
                            viewMovingUnderline.animate().scaleX(1.0f).scaleY(1.0f);
                            viewLoginUnderline.animate().scaleX(1.0f).scaleY(1.0f);
                            viewSignupUnderline.animate().scaleX(1.0f).scaleY(1.0f);

                            if (editTextTwo.getText().toString().matches("")) {
                                editTextTwo.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotTwo);
                                textViewTwo.animate().scaleX(1.0f).scaleY(1.0f);
                            } else {
                                editTextTwo.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotTwo);
                                textViewTwo.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotTwo - (inputHintAboveHeight)).translationX(-textViewTwo.getWidth() * 0.5f * 0.35f);
                            }

                            if (editTextThree.getText().toString().matches("")) {
                                editTextThree.setCursorVisible(false);
                                editTextThree.setBackground(ContextCompat.getDrawable(CredsActivity.this, R.drawable.edit_text_border_gray));
                                editTextThree.animate().translationY(inputHeightSlotThree);
                                textViewThree.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotThree).translationX(inputHintInX);
                            } else {
                                editTextThree.setCursorVisible(false);
                                editTextThree.setBackground(ContextCompat.getDrawable(CredsActivity.this, R.drawable.edit_text_border));
                                editTextThree.animate().translationY(inputHeightSlotThree);
                                textViewThree.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotThree - (inputHintAboveHeight)).translationX(-textViewThree.getWidth() * 0.5f * 0.35f);
                            }

                            inputIconThree.animate().translationY(inputHeightSlotThree);
                            inputIconTwo.animate().scaleX(1.0f).scaleY(1.0f);

                            buttonMainCreds.animate().scaleX(mainButtonScaleDownValue).scaleY(1.0f);
                            buttonBackCreds.animate().scaleX(1.0f).scaleY(1.0f);
                            break;
                        case 5:
                            break;
                        default:
                            break;
                    }
                }
            }
        });
    }

    // misc //
    private int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
    private int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

//
//    private void setMargins (View view, int left, int top, int right, int bottom) {
//        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
//            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
//            p.setMargins(left, top, right, bottom);
//            view.requestLayout();
//        }
//    }

    @Override public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    editTextFocus = 0;
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }


    public void slide(boolean isFromLtoR, final String hintText2, final String hintText3, final String icon2, final String icon3) {

        final int valueXincoming;
        final int valueXoutgoing;

        if (isFromLtoR) {
            valueXincoming = widthPixels + 1;
            valueXoutgoing = -widthPixels;
        } else {
            valueXincoming = -widthPixels;
            valueXoutgoing = widthPixels + 1;
        }

        editTextHidden2.setText(hintText2);
        editTextHidden2.setX(valueXincoming);
        editTextHidden2.animate().translationX(dpToPx(32));
        textViewHidden2.setText(icon2);
        textViewHidden2.setX(valueXincoming);
        textViewHidden2.animate().translationX(dpToPx(32));
        textViewTwo.animate().translationX(valueXoutgoing);
        inputIconTwo.animate().translationX(valueXoutgoing);
        editTextTwo.animate().translationX(valueXoutgoing).withEndAction(new Runnable() {
            @Override public void run() {

                inputIconTwo.setText(icon2);
                inputIconTwo.setX(dpToPx(32));

                textViewTwo.setText(hintText2);
                textViewTwo.setScaleX(1.0f);
                textViewTwo.setScaleY(1.0f);
                textViewTwo.setY(inputHeightSlotTwo);
                textViewTwo.setX(dpToPx(32) + inputHintInX);

                editTextHidden2.setX(valueXincoming);
                textViewHidden2.setX(valueXincoming);

                editTextTwo.setX(dpToPx(32));
                editTextTwo.setCursorVisible(false);
                editTextTwo.setText("");
                editTextTwo.setBackground(ContextCompat.getDrawable(CredsActivity.this, R.drawable.edit_text_border_gray));
            }
        });

        editTextHidden3.setText(hintText3);
        editTextHidden3.setX(valueXincoming);
        editTextHidden3.animate().translationX(dpToPx(32));
        textViewHidden3.setText(icon3);
        textViewHidden3.setX(valueXincoming);
        textViewHidden3.animate().translationX(dpToPx(32));
        textViewThree.animate().translationX(valueXoutgoing);
        inputIconThree.animate().translationX(valueXoutgoing);
        editTextThree.animate().translationX(valueXoutgoing).withEndAction(new Runnable() {
            @Override public void run() {
                inputIconThree.setText(icon3);
                inputIconThree.setX(dpToPx(32));

                textViewThree.setText(hintText3);
                textViewThree.setScaleX(1.0f);
                textViewThree.setScaleY(1.0f);
                textViewThree.setY(inputHeightSlotThree);
                textViewThree.setX(dpToPx(32) + inputHintInX);

                editTextHidden3.setX(valueXincoming);
                textViewHidden3.setX(valueXincoming);

                editTextThree.setX(dpToPx(32));
                editTextThree.setCursorVisible(false);
                editTextThree.setText("");
                editTextThree.setBackground(ContextCompat.getDrawable(CredsActivity.this, R.drawable.edit_text_border_gray));
            }
        });
    }


    public void mainButtonAlpha() {
        switch (credState) {
            case 0:case 1:case 3:case 4:
                if (!editTextTwo.getText().toString().matches("") && !editTextThree.getText().toString().matches("")) {
                    buttonMainCreds.setAlpha(1.0f);
                } else {
                    buttonMainCreds.setAlpha(0.3f);
                }
                break;
            case 2:
                if (!editTextTwo.getText().toString().matches("")) {
                    buttonMainCreds.setAlpha(1.0f);
                } else {
                    buttonMainCreds.setAlpha(0.3f);
                }
                break;
            case 5:
                break;
            default:
                break;
        }
    }


    public void focusOnInput(int inputNumber) {
        EditText theInputField = null;
        TextView theHintTextView = null;
        TextView theInputIcon = null;

        EditText theOtherInputField = null;
        TextView theOtherHintTextView = null;
        TextView theOtherInputIcon = null;

        EditText theOtherOtherInputField = null;
        TextView theOtherOtherHintTextView = null;
        TextView theOtherOtherInputIcon = null;

        if (inputNumber == 1) {
            theInputField = editTextOne;
            theHintTextView = textViewOne;
            theInputIcon = inputIconOne;

            theOtherInputField = editTextTwo;
            theOtherHintTextView = textViewTwo;
            theOtherInputIcon = inputIconTwo;

            theOtherOtherInputField = editTextThree;
            theOtherOtherHintTextView = textViewThree;
            theOtherOtherInputIcon = inputIconThree;
        } else if (inputNumber == 2) {
            theInputField = editTextTwo;
            theHintTextView = textViewTwo;
            theInputIcon = inputIconTwo;

            theOtherInputField = editTextOne;
            theOtherHintTextView = textViewOne;
            theOtherInputIcon = inputIconOne;

            theOtherOtherInputField = editTextThree;
            theOtherOtherHintTextView = textViewThree;
            theOtherOtherInputIcon = inputIconThree;
        } else if (inputNumber == 3) {
            theInputField = editTextThree;
            theHintTextView = textViewThree;
            theInputIcon = inputIconThree;

            theOtherInputField = editTextOne;
            theOtherHintTextView = textViewOne;
            theOtherInputIcon = inputIconOne;

            theOtherOtherInputField = editTextTwo;
            theOtherOtherHintTextView = textViewTwo;
            theOtherOtherInputIcon = inputIconTwo;
        }

        imageViewBrandTitle.animate().translationY(heightPixels * 0.09f).scaleX(1.2f).scaleY(1.2f);
        buttonLogin.animate().scaleX(0.0f).scaleY(0.0f);
        buttonSignUp.animate().scaleX(0.0f).scaleY(0.0f);
        buttonLoginText.animate().scaleX(0.0f).scaleY(0.0f);
        viewMovingUnderline.animate().scaleX(0.0f).scaleY(0.0f);
        viewLoginUnderline.animate().scaleX(0.0f).scaleY(0.0f);
        viewSignupUnderline.animate().scaleX(0.0f).scaleY(0.0f);
        buttonMainCreds.animate().scaleX(0.0f).scaleY(0.0f);
        buttonBackCreds.animate().scaleX(0.0f).scaleY(0.0f);

        theOtherInputField.animate().scaleX(0.0f).scaleY(0.0f);
        theOtherHintTextView.animate().scaleX(0.0f).scaleY(0.0f);
        theOtherInputIcon.animate().scaleX(0.0f).scaleY(0.0f);

        theOtherOtherInputField.animate().scaleX(0.0f).scaleY(0.0f);
        theOtherOtherHintTextView.animate().scaleX(0.0f).scaleY(0.0f);
        theOtherOtherInputIcon.animate().scaleX(0.0f).scaleY(0.0f);

        theInputField.setCursorVisible(true);
        theInputField.setBackground(ContextCompat.getDrawable(CredsActivity.this, R.drawable.edit_text_border));
        theInputField.animate().translationY(inputHeightOnFocus);
        theInputIcon.animate().translationY(inputHeightOnFocus);
        theInputIcon.setTextColor(ContextCompat.getColor(CredsActivity.this, R.color.colorPrimaryDark));
        theHintTextView.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightOnFocus - (inputHintAboveHeight)).translationX(-theHintTextView.getWidth() * 0.5f * 0.35f);
    }

    public void setInitialValues() {
        titleSlot = heightPixels * 0.005f;
        inputHeightOnFocus = heightPixels * 0.38f;
        inputHeightSlotOne = heightPixels * 0.3935f;
        inputHeightSlotTwo = heightPixels * 0.499f;
        inputHeightSlotThree = heightPixels * 0.6048f;
        editTextHeight = heightPixels * 0.079f;
        editTextWidth = widthPixels - dpToPx(64);
        loginButtonheight = heightPixels * 0.069f;
        inputHintInX = widthPixels - (widthPixels - dpToPx(48));
        inputHintOutX = -dpToPx(14);
        inputHintAboveHeight = editTextHeight * 0.65f;
        mainButtonHeight = heightPixels * 0.071f;
        mainButtonSlot = heightPixels * 0.74f;
        backButtonWidth = widthPixels * 0.5f - dpToPx(38);
        mainButtonScaleDownValue = (backButtonWidth) / (widthPixels - dpToPx(64));
        hintTextViewWidth = widthPixels - (int)(inputHintInX * 2) - (dpToPx(96));
    }

    public void setCustomFont() {
        // fonts //
        sstFontLight = Typeface.createFromAsset(getAssets(), "fonts/SST-Light.ttf");
        sstFontRoman = Typeface.createFromAsset(getAssets(), "fonts/SST-Roman.ttf");
        sstFontHeavy = Typeface.createFromAsset(getAssets(), "fonts/SST-Heavy.ttf");
        sstFontBold = Typeface.createFromAsset(getAssets(), "fonts/SST-Bold.ttf");
        sstFontCondensed = Typeface.createFromAsset(getAssets(), "fonts/SST-Condensed.ttf");
        sstFontCondensedBd = Typeface.createFromAsset(getAssets(), "fonts/SST-CondensedBd.ttf");
        fontAwesome = Typeface.createFromAsset(getAssets(), "fonts/fontawesome-webfont.ttf");

        buttonLogin.setTypeface(sstFontRoman);
        buttonLoginText.setTypeface(sstFontRoman);
        buttonSignUp.setTypeface(sstFontRoman);
        textViewStepPrompt.setTypeface(sstFontRoman);
        editTextOne.setTypeface(sstFontRoman);
        textViewOne.setTypeface(sstFontRoman);
        inputIconOne.setTypeface(fontAwesome);
        editTextTwo.setTypeface(sstFontRoman);
        textViewTwo.setTypeface(sstFontRoman);
        inputIconTwo.setTypeface(fontAwesome);
        editTextThree.setTypeface(sstFontRoman);
        textViewThree.setTypeface(sstFontRoman);
        inputIconThree.setTypeface(fontAwesome);
        buttonMainCreds.setTypeface(sstFontRoman);
        buttonBackCreds.setTypeface(sstFontRoman);
        textForMainButton.setTypeface(sstFontRoman);
        editTextHidden2.setTypeface(sstFontRoman);
        editTextHidden3.setTypeface(sstFontRoman);
        textViewHidden2.setTypeface(fontAwesome);
        textViewHidden3.setTypeface(fontAwesome);
        psbaAlertTitle.setTypeface(sstFontBold);
        psbaAlertBody.setTypeface(sstFontRoman);
//        customSpinnerRow.setTypeface(sstFontRoman);
    }

    public void findViews() {
        relay = (RelativeLayout) findViewById(R.id.creds_relay);
        imageViewBrandTitle = (ImageView) findViewById(R.id.creds_brand_title_image_view);
        buttonLogin = (Button) findViewById(R.id.creds_login_button);
        buttonLoginText = (TextView) findViewById(R.id.creds_login_button_text);
        buttonSignUp = (Button) findViewById(R.id.creds_sign_up_button);
        viewLoginUnderline = findViewById(R.id.creds_login_underline);
        viewSignupUnderline = findViewById(R.id.creds_signup_underline);
        viewMovingUnderline = findViewById(R.id.creds_moving_underline);
        textViewStepPrompt = (TextView) findViewById(R.id.creds_text_view_step_prompt);
        editTextOne = (EditText) findViewById(R.id.creds_edit_text_one);
        textViewOne = (TextView) findViewById(R.id.creds_text_view_one);
        inputIconOne = (TextView) findViewById(R.id.creds_input_icon_1);
        editTextTwo = (EditText) findViewById(R.id.creds_edit_text_two);
        textViewTwo = (TextView) findViewById(R.id.creds_text_view_two);
        inputIconTwo = (TextView) findViewById(R.id.creds_input_icon_2);
        editTextThree = (EditText) findViewById(R.id.creds_edit_text_three);
        textViewThree = (TextView) findViewById(R.id.creds_text_view_three);
        inputIconThree = (TextView) findViewById(R.id.creds_input_icon_3);
        buttonMainCreds = (Button) findViewById(R.id.creds_main_button);
        buttonBackCreds = (Button) findViewById(R.id.creds_back_button);
        textForMainButton = (TextView) findViewById(R.id.creds_main_button_text);
        editTextHidden2 = (EditText) findViewById(R.id.creds_secret_edit_text_two);
        editTextHidden3 = (EditText) findViewById(R.id.creds_secret_edit_text_three);
        textViewHidden2 = (TextView) findViewById(R.id.creds_secret_input_icon_2);
        textViewHidden3 = (TextView) findViewById(R.id.creds_secret_input_icon_3);

        psbaAlertOverlay = findViewById(R.id.psba_alert_overlay);
        psbaAlert = (RelativeLayout) findViewById(R.id.psba_alert);
        psbaAlertGif = (GifImageView) findViewById(R.id.psba_alert_gif);
        psbaAlertTitle = (TextView) findViewById(R.id.psba_alert_title);
        psbaAlertBody = (TextView) findViewById(R.id.psba_alert_body);

        spinner1 = (Spinner) findViewById(R.id.spinner1);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        spinner3 = (Spinner) findViewById(R.id.spinner3);

        customSpinnerRow = (TextView) findViewById(R.id.cust_spinner_view);

        triangle1 = (TextView) findViewById(R.id.down_triangle1);
        triangle2 = (TextView) findViewById(R.id.down_triangle2);
        triangle3 = (TextView) findViewById(R.id.down_triangle3);
    }

    public void setInitialUIPosAndSize() {
        // set view position and size values //
        imageViewBrandTitle.setY(titleSlot);

        buttonLogin.getLayoutParams().height = (int) (loginButtonheight);
        buttonLogin.getLayoutParams().width = (int) (widthPixels * 0.5) - (dpToPx(41));
        buttonLogin.requestLayout();
        //set text to "" so we can see the blue
        buttonLogin.setText("");

        buttonLoginText.getLayoutParams().height = (int) (loginButtonheight);
        buttonLoginText.getLayoutParams().width = (int) (widthPixels * 0.5) - (dpToPx(41));
        buttonLoginText.requestLayout();

        buttonSignUp.getLayoutParams().height = (int) (loginButtonheight);
        buttonSignUp.getLayoutParams().width = (int) (widthPixels * 0.5) - (dpToPx(41));
        buttonSignUp.requestLayout();

        viewLoginUnderline.getLayoutParams().width = (int) (widthPixels * 0.5) - (dpToPx(41));

        viewSignupUnderline.getLayoutParams().width = (int) (widthPixels * 0.5) - (dpToPx(41));

        viewMovingUnderline.getLayoutParams().width = (int) (widthPixels * 0.5) - (dpToPx(41));
        viewMovingUnderline.setX(dpToPx(35));

        editTextOne.getLayoutParams().height = (int) editTextHeight;
        editTextOne.requestLayout();
        editTextOne.setY(inputHeightSlotOne);
        editTextOne.setX(-widthPixels);

        textViewOne.getLayoutParams().height = (int) editTextHeight;
        textViewOne.getLayoutParams().width = (int) hintTextViewWidth;
        textViewOne.requestLayout();
        textViewOne.setY(inputHeightSlotOne);

        inputIconOne.getLayoutParams().height = (int) editTextHeight;
        inputIconOne.getLayoutParams().width = widthPixels;
        inputIconOne.requestLayout();
        inputIconOne.setText("");
        inputIconOne.setY(inputHeightSlotOne);

        editTextTwo.getLayoutParams().height = (int) editTextHeight;
        editTextTwo.requestLayout();
        editTextTwo.setY(inputHeightSlotTwo);

        textViewTwo.getLayoutParams().height = (int) editTextHeight;
        textViewTwo.getLayoutParams().width = (int) hintTextViewWidth;
        textViewTwo.requestLayout();
        textViewTwo.setX(inputHintInX);
        textViewTwo.setY(inputHeightSlotTwo);

        inputIconTwo.getLayoutParams().height = (int) editTextHeight;
        inputIconTwo.getLayoutParams().width = (int) editTextWidth;
        inputIconTwo.requestLayout();
        inputIconTwo.setText(profileIconString);
        inputIconTwo.setY(inputHeightSlotTwo);

        editTextThree.getLayoutParams().height = (int) editTextHeight;
        editTextThree.requestLayout();
        editTextThree.setY(inputHeightSlotThree);

        textViewThree.getLayoutParams().height = (int) editTextHeight;
        textViewThree.getLayoutParams().width = (int) hintTextViewWidth;
        textViewThree.requestLayout();
        textViewThree.setX(inputHintInX);
        textViewThree.setY(inputHeightSlotThree);

        inputIconThree.getLayoutParams().height = (int) editTextHeight;
        inputIconThree.getLayoutParams().width = (int) editTextWidth;
        inputIconThree.requestLayout();
        inputIconThree.setText(lockIconString);
        inputIconThree.setY(inputHeightSlotThree);

        editTextHidden2.getLayoutParams().height = (int) editTextHeight;
        editTextHidden2.getLayoutParams().width = widthPixels - dpToPx(64);
        editTextHidden2.requestLayout();
        editTextHidden2.setX(-widthPixels);
        editTextHidden2.setY(inputHeightSlotTwo);

        editTextHidden3.getLayoutParams().height = (int) editTextHeight;
        editTextHidden3.getLayoutParams().width = widthPixels - dpToPx(64);
        editTextHidden3.requestLayout();
        editTextHidden3.setX(-widthPixels);
        editTextHidden3.setY(inputHeightSlotThree);

        textViewHidden2.getLayoutParams().height = (int) editTextHeight;
        textViewHidden2.getLayoutParams().width = (int) editTextWidth;
        textViewHidden2.requestLayout();
        textViewHidden2.setText(groupProfileIconString);
        textViewHidden2.setX(-widthPixels);
        textViewHidden2.setY(inputHeightSlotTwo);

        textViewHidden3.getLayoutParams().height = (int) editTextHeight;
        textViewHidden3.getLayoutParams().width = (int) editTextWidth;
        textViewHidden3.requestLayout();
        textViewHidden3.setText(groupProfileIconString);
        textViewHidden3.setX(-widthPixels);
        textViewHidden3.setY(inputHeightSlotThree);

        buttonMainCreds.getLayoutParams().height = (int) mainButtonHeight;
        buttonMainCreds.getLayoutParams().width = widthPixels - dpToPx(64);
        buttonMainCreds.requestLayout();
        buttonMainCreds.setX(dpToPx(32));
        buttonMainCreds.setY(mainButtonSlot);
        buttonMainCreds.setAlpha(0.3f);

        buttonBackCreds.getLayoutParams().height = (int) mainButtonHeight;
        buttonBackCreds.getLayoutParams().width = (int) backButtonWidth;
        buttonBackCreds.requestLayout();
        buttonBackCreds.setY(mainButtonSlot);
        buttonBackCreds.setX(dpToPx(32));
        buttonBackCreds.setAlpha(0.0f);

        textForMainButton.getLayoutParams().height = (int) mainButtonHeight;
        textForMainButton.getLayoutParams().width = (int) backButtonWidth;
        textForMainButton.setX(widthPixels * 0.5f - backButtonWidth * 0.5f);
        textForMainButton.setY(mainButtonSlot);
        textForMainButton.setZ(100);
        //string
        textForMainButton.setText("LOGIN");

        psbaAlert.getLayoutParams().width = widthPixels - dpToPx(50);
        psbaAlert.setY(100);
        psbaAlert.setX(widthPixels);
        psbaAlert.setZ(102);

        psbaAlertOverlay.setZ(101);


//        spinner1.getLayoutParams().height = (int) editTextHeight;
//        spinner1.requestLayout();
//        spinner1.setY(inputHeightSlotOne);

        SpinnerArrayAdapter adapter = new SpinnerArrayAdapter(this, R.layout.spinner_row, items);
        spinner1.setAdapter(adapter);
//        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Log.v("XYZ_FUTURE:::", "onItemSelected in the spinner" + (String) parent.getItemAtPosition(position));
//            }
//
//            @Override public void onNothingSelected(AdapterView<?> parent) {
//                // TODO Auto-generated method stub
//            }
//
//        });

        triangle1.getLayoutParams().height = (int) editTextHeight;
        triangle1.requestLayout();
        triangle1.setY(inputHeightSlotOne);
        triangle1.setX(widthPixels - dpToPx(64));
        triangle1.setAlpha(0.0f);

        triangle2.getLayoutParams().height = (int) editTextHeight;
        triangle2.requestLayout();
        triangle2.setY(inputHeightSlotTwo);
        triangle2.setX(widthPixels - dpToPx(64));
        triangle2.setAlpha(0.0f);

        triangle3.getLayoutParams().height = (int) editTextHeight;
        triangle3.requestLayout();
        triangle3.setY(inputHeightSlotThree);
        triangle3.setX(widthPixels - dpToPx(64));
        triangle3.setAlpha(0.0f);

    }

    public void slideInAlert(String title, String body, final boolean bgVisible, int timeOpen) {
        if (bgVisible == true) {
            psbaAlertOverlay.animate().alpha(0.4f);
            psbaAlertOverlay.setClickable(true);
        }

        psbaAlert.animate().translationX(widthPixels - psbaAlert.getWidth());
        psbaAlertTitle.setText(title);
        psbaAlertBody.setText(body);

        Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {
                slideOutAlert(bgVisible);
            }
        };
        handler.postDelayed(r, timeOpen);
    }

    public void slideOutAlert(boolean bgVisible) {
        if (bgVisible == true) {
            psbaAlertOverlay.animate().alpha(0.0f);
            psbaAlertOverlay.setClickable(false);
        }
        psbaAlert.animate().translationX(widthPixels);
    }

    // API //
    public void psbaAPIRegisterUser() {

        AsyncTask asyncTask = new AsyncTask() {
            @Override protected Object doInBackground(Object[] params) {
                OkHttpClient client = new OkHttpClient();

                RequestBody formBody = new FormBody.Builder()
                        .add("username", "PixelBeard")
                        .add("password", "Password1")
                        .build();
                Request request = new Request.Builder()
                        .url("http://psba.pixelbeard.co/api/users/login")
                        .addHeader("Content-type", "application/x-www-form-urlencoded")
                        .post(formBody)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    Log.v("XYZ_FUTURE::: ", "try");
                    JSONObject testJSON = new JSONObject(response.body().string());
                    Log.v("XYZ_FUTURE::: ", "try2");
                    HashMap<String, String> loginResponse;
                    loginResponse = HQ.getInstance().jsonToMap(testJSON.toString());
                    Log.v("XYZ_FUTURE::: ", "login response" + loginResponse);

                    if (loginResponse.get("success").matches("false")) {
                        return "success";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return "fail";
            }

            @Override protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                if (o.equals("success")) {
                    Log.v("XYZ_FUTURE::: ", "onPostExecute of beepingAPILogin CONSTANTS");

                } else {
                    Toast toast = Toast.makeText(CredsActivity.this, "Failed to login. Please try again.", Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        };
        asyncTask.execute();
    }

    public void psbaAPILogin() {

        final String username = String.valueOf(editTextTwo.getText());
        final String password = String.valueOf(editTextThree.getText());
        Log.v("XYZ_FUTURE::: ", "userpass" + username + password);


        AsyncTask asyncTask = new AsyncTask() {
            @Override protected Object doInBackground(Object[] params) {
                OkHttpClient client = new OkHttpClient();

                RequestBody formBody = new FormBody.Builder()
                        .add("username", username)
                        .add("password", password)
                        .build();
                Request request = new Request.Builder()
                        .url("http://psba.pixelbeard.co/api/users/login")
                        .addHeader("Content-type", "application/x-www-form-urlencoded")
                        .post(formBody)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    Log.v("XYZ_FUTURE::: ", "response from login is: " + response);
                    JSONObject responseJSON = new JSONObject(response.body().string());
                    Log.v("XYZ_FUTURE::: ", "responseJSON from login is: " + responseJSON);
                    String success = responseJSON.getString("success");

                    if (success.matches("true")) {

                        String userJSONObject = responseJSON.getString("user");
                        HashMap<String, String> userHash = HQ.getInstance().jsonToMap(userJSONObject.toString());
                        Log.v("XYZ_FUTURE::: ", "user hash is: " + userHash);

                        HQ.getInstance().theUser = new User(
                                userHash.get("firstname"),
                                userHash.get("lastname"),
                                userHash.get("username"),
                                userHash.get("psnid"),
                                userHash.get("email"),
                                userHash.get("company"),
                                userHash.get("branch"),
                                userHash.get("occupation"),
                                userHash.get("points"),
                                userHash.get("avatar"),
                                userHash.get("user_id"),
                                userHash.get("modulescompleted"),
                                userHash.get("company_name"));

                        HQ.getInstance().populateUserPrefsWithLogin();

                        return "success";
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return "fail";
            }

            @Override protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                if (o.equals("success")) {
                    Toast toastLogin = Toast.makeText(CredsActivity.this, "You are logged in.", Toast.LENGTH_LONG);
                    toastLogin.show();

                    Intent intent = new Intent(CredsActivity.this, MainActivity.class);
                    intent.putExtra("fromLogin", true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.slide_out_down);

                } else {
                    slideInAlert("FAILURE", "We were unable to log you in with those details.", true, 3000);
                }
            }
        };
        asyncTask.execute();
    }

    public void psbaAPICheckUsername() {
        AsyncTask asyncTask = new AsyncTask() {
            @Override protected Object doInBackground(Object[] params) {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://psba.pixelbeard.co/api/users/check/username/PixelBeard")
                        .build();
                try {
                    okhttp3.Response response = client.newCall(request).execute();
                    JSONObject testJSON = new JSONObject(response.body().string());
                    HashMap<String, String> fetchUserResponse;
                    fetchUserResponse = HQ.getInstance().jsonToMap(testJSON.toString());
                    if (fetchUserResponse.get("success").matches("false")) {
                        return "not avail";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return "avail";
            }
            @Override protected void onPostExecute(Object o) {
                if (o.equals("avail")) {

                } else {
                    editTextTwo.setBackground(ContextCompat.getDrawable(CredsActivity.this, R.drawable.edit_text_border_red));
                    slideInAlert("WARNING", "That username is already in use.", false, 2000);
                }
            }
        };
        asyncTask.execute();
    }

    public void psbaAPICheckEmail() {
        AsyncTask asyncTask = new AsyncTask() {
            @Override protected Object doInBackground(Object[] params) {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://psba.pixelbeard.co/api/users/check/email/will@pixelbeard.co")
                        .build();
                try {
                    Log.v("XYZ_FUTURE::: ", "try1 in psbaAPICheckUsername");
                    okhttp3.Response response = client.newCall(request).execute();
                    Log.v("XYZ_FUTURE::: ", "try2 in psbaAPICheckUsername");
                    JSONObject testJSON = new JSONObject(response.body().string());
                    Log.v("XYZ_FUTURE::: ", "try3 in psbaAPICheckUsername");
                    HashMap<String, String> fetchUserResponse;
                    fetchUserResponse = HQ.getInstance().jsonToMap(testJSON.toString());

                    if (fetchUserResponse.get("success").matches("false")) {

                        return "success";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return "fail";
            }
            @Override protected void onPostExecute(Object o) {

            }
        };
        asyncTask.execute();
    }

    public void psbaAPIGetCompanies() {
        Log.v("XYZ_FUTURE::: ", "psbaAPIGetCompanies");
        AsyncTask asyncTask = new AsyncTask() {

            @Override protected Object doInBackground(Object[] params) {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://psba.pixelbeard.co/api/companies")
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    JSONObject responseJSON = new JSONObject(response.body().string());
                    String success = responseJSON.getString("success");

                    if (success.matches("true")) {
                        JSONArray companyList = responseJSON.getJSONArray("companies");
//                        Log.v("XYZ_FUTURE::: ", "test companyList: " + companyList);

                        for (int i = 0; i < companyList.length(); i++) {
                            JSONObject individualCompany = (JSONObject) companyList.get(i);
                            HashMap<String, String> singleCoHash = HQ.getInstance().jsonToMap(individualCompany.toString());
                            Company co = new Company();
                            co.setCompanyName(singleCoHash.get("name"));
                            co.setCompanyId(singleCoHash.get("id"));
                            co.setCompanyActive(singleCoHash.get("active"));
                            co.setCompanyOrder(singleCoHash.get("order"));
                            co.setCompanyImage(singleCoHash.get("image"));
                            HQ.getInstance().companies.add(co);
                            Log.v("XYZ_FUTURE::: ", "getCompanyNameList: " + HQ.getInstance().getCompanyNameList());
                        }

                        return "success";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return "fail";
            }
            @Override protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                if (o.equals("success")) {
                    Toast toastLogin = Toast.makeText(CredsActivity.this, "got companies", Toast.LENGTH_LONG);
                    toastLogin.show();


                } else {
//                    slideInAlert("FAILURE", "You have no network connection, please try again later.", true, 3000);
                }
            }
        };
        asyncTask.execute();
    }

    public void psbaAPIGetCompanyExtrasByID(final String companyId) {

        AsyncTask asyncTask = new AsyncTask() {
            @Override protected Object doInBackground(Object[] params) {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://psba.pixelbeard.co/api/companies/" + companyId)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    JSONObject testJSON = new JSONObject(response.body().string());
                    HashMap<String, String> fetchUserResponse;
                    fetchUserResponse = HQ.getInstance().jsonToMap(testJSON.toString());

                    if (fetchUserResponse.get("success").matches("true")) {

                        return "success";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return "fail";
            }
            @Override protected void onPostExecute(Object o) {
                if (o.equals("success")) {

                }
            }
        };
        asyncTask.execute();
    }


    private class SpinnerArrayAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        public SpinnerArrayAdapter(CredsActivity con, int spinner_row, String[] items) {
            mInflater = LayoutInflater.from(con);
        }

        @Override public int getCount() {
            return items.length;
        }

        @Override public Object getItem(int position) {
            Log.v("XYZ_FUTURE::: ", "getItem position is: " + position);
            return position;
        }

        @Override public long getItemId(int position) {
            Log.v("XYZ_FUTURE::: ", "getItemId position is: " + position);
            return position;
        }

        @Override public View getView(int position, View convertView, ViewGroup parent) {
            final ListContent holder;
            View v = convertView;
            if (v == null) {
                v = mInflater.inflate(R.layout.spinner_row, null);
                holder = new ListContent();
                holder.name = (TextView) v.findViewById(R.id.cust_spinner_view);
                v.setTag(holder);
            } else {
                holder = (ListContent) v.getTag();
            }
            holder.name.setTypeface(sstFontRoman);
            holder.name.setText("" + items[position]);

            return v;
        }
    }

    static class ListContent {
        TextView name;
    }

}
