package co.pixelbeard.psba_android;

import android.animation.Animator;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import co.pixelbeard.psba_android.Modules.Profile.Frags.ProfileModuleFragment;
import co.pixelbeard.psba_android.Modules.Store.Frags.ProductFragment;
import co.pixelbeard.psba_android.Modules.Store.Frags.StoreModuleFragment;
import co.pixelbeard.psba_android.Modules.Training.Frags.TrainingModuleFragment;
import co.pixelbeard.psba_android.Wizards.KeyboardWizard;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, ProfileModuleFragment.FragmentListener, StoreModuleFragment.FragmentListener, ProductFragment.FragmentListener, TrainingModuleFragment.FragmentListener {

    public RelativeLayout relay;
    public EditText testInput;

    private TextView textNavTitle;
    private TextView textNavSubtitle;
    private Button buttonLeftNav;
    private Button buttonInsideRightNav;
    private Button buttonRightNav;

    private LinearLayout linlayTabBar;
    private Button leftTabButton;
    private Button leftMidTabButton;
    private Button midTabButton;
    private Button rightMidTabButton;
    private Button rightTabButton;

    int heightPixels;
    int widthPixels;

    private FrameLayout fragFrame;
    ProfileModuleFragment frag;
    android.support.v4.app.FragmentTransaction transaction;

    private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);

    public static final String TAG = MainActivity.class.getSimpleName();

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        // pixels, dpi //
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        heightPixels = metrics.heightPixels;
        widthPixels = metrics.widthPixels;
        int densityDpi = metrics.densityDpi;
        float xdpi = metrics.xdpi;
        float ydpi = metrics.ydpi;
        Log.i("XYZ", "widthPixels  = " + widthPixels);
        Log.i("XYZ", "heightPixels = " + heightPixels);

        // orientation (either ORIENTATION_LANDSCAPE, ORIENTATION_PORTRAIT) //
        int orientation = getResources().getConfiguration().orientation;
        Log.i("XYZ", "orientation  = " + orientation);

        relay = (RelativeLayout) findViewById(R.id.rootLayout);

        // nav bar //
        textNavTitle = (TextView) findViewById(R.id.text_store_nav_bar_title);
        textNavSubtitle = (TextView) findViewById(R.id.text_store_nav_bar_sub_title);
        buttonLeftNav = (Button) findViewById(R.id.button_left_nav_bar);
        buttonInsideRightNav = (Button) findViewById(R.id.button_inside_right_nav_bar);
        buttonRightNav = (Button) findViewById(R.id.button_right_nav_bar);


        // tab bar //
        leftTabButton = (Button) findViewById(R.id.left_tab_button);
        leftMidTabButton = (Button) findViewById(R.id.mid_left_tab_button);
        midTabButton = (Button) findViewById(R.id.mid_tab_button);
        rightMidTabButton = (Button) findViewById(R.id.mid_right_tab_button);
        rightTabButton = (Button) findViewById(R.id.right_tab_button);

        leftTabButton.setOnClickListener(this);
        leftMidTabButton.setOnClickListener(this);
        midTabButton.setOnClickListener(this);
        rightMidTabButton.setOnClickListener(this);
        rightTabButton.setOnClickListener(this);

        KeyboardWizard.addKeyboardToggleListener(this, new KeyboardWizard.SoftKeyboardToggleListener() {
            public void onToggleSoftKeyboard(boolean isVisible) {
//                Log.v("XYZ", "keyboard visible: " + isVisible);
            }
        });

        fragFrame = (FrameLayout) findViewById(R.id.fragment_frame);
        linlayTabBar = (LinearLayout) findViewById(R.id.linlay_tab_bar_main);

        SharedPreferences userPrefs = getSharedPreferences("theUser", Context.MODE_PRIVATE);
        boolean isLoggedIn = userPrefs.getBoolean("isLoggedIn", false);

        boolean fromLogin = getIntent().getBooleanExtra("fromLogin", false);
        boolean fromEditProfile = getIntent().getBooleanExtra("fromEditProfile", false);

        Log.v("XYZ_FUTURE::: ", "fromLogin is: " + fromLogin + " :: isLoggedIn: " + isLoggedIn);

        if (!fromEditProfile) {
            HQ.getInstance().populateTheUserObject();
        }

        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        ProfileModuleFragment profFrag = (ProfileModuleFragment) fm.findFragmentById(R.id.fragment_frame);
        transaction = fm.beginTransaction();
        if (profFrag == null) {
            profFrag = profFrag.newInstance(widthPixels, heightPixels);
            transaction.add(R.id.fragment_frame, profFrag);
        }
        transaction.commit();
    }



    @Override protected void onDestroy() {
        super.onDestroy();

    }

    @Override public void onClick(final View view) {
        view.animate().alpha(0.0f).setDuration(100).setListener(new Animator.AnimatorListener() {
            @Override public void onAnimationStart(Animator animation) {}
            @Override public void onAnimationEnd(Animator animation) {
                view.animate().alpha(1.0f);
//                view.setBackgroundColor(getResources().getColor(R.color.colorClear));
            }
            @Override public void onAnimationCancel(Animator animation) {}
            @Override public void onAnimationRepeat(Animator animation) {}
        });
        switch (view.getId()) {
            case R.id.left_tab_button:
                // replace fragment //
                ProfileModuleFragment profileModuleFragment = new ProfileModuleFragment().newInstance(widthPixels, heightPixels);
                profileModuleFragment.newInstance(widthPixels, heightPixels);
                if(getFragmentManager().findFragmentById(R.id.fragment_frame) != null) {
                    getFragmentManager().beginTransaction().detach(getFragmentManager().findFragmentById(R.id.fragment_frame)).addToBackStack(TAG).commit();
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_frame, profileModuleFragment).commit();
                break;
            case R.id.mid_left_tab_button:
                // replace fragment //
                TrainingModuleFragment trainingModuleFragment = new TrainingModuleFragment().newInstance(widthPixels, heightPixels);
                trainingModuleFragment.newInstance(widthPixels, heightPixels);
                if(getFragmentManager().findFragmentById(R.id.fragment_frame) != null) {
                    getFragmentManager().beginTransaction().detach(getFragmentManager().findFragmentById(R.id.fragment_frame)).addToBackStack(TAG).commit();
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_frame, trainingModuleFragment).commit();
                break;
            case R.id.mid_tab_button:
                // replace fragment //
                StoreModuleFragment storeModuleFragment = new StoreModuleFragment().newInstance(widthPixels, heightPixels);
                storeModuleFragment.newInstance(widthPixels, heightPixels);
                if(getFragmentManager().findFragmentById(R.id.fragment_frame) != null) {
                    getFragmentManager().beginTransaction().detach(getFragmentManager().findFragmentById(R.id.fragment_frame)).addToBackStack(TAG).commit();
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_frame, storeModuleFragment).commit();
                break;
            case R.id.mid_right_tab_button:
                Toast toast4 = Toast.makeText(this, "4th tab", Toast.LENGTH_LONG);
                toast4.show();
                break;
            case R.id.right_tab_button:
                Toast toast5 = Toast.makeText(this, "5th tab", Toast.LENGTH_LONG);
                toast5.show();
                break;
            default:
                //code..
                break;
        }

    }

    //profile frag interface
    @Override public void onFragmentFinish(String testString1, String testString2) {

    }

    @Override public void removeTabBar() {
//        linlayTabBar.animate().translationYBy(getResources().getDimension(R.dimen.tabbar_height));
//        fragFrame.setY((int)getResources().getDimension(R.dimen.navbar_height));
//        fragFrame.getLayoutParams().height = heightPixels - (int)getResources().getDimension(R.dimen.navbar_height);
//        fragFrame.requestLayout();
    }

    @Override
    public void displayTabBar() {
//        linlayTabBar.animate().translationYBy(-getResources().getDimension(R.dimen.tabbar_height));
//        fragFrame.setY((int)getResources().getDimension(R.dimen.navbar_height));
//        fragFrame.getLayoutParams().height = heightPixels - (int)getResources().getDimension(R.dimen.navbar_height) - (int)getResources().getDimension(R.dimen.tabbar_height);
    }

    @Override public void onTest(int intTest) {

    }

    @Override public void onBackPressed() {

    }


}
