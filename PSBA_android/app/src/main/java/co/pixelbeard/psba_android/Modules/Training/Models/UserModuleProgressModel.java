package co.pixelbeard.psba_android.Modules.Training.Models;

/**
 * Created by AE1 on 8/31/17.
 */

public class UserModuleProgressModel {

    public String moduleId;
    public int total_questions;
    public int correct_answers;
    public int total_attempts;
    public boolean passed;

    public UserModuleProgressModel() {
        //empty constr
    }

    public UserModuleProgressModel(String moduleId, int total_questions, int correct_answers, int total_attempts, boolean passed) {
        this.moduleId = moduleId;
        this.total_questions = total_questions;
        this.correct_answers = correct_answers;
        this.total_attempts = total_attempts;
        this.passed = passed;
    }
}
