package co.pixelbeard.psba_android.Modules.Profile.Acts;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.widget.NestedScrollView;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;

import co.pixelbeard.psba_android.MainActivity;
import co.pixelbeard.psba_android.R;
import co.pixelbeard.psba_android.HQ;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ProfileEditActivity extends Activity {

    int heightPixels;
    int widthPixels;

    float inputHeightOnFocus;
    float inputHeightSlotOne;
    float inputHeightSlotTwo;
    float inputHeightSlotThree;
    float inputHeightSlotFour;
    float inputHeightSlotFive;
    float inputHintInX;
    float inputHintOutX;
    float inputHintAboveHeight;
    float editTextHeight;
    float editTextWidth;
    float loginButtonheight;
    float mainButtonHeight;
    float mainButtonSlot;
    float titleSlot;
    float mainButtonScaleDownValue;
    float hintTextViewWidth;
    float backButtonWidth;

    private Button buttonBack;
    private TextView textNavTitle;
    private NestedScrollView scrollView;
    private RelativeLayout relayInScrollView;
    private ImageView imageViewAvatar;
    private Button buttonEditAvatar;
    private EditText editTextOne;
    private TextView textViewOne;
    private TextView inputIconOne;
    private EditText editTextTwo;
    private TextView textViewTwo;
    private TextView inputIconTwo;
    private EditText editTextThree;
    private TextView textViewThree;
    private TextView inputIconThree;
    private EditText editTextFour;
    private TextView textViewFour;
    private TextView inputIconFour;
    private EditText editTextFive;
    private TextView textViewFive;
    private TextView inputIconFive;
    private Button buttonSave;

    private Typeface sstFontLight;
    private Typeface sstFontRoman;
    private Typeface sstFontHeavy;
    private Typeface sstFontBold;
    private Typeface sstFontCondensed;
    private Typeface sstFontCondensedBd;
    private Typeface fontAwesome;

    // FLAG isFromGallery set 1 of 3 //
    private boolean isFromGallery = false;

    private int editTextFocus;

    Uri imageUri;
    static File file5000;
    byte[] aviByteArray;

    private Bitmap theSelectedImageBitmap;
    ByteArrayOutputStream theAvatarStream = new ByteArrayOutputStream();

    Spanned lockIcon = Html.fromHtml("&#xf023;");
    Spanned envelopeIcon = Html.fromHtml("&#xf0e0;");
    Spanned profileIcon = Html.fromHtml("&#xf007;");
    Spanned pencilIcon = Html.fromHtml("&#xf040;");

    String lockIconString = " " + lockIcon + " ";
    String envelopeIconString = "" + envelopeIcon + " ";
    String profileIconString = " " + profileIcon + " ";
    String pencilIconString = " " + pencilIcon + " ";

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);

        // pixels, dpi //
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        heightPixels = metrics.heightPixels;
        widthPixels = metrics.widthPixels;

        findViews();
        setInitialValues();
        setCustomFont();
        setInitialPosition();

        setupEditTextOneListeners();
        setupEditTextTwoListeners();
        setupEditTextThreeListeners();
        setupEditTextFourListeners();
        setupEditTextFiveListeners();

        setEditTextDoneAction();


        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                onBackPressed();

//                psbaAPIUploadAvatar();

            }
        });

        buttonEditAvatar.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                // FLAG isFromGallery set 2 of 3 //
                isFromGallery = true;
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 5);
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                psbaAPIEditAccountDeets();
            }
        });
    }

    public void pickImage() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        photoPickerIntent.putExtra("crop", "true");
        photoPickerIntent.putExtra("aspectX", 0);
        photoPickerIntent.putExtra("aspectY", 0);
        try {
            photoPickerIntent.putExtra("return-data", true);
            startActivityForResult(photoPickerIntent, 1);

        } catch (ActivityNotFoundException e) {

        }
    }


    // SO //
    String picturePath;
    Uri selectedImage;
    Bitmap theChosenOne;


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 5 && resultCode == RESULT_OK) {

            selectedImage = data.getData();
            file5000 = new File(String.valueOf(selectedImage));
            Log.v("XYZ_FUTURE::: ", "file5000 is: " + file5000);
            Log.v("XYZ_FUTURE::: ", "file5000 getNAME is: " + file5000.getName());
            try {
//                theChosenOne = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                theChosenOne = handleSamplingAndRotationBitmap(this, selectedImage);//(Bitmap) data.getExtras().get("data");
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Cursor to get image uri to display

            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            Log.v("XYZ_FUTURE::: ", "columnIndex is: " + columnIndex);
            picturePath = cursor.getString(columnIndex);
            Log.v("XYZ", "5000 on act res picturePath is: " + picturePath + "\n5000getAbsolutePath  is: " + file5000.getAbsoluteFile());
            cursor.close();

            imageViewAvatar.setImageBitmap(theChosenOne);

            Log.v("XYZ_FUTURE::: ", "file5000 is: " + file5000);

            psbaAPIUploadAvatar();
        }
    }

    public void psbaAPIUploadAvatar() {

        AsyncTask asyncTask = new AsyncTask() {
            @Override protected Object doInBackground(Object[] params) {
                // Timestamp for arbitrary file name //
                Long theTimestamp = System.currentTimeMillis()/1000;
                String theTimestampString = theTimestamp.toString();
                // Convert the bitmap to data //
                byte[] theBitmapData = new byte[0];
                if (theChosenOne != null) {
                    ByteArrayOutputStream theStream = new ByteArrayOutputStream();
                    theChosenOne.compress(Bitmap.CompressFormat.PNG, 100, theStream);
                    theBitmapData = theStream.toByteArray();
                }
                // this here mine //
                OkHttpClient client = new OkHttpClient();
                RequestBody body = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("user_id", HQ.getInstance().theUser.getUser_id())
                        .addFormDataPart("userfile", theTimestampString + ".png", RequestBody.create(MediaType.parse("image/png"), theBitmapData))
                        .build();
                Request request = new Request.Builder()
                        .url("http://psba.pixelbeard.co/api/users/avatar/upload")
                        .addHeader("content-type", "multipart/form-data")
                        .post(body)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    JSONObject avatarEditJSON = new JSONObject(response.body().string());
                    Log.v("XYZ_FUTURE::: ", "avatarEditJSON" + avatarEditJSON);
                    if (avatarEditJSON.getBoolean("success")) {
                        Log.v("XYZ_FUTURE::: ", "avatarEditJSON.getString(\"new_avatar\")" + avatarEditJSON.getString("new_avatar"));
                        HQ.getInstance().theUser.setAvatar(avatarEditJSON.getString("new_avatar"));
                        return "success";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return "fail";
            }

            @Override protected void onPostExecute(Object o) {
                if (o.equals("success")) {
                    Toast toast = Toast.makeText(ProfileEditActivity.this, "Your new avatar has been saved", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    Toast toast = Toast.makeText(ProfileEditActivity.this, "There was a problem saving your new avatar, please try again", Toast.LENGTH_SHORT);
                    toast.show();

                }
            }
        };
        asyncTask.execute();
    }



    public static void copyStream(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[1024]; // Adjust if you want
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1)
        {
            output.write(buffer, 0, bytesRead);
        }
    }



    @Override protected void onResume() {
        super.onResume();
        Log.v("XYZ_FUTURE::: ", "onResume: get email:  " + HQ.getInstance().theUser.getEmail());
        editTextOne.setText(HQ.getInstance().theUser.getUsername());
        editTextTwo.setText(HQ.getInstance().theUser.getEmail());
        editTextThree.setText(HQ.getInstance().theUser.getPsnid());
        editTextFour.setText("Change Password");
        editTextFive.setText("Confirm New Password");

        Log.v("XYZ_FUTURE::: ", "HQ.getInstance().theUser.getAvatar() is: " + HQ.getInstance().theUser.getAvatar());



        if (isFromGallery == false) {
            if (HQ.getInstance().theUser.getAvatar().matches("") || HQ.getInstance().theUser.getAvatar() == null || HQ.getInstance().theUser.getAvatar().matches("default.png")) {
                Picasso.with(this).load(HQ.getInstance().theAvatarURL + "default-avatar.jpg").fit().centerCrop().into(imageViewAvatar);
            } else {
                Picasso.with(this).load(HQ.getInstance().theAvatarURL + HQ.getInstance().theUser.getAvatar()).fit().centerCrop().into(imageViewAvatar);
            }
        }
        // FLAG isFromGallery set 3 of 3 //
        isFromGallery = false;
    }

    public void setEditTextDoneAction() {
        editTextOne.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch(result) {
                    case EditorInfo.IME_ACTION_DONE:
                        view.clearFocus();
                        editTextFocus = 0;
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        // next stuff
                        break;
                }
                return true;
            }
        });
        editTextTwo.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch(result) {
                    case EditorInfo.IME_ACTION_DONE:
                        view.clearFocus();
                        editTextFocus = 0;
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        // next stuff
                        break;
                }
                return true;
            }
        });
        editTextThree.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch(result) {
                    case EditorInfo.IME_ACTION_DONE:
                        view.clearFocus();
                        editTextFocus = 0;
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        // next stuff
                        break;
                }
                return true;
            }
        });
        editTextFour.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch(result) {
                    case EditorInfo.IME_ACTION_DONE:
                        view.clearFocus();
                        editTextFocus = 0;
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        // next stuff
                        break;
                }
                return true;
            }
        });
        editTextFive.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch(result) {
                    case EditorInfo.IME_ACTION_DONE:
                        view.clearFocus();
                        editTextFocus = 0;
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        // next stuff
                        break;
                }
                return true;
            }
        });
    }

    public void setupEditTextOneListeners() {
        editTextOne.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v1, boolean hasFocus) {

                if (hasFocus) {
                    Log.v("XYZ_FUTURE::: ", "EditTextTwo has focus");
                    editTextFocus = 1;
                    editTextOne.setCursorVisible(true);
                    animateEditTextOnFocus(1, true);
//                    editTextOne.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
                    textViewOne.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotOne - (inputHintAboveHeight)).translationX(-textViewOne.getWidth() * 0.5f * 0.35f);

                } else if (!hasFocus) {
                    Log.v("XYZ_FUTURE::: ", "EditTextTwo NO focus and text is: " + editTextOne.getText().toString());
                    editTextFocus = 0;
                    editTextOne.setCursorVisible(false);
                    animateEditTextOnFocus(1, false);

                    if (editTextOne.getText().toString().matches("")) {
                        editTextOne.setCursorVisible(false);
//                        editTextOne.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border_gray));
//                        textViewOne.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotOne).translationX(inputHintInX);
                    } else {
                        editTextOne.setCursorVisible(false);
//                        editTextOne.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
//                        textViewOne.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotOne - (inputHintAboveHeight)).translationX(-textViewOne.getWidth() * 0.5f * 0.35f);
                    }

                }
            }
        });
    }

    public void setupEditTextTwoListeners() {
        editTextTwo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v2, boolean hasFocus) {
                if (hasFocus) {
                    Log.v("XYZ_FUTURE::: ", "EditTextTwo has focus");
                    editTextFocus = 2;
//                    focusOnInput(editTextFocus);

                    editTextTwo.setCursorVisible(true);
                    animateEditTextOnFocus(2, true);

                } else if (!hasFocus) {
                    Log.v("XYZ_FUTURE::: ", "EditTextTwo NO focus");
                    editTextFocus = 0;
                    editTextTwo.setCursorVisible(false);
                    animateEditTextOnFocus(2, false);

                    if (editTextTwo.getText().toString().matches("")) {
                        editTextTwo.setCursorVisible(false);
//                        editTextTwo.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border_gray));
//                        textViewTwo.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotTwo).translationX(inputHintInX);
                    } else {
                        editTextTwo.setCursorVisible(false);
//                        editTextTwo.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
//                        textViewTwo.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotTwo - (inputHintAboveHeight)).translationX(-textViewTwo.getWidth() * 0.5f * 0.35f);
                    }
                }
            }
        });
    }

    public void setupEditTextThreeListeners() {
        editTextThree.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v2, boolean hasFocus) {
                if (hasFocus) {
                    Log.v("XYZ_FUTURE::: ", "EditTextThree has focus");
                    editTextFocus = 3;
//                    focusOnInput(editTextFocus);

                    editTextThree.setCursorVisible(true);
                    animateEditTextOnFocus(3, true);
//                    editTextThree.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
//                    textViewThree.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotThree - (inputHintAboveHeight)).translationX(-textViewOne.getWidth() * 0.5f * 0.35f);

                } else if (!hasFocus) {
                    Log.v("XYZ_FUTURE::: ", "EditTextThree NO focus");
                    editTextFocus = 0;
                    editTextThree.setCursorVisible(false);
                    animateEditTextOnFocus(3, false);

                    if (editTextThree.getText().toString().matches("")) {
                        editTextThree.setCursorVisible(false);
//                        editTextThree.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border_gray));
//                        textViewThree.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotThree).translationX(inputHintInX);
                    } else {
                        editTextThree.setCursorVisible(false);
//                        editTextThree.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
//                        textViewThree.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotThree - (inputHintAboveHeight)).translationX(-textViewThree.getWidth() * 0.5f * 0.35f);
                    }
                }
            }
        });
    }

    public void setupEditTextFourListeners() {
        editTextFour.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v2, boolean hasFocus) {
                if (hasFocus) {
                    editTextFour.setText("");
                    Log.v("XYZ_FUTURE::: ", "EditTextFour has focus");
                    editTextFocus = 4;
//                    focusOnInput(editTextFocus);

                    editTextFour.setCursorVisible(true);
                    animateEditTextOnFocus(4, true);
//                    editTextFour.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
//                    textViewFour.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotFour - (inputHintAboveHeight)).translationX(-textViewOne.getWidth() * 0.5f * 0.35f);

                } else if (!hasFocus) {
                    Log.v("XYZ_FUTURE::: ", "EditTextFour NO focus");
                    editTextFocus = 0;
                    editTextFour.setCursorVisible(false);
                    animateEditTextOnFocus(4, false);

                    if (editTextFour.getText().toString().matches("")) {
                        editTextFour.setCursorVisible(false);
//                        editTextFour.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border_gray));
//                        textViewFour.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotFour).translationX(inputHintInX);
                    } else {
                        editTextFour.setCursorVisible(false);
//                        editTextFour.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
//                        textViewFour.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotFour - (inputHintAboveHeight)).translationX(-textViewThree.getWidth() * 0.5f * 0.35f);
                    }
                }
            }
        });
    }

    public void setupEditTextFiveListeners() {
        editTextFive.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v2, boolean hasFocus) {
                if (hasFocus) {
                    editTextFive.setText("");
                    Log.v("XYZ_FUTURE::: ", "EditTextFive has focus");
                    editTextFocus = 5;
//                    focusOnInput(editTextFocus);
                    editTextFive.animate().translationYBy(dpToPx(-300));
                    editTextFive.setCursorVisible(true);
                    animateEditTextOnFocus(5, true);
//                    editTextFive.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
//                    textViewFive.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotFive - (inputHintAboveHeight)).translationX(-textViewOne.getWidth() * 0.5f * 0.35f);

                } else if (!hasFocus) {
                    Log.v("XYZ_FUTURE::: ", "EditTextFive NO focus");
                    editTextFocus = 0;
                    editTextFive.setCursorVisible(false);
                    animateEditTextOnFocus(5, false);

                    if (editTextFive.getText().toString().matches("")) {
                        editTextFive.setCursorVisible(false);
//                        editTextFive.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border_gray));
//                        textViewFive.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotFive).translationX(inputHintInX);
                    } else {
                        editTextFive.setCursorVisible(false);
//                        editTextFive.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
//                        textViewFive.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotFour - (inputHintAboveHeight)).translationX(-textViewThree.getWidth() * 0.5f * 0.35f);
                    }
                }
            }
        });
    }

    public void findViews() {
        buttonBack = (Button) findViewById(R.id.edit_profile_button_back);
        textNavTitle = (TextView) findViewById(R.id.edit_profile_nav_title_creds);
        scrollView = (NestedScrollView) findViewById(R.id.edit_profile_scroll_view);
        relayInScrollView = (RelativeLayout) findViewById(R.id.edit_profile_relay_scrollview);
        imageViewAvatar = (ImageView) findViewById(R.id.edit_profile_imageview_avatar);
        buttonEditAvatar = (Button) findViewById(R.id.edit_profile_button_edit_avatar);
        editTextOne = (EditText) findViewById(R.id.edit_profile_edit_text_one);
        textViewOne = (TextView) findViewById(R.id.edit_profile_text_view_one);
        inputIconOne = (TextView) findViewById(R.id.edit_profile_input_icon_1);
        editTextTwo = (EditText) findViewById(R.id.edit_profile_edit_text_two);
        textViewTwo = (TextView) findViewById(R.id.edit_profile_text_view_two);
        inputIconTwo = (TextView) findViewById(R.id.edit_profile_input_icon_2);
        editTextThree = (EditText) findViewById(R.id.edit_profile_edit_text_three);
        textViewThree = (TextView) findViewById(R.id.edit_profile_text_view_three);
        inputIconThree = (TextView) findViewById(R.id.edit_profile_input_icon_3);
        editTextFour = (EditText) findViewById(R.id.edit_profile_edit_text_four);
        textViewFour = (TextView) findViewById(R.id.edit_profile_text_view_four);
        inputIconFour = (TextView) findViewById(R.id.edit_profile_input_icon_4);
        editTextFive = (EditText) findViewById(R.id.edit_profile_edit_text_five);
        textViewFive = (TextView) findViewById(R.id.edit_profile_text_view_five);
        inputIconFive = (TextView) findViewById(R.id.edit_profile_input_icon_5);
        buttonSave = (Button) findViewById(R.id.edit_profile_button_send_recovery_email);
    }

    public void setInitialValues() {
        titleSlot = heightPixels * 0.005f;

        inputHeightOnFocus = heightPixels * 0.38f;

        inputHeightSlotOne = heightPixels * 0.30f;
        inputHeightSlotTwo = heightPixels * 0.42f;
        inputHeightSlotThree = heightPixels * 0.54f;
        inputHeightSlotFour = heightPixels * 0.66f;
        inputHeightSlotFive = heightPixels * 0.78f;
        editTextHeight = heightPixels * 0.079f;
        editTextWidth = widthPixels - dpToPx(64);
        loginButtonheight = heightPixels * 0.069f;
        inputHintInX = widthPixels - (widthPixels - dpToPx(48));
        inputHintOutX = -dpToPx(14);
        inputHintAboveHeight = editTextHeight * 0.65f;
        mainButtonHeight = heightPixels * 0.071f;
        mainButtonSlot = heightPixels * 0.74f;
        backButtonWidth = widthPixels * 0.5f - dpToPx(38);
        mainButtonScaleDownValue = (backButtonWidth) / (widthPixels - dpToPx(64));
        hintTextViewWidth = widthPixels - (int)(inputHintInX * 2) - (dpToPx(96));
    }

    public void setInitialPosition() {
        int xAdjustment = -28;
        float scaleAdjustment = 0.65f;

        relayInScrollView.getLayoutParams().height = heightPixels + 40;
        relayInScrollView.requestLayout();

        imageViewAvatar.setY(heightPixels * 0.0385f);

        buttonEditAvatar.setY(heightPixels * 0.0385f + dpToPx(80));
        buttonEditAvatar.setX(widthPixels * 0.5f + dpToPx(30));
        buttonEditAvatar.setText(pencilIconString);

        editTextOne.getLayoutParams().height = (int) editTextHeight;
        editTextOne.requestLayout();
        editTextOne.setY(inputHeightSlotOne);
        editTextOne.setText("Username");

        textViewOne.getLayoutParams().height = (int) editTextHeight;
        textViewOne.getLayoutParams().width = (int) hintTextViewWidth;
        textViewOne.requestLayout();
        textViewOne.setScaleY(scaleAdjustment);
        textViewOne.setScaleX(scaleAdjustment);
        textViewOne.setX(dpToPx(xAdjustment));
        textViewOne.setY(inputHeightSlotOne - inputHintAboveHeight);

        inputIconOne.getLayoutParams().height = (int) editTextHeight;
        inputIconOne.getLayoutParams().width = (int) editTextWidth;
        inputIconOne.requestLayout();
        inputIconOne.setText(profileIconString);
        inputIconOne.setY(inputHeightSlotOne);

        editTextTwo.getLayoutParams().height = (int) editTextHeight;
        editTextTwo.requestLayout();
        editTextTwo.setY(inputHeightSlotTwo);

        textViewTwo.getLayoutParams().height = (int) editTextHeight;
        textViewTwo.getLayoutParams().width = (int) hintTextViewWidth;
        textViewTwo.requestLayout();
        textViewTwo.setScaleY(scaleAdjustment);
        textViewTwo.setScaleX(scaleAdjustment);
        textViewTwo.setX(dpToPx(xAdjustment));
        textViewTwo.setY(inputHeightSlotTwo - inputHintAboveHeight);

        inputIconTwo.getLayoutParams().height = (int) editTextHeight;
        inputIconTwo.getLayoutParams().width = (int) editTextWidth;
        inputIconTwo.requestLayout();
        inputIconTwo.setText(envelopeIconString);
        inputIconTwo.setY(inputHeightSlotTwo);

        editTextThree.getLayoutParams().height = (int) editTextHeight;
        editTextThree.requestLayout();
        editTextThree.setY(inputHeightSlotThree);

        textViewThree.getLayoutParams().height = (int) editTextHeight;
        textViewThree.getLayoutParams().width = (int) hintTextViewWidth;
        textViewThree.requestLayout();
        textViewThree.requestLayout();
        textViewThree.setScaleY(scaleAdjustment);
        textViewThree.setScaleX(scaleAdjustment);
        textViewThree.setX(dpToPx(xAdjustment));
        textViewThree.setY(inputHeightSlotThree - inputHintAboveHeight);

        inputIconThree.getLayoutParams().height = (int) editTextHeight;
        inputIconThree.getLayoutParams().width = (int) editTextWidth;
        inputIconThree.requestLayout();
        inputIconThree.setText(lockIconString);
        inputIconThree.setY(inputHeightSlotThree);

        editTextFour.getLayoutParams().height = (int) editTextHeight;
        editTextFour.requestLayout();
        editTextFour.setY(inputHeightSlotFour);

        textViewFour.getLayoutParams().height = (int) editTextHeight;
        textViewFour.getLayoutParams().width = (int) hintTextViewWidth;
        textViewFour.requestLayout();
        textViewFour.setScaleY(scaleAdjustment);
        textViewFour.setScaleX(scaleAdjustment);
        textViewFour.setX(dpToPx(xAdjustment));
        textViewFour.setY(inputHeightSlotFour - inputHintAboveHeight);

        inputIconFour.getLayoutParams().height = (int) editTextHeight;
        inputIconFour.getLayoutParams().width = (int) editTextWidth;
        inputIconFour.requestLayout();
        inputIconFour.setText(lockIconString);
        inputIconFour.setY(inputHeightSlotFour);

        editTextFive.getLayoutParams().height = (int) editTextHeight;
        editTextFive.requestLayout();
        editTextFive.setY(inputHeightSlotFive);

        textViewFive.getLayoutParams().height = (int) editTextHeight;
        textViewFive.getLayoutParams().width = (int) hintTextViewWidth;
        textViewFive.setScaleY(scaleAdjustment);
        textViewFive.setScaleX(scaleAdjustment);
        textViewFive.setX(dpToPx(xAdjustment));
        textViewFive.setY(inputHeightSlotFive - inputHintAboveHeight);

        inputIconFive.getLayoutParams().height = (int) editTextHeight;
        inputIconFive.getLayoutParams().width = (int) editTextWidth;
        inputIconFive.requestLayout();
        inputIconFive.setText(lockIconString);
        inputIconFive.setY(inputHeightSlotFive);

//        buttonSave.setY(heightPixels - dpToPx(78));
        buttonSave.setY(heightPixels - HQ.getInstance().dpToPx(78));
    }

    public void setCustomFont() {
        // fonts //
        sstFontLight = Typeface.createFromAsset(getAssets(), "fonts/SST-Light.ttf");
        sstFontRoman = Typeface.createFromAsset(getAssets(), "fonts/SST-Roman.ttf");
        sstFontHeavy = Typeface.createFromAsset(getAssets(), "fonts/SST-Heavy.ttf");
        sstFontBold = Typeface.createFromAsset(getAssets(), "fonts/SST-Bold.ttf");
        sstFontCondensed = Typeface.createFromAsset(getAssets(), "fonts/SST-Condensed.ttf");
        sstFontCondensedBd = Typeface.createFromAsset(getAssets(), "fonts/SST-CondensedBd.ttf");
        fontAwesome = Typeface.createFromAsset(getAssets(), "fonts/fontawesome-webfont.ttf");

//        textNavTitle.setTypeface(sstFontBold);
        editTextOne.setTypeface(sstFontRoman);
        buttonEditAvatar.setTypeface(fontAwesome);
        textViewOne.setTypeface(sstFontRoman);
        inputIconOne.setTypeface(fontAwesome);
        editTextTwo.setTypeface(sstFontRoman);
        textViewTwo.setTypeface(sstFontRoman);
        inputIconTwo.setTypeface(fontAwesome);
        editTextThree.setTypeface(sstFontRoman);
        textViewThree.setTypeface(sstFontRoman);
        inputIconThree.setTypeface(fontAwesome);
        editTextFour.setTypeface(sstFontRoman);
        textViewFour.setTypeface(sstFontRoman);
        inputIconFour.setTypeface(fontAwesome);
        editTextFive.setTypeface(sstFontRoman);
        textViewFive.setTypeface(sstFontRoman);
        inputIconFive.setTypeface(fontAwesome);
    }

    private void animateEditTextOnFocus(int editText, boolean onFocus) {
        float inputInitialHeight = 0;

        EditText theET = null;
        TextView theTV = null;
        TextView theICON = null;

        EditText theET1 = null;
        TextView theTV1 = null;
        TextView theICON1 = null;
        EditText theET2 = null;
        TextView theTV2 = null;
        TextView theICON2 = null;
        EditText theET3 = null;
        TextView theTV3 = null;
        TextView theICON3 = null;
        EditText theET4 = null;
        TextView theTV4 = null;
        TextView theICON4 = null;

        switch (editText) {
            case 1:
                theET = editTextOne;
                theTV = textViewOne;
                theICON = inputIconOne;
                inputInitialHeight = inputHeightSlotOne;

                theET1 = editTextTwo;
                theTV1 = textViewTwo;
                theICON1 = inputIconTwo;

                theET2 = editTextThree;
                theTV2 = textViewThree;
                theICON2 = inputIconThree;

                theET3 = editTextFour;
                theTV3 = textViewFour;
                theICON3 = inputIconFour;

                theET4 = editTextFive;
                theTV4 = textViewFive;
                theICON4 = inputIconFive;
                break;
            case 2:
                theET1 = editTextOne;
                theTV1 = textViewOne;
                theICON1 = inputIconOne;

                theET = editTextTwo;
                theTV = textViewTwo;
                theICON = inputIconTwo;
                inputInitialHeight = inputHeightSlotTwo;

                theET2 = editTextThree;
                theTV2 = textViewThree;
                theICON2 = inputIconThree;

                theET3 = editTextFour;
                theTV3 = textViewFour;
                theICON3 = inputIconFour;

                theET4 = editTextFive;
                theTV4 = textViewFive;
                theICON4 = inputIconFive;
                break;
            case 3:
                theET1 = editTextOne;
                theTV1 = textViewOne;
                theICON1 = inputIconOne;

                theET2 = editTextTwo;
                theTV2 = textViewTwo;
                theICON2 = inputIconTwo;

                theET = editTextThree;
                theTV = textViewThree;
                theICON = inputIconThree;
                inputInitialHeight = inputHeightSlotThree;

                theET3 = editTextFour;
                theTV3 = textViewFour;
                theICON3 = inputIconFour;

                theET4 = editTextFive;
                theTV4 = textViewFive;
                theICON4 = inputIconFive;
                break;
            case 4:
                theET1 = editTextOne;
                theTV1 = textViewOne;
                theICON1 = inputIconOne;

                theET2 = editTextTwo;
                theTV2 = textViewTwo;
                theICON2 = inputIconTwo;

                theET3 = editTextThree;
                theTV3 = textViewThree;
                theICON3 = inputIconThree;

                theET = editTextFour;
                theTV = textViewFour;
                theICON = inputIconFour;
                inputInitialHeight = inputHeightSlotFour;

                theET4 = editTextFive;
                theTV4 = textViewFive;
                theICON4 = inputIconFive;
                break;
            case 5:
                theET1 = editTextOne;
                theTV1 = textViewOne;
                theICON1 = inputIconOne;

                theET2 = editTextTwo;
                theTV2 = textViewTwo;
                theICON2 = inputIconTwo;

                theET3 = editTextThree;
                theTV3 = textViewThree;
                theICON3 = inputIconThree;

                theET4 = editTextFour;
                theTV4 = textViewFour;
                theICON4 = inputIconFour;

                theET = editTextFive;
                theTV = textViewFive;
                theICON = inputIconFive;
                inputInitialHeight = inputHeightSlotFive;
                break;
            default:
                break;
        }

        if (onFocus == true) {
            imageViewAvatar.animate().scaleX(0.5f).scaleY(0.5f);
            buttonEditAvatar.animate().alpha(0.0f);

            theET.animate().translationY(inputHeightSlotOne);
            theICON.animate().translationY(inputHeightSlotOne);
            theTV.animate().translationY(inputHeightSlotOne - inputHintAboveHeight);

            theET1.animate().scaleX(0.0f).scaleY(0.0f);
            theICON1.animate().scaleX(0.0f).scaleY(0.0f);
            theTV1.animate().scaleX(0.0f).scaleY(0.0f);

            theET2.animate().scaleX(0.0f).scaleY(0.0f);
            theICON2.animate().scaleX(0.0f).scaleY(0.0f);
            theTV2.animate().scaleX(0.0f).scaleY(0.0f);

            theET3.animate().scaleX(0.0f).scaleY(0.0f);
            theICON3.animate().scaleX(0.0f).scaleY(0.0f);
            theTV3.animate().scaleX(0.0f).scaleY(0.0f);

            theET4.animate().scaleX(0.0f).scaleY(0.0f);
            theICON4.animate().scaleX(0.0f).scaleY(0.0f);
            theTV4.animate().scaleX(0.0f).scaleY(0.0f);
        } else {
            imageViewAvatar.animate().scaleX(1.0f).scaleY(1.0f);
            buttonEditAvatar.animate().alpha(1.0f);

            theET.animate().translationY(inputInitialHeight);
            theICON.animate().translationY(inputInitialHeight);
            theTV.animate().translationY(inputInitialHeight - inputHintAboveHeight);

            theET1.animate().scaleX(1.0f).scaleY(1.0f);
            theICON1.animate().scaleX(1.0f).scaleY(1.0f);
            theTV1.animate().scaleX(0.65f).scaleY(0.65f);

            theET2.animate().scaleX(1.0f).scaleY(1.0f);
            theICON2.animate().scaleX(1.0f).scaleY(1.0f);
            theTV2.animate().scaleX(0.65f).scaleY(0.65f);

            theET3.animate().scaleX(1.0f).scaleY(1.0f);
            theICON3.animate().scaleX(1.0f).scaleY(1.0f);
            theTV3.animate().scaleX(0.65f).scaleY(0.65f);

            theET4.animate().scaleX(1.0f).scaleY(1.0f);
            theICON4.animate().scaleX(1.0f).scaleY(1.0f);
            theTV4.animate().scaleX(0.65f).scaleY(0.65f);
        }



    }

    private int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
    private int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }


    @Override public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    editTextFocus = 0;
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }

    //API
    public void psbaAPIEditAccountDeets() {

        final String username = String.valueOf(editTextOne.getText());
        final String email = String.valueOf(editTextTwo.getText());
        final String psnid = String.valueOf(editTextThree.getText());

        final String pass = String.valueOf(editTextFour.getText());
        final String confirmPass = String.valueOf(editTextFive.getText());

        if (!pass.equals(confirmPass)) {
            Toast toast = Toast.makeText(ProfileEditActivity.this, "passwords dont match", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            return;
        }


        AsyncTask asyncTask = new AsyncTask() {
            @Override protected Object doInBackground(Object[] params) {
                OkHttpClient client = new OkHttpClient();

                RequestBody formBody = new FormBody.Builder()
                        .add("user_id", HQ.getInstance().theUser.getUser_id())
                        .add("username", username)
                        .add("psnid", psnid)
                        .add("email", email)
                        .add("password", pass)
                        .build();
                Request request = new Request.Builder()
                        .url("http://psba.pixelbeard.co/api/users/update")
                        .post(formBody)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    JSONObject testJSON = new JSONObject(response.body().string());
                    HashMap<String, String> editProfileResponse = HQ.getInstance().jsonToMap(testJSON.toString());

                    if (editProfileResponse.get("success").matches("true")) {


                        return "success";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return "fail";
            }

            @Override protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                if (o.equals("success")) {
                    Log.v("XYZ_FUTURE::: ", "editProfileResponse success true: " + username + psnid + email);
                    HQ.getInstance().theUser.setUsername(username);
                    HQ.getInstance().theUser.setPsnid(psnid);
                    HQ.getInstance().theUser.setEmail(email);
//                    HQ.getInstance().editProfilePopulatePrefs();

                    Toast toastLogin = Toast.makeText(ProfileEditActivity.this, "You have successfully edited your profile.", Toast.LENGTH_LONG);
                    toastLogin.show();

                    Intent intent = new Intent(ProfileEditActivity.this, MainActivity.class);
                    intent.putExtra("fromEditProfile", true);
                    startActivity(intent);
//                    overridePendingTransition(R.anim.fade_in, R.anim.slide_out_down);

                } else {
//                    slideInAlert("FAILURE", "We were unable to log you in with those details.", true, 3000);
                }
            }
        };
        asyncTask.execute();
    }



    /////////////////////////////////
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
    private void performCrop(String picUri) {
        try {
            //Start Crop Activity

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            File f = new File(picUri);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 280);
            cropIntent.putExtra("outputY", 280);

            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, 11);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage) throws IOException {
        int MAX_HEIGHT = 1024;
        int MAX_WIDTH = 1024;

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
        BitmapFactory.decodeStream(imageStream, null, options);
        imageStream.close();

        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        options.inJustDecodeBounds = false;
        imageStream = context.getContentResolver().openInputStream(selectedImage);
        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

        img = rotateImageIfRequired(img, selectedImage);
        return img;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

            final float totalPixels = width * height;
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    private Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) throws IOException {

        InputStream input = this.getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else
            ei = new ExifInterface(selectedImage.getPath());

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }
    /////////////////////////////////
}
