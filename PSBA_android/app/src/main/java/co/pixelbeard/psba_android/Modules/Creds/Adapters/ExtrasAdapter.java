package co.pixelbeard.psba_android.Modules.Creds.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.pixelbeard.psba_android.Modules.Creds.Models.ExtraOption;
import co.pixelbeard.psba_android.R;
import co.pixelbeard.psba_android.HQ;

/**
 * Created by aaroneckhart on 8/9/17.
 */

public class ExtrasAdapter extends RecyclerView.Adapter {

    private ArrayList<ExtraOption> dataSet;
    Context mContext;
    int total_types;
    MediaPlayer mPlayer;



    public HashMap<String, String> indexedQuestions = new HashMap<>();
    public ArrayList<String> theQuestionsArrayList;

    String dropTextDynamic;

    private LayoutInflater inflater;
    private String timeString;

    private static ExtrasAdapter.ItemClickCallback itemClickCallback;


    //
    public static class DropDownHolder extends RecyclerView.ViewHolder /* implements View.OnClickListener*/ {

        private RelativeLayout dropDownRelay;
        private Spinner dropDown;
        private TextView dropDownText;
        private TextView dropDownIcon;
        private TextView dropDownRequired;



        public DropDownHolder(View itemView) {
            super(itemView);
//            HQ.getInstance().positionExtrasAdapterMain++;
            Log.v("XYZ_FUTURE::: ", "DropDownHolder");

            dropDownRelay = (RelativeLayout) itemView.findViewById(R.id.recyc_element_extras_dropdown_row_relay);
            dropDown = (Spinner) itemView.findViewById(R.id.recyc_element_extras_dropdown);
            dropDownText = (TextView) itemView.findViewById(R.id.recyc_element_extras_dropdown_prompt_text);
            dropDownIcon = (TextView) itemView.findViewById(R.id.recyc_element_extras_dropdown_text);
            dropDownRequired = (TextView) itemView.findViewById(R.id.recyc_element_extras_dropdown_required);

            dropDownText.setTypeface(HQ.getInstance().sstFontRoman);
            dropDownIcon.setTypeface(HQ.getInstance().fontAwesome);
            dropDownRequired.setTypeface(HQ.getInstance().sstFontRoman);

            // Spinner click listener
            dropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String question = HQ.getInstance().extraOptions.get(getAdapterPosition()).getIdentifier();
                    String answer = HQ.getInstance().spinnerDropDownElements.get(position);
                    Log.v("XYZ_FUTURE::: ", "!!!!!!!!!!!! DROPDOWN setOnItemSelectedListener spinner position value: " + answer + "\n spinner question value: " + question);
                    HQ.getInstance().dropChosen = true;

                    for (int i = 0; i < HQ.getInstance().extrasAnswersArray.size(); i++) {
                        HashMap<String, String> hash = HQ.getInstance().extrasAnswersArray.get(i);
                        if (hash.containsValue(question)) {
                            HQ.getInstance().extrasAnswersArray.remove(i);
                        }
                    }

                    HashMap<String, String> answerObject = new HashMap();
                    answerObject.put("question", question);
                    answerObject.put("answer", answer);
                    HQ.getInstance().extrasAnswersArray.add(answerObject);

                    Log.v("XYZ_FUTURE::: ", "extrasAnswersArray is: " + HQ.getInstance().extrasAnswersArray.toString());
                    HQ.getInstance().extrasAnswers.clear();
                    HQ.getInstance().extrasAnswers.put("answers", HQ.getInstance().extrasAnswersArray);
                    JSONObject json = new JSONObject(HQ.getInstance().extrasAnswers);
                    HQ.getInstance().theUser.setExtras(json.toString());
                    Log.v("XYZ_FUTURE::: ", "extrasAnswers json is: " + json);
                }

                @Override public void onNothingSelected(AdapterView<?> parent) {
                    Log.v("XYZ_FUTURE::: ", "dropDown.setOnItemSelectedListener onNothingSelected");
//                    dropDown.setPrompt(HQ.getInstance().extraOptions.get(getAdapterPosition()).getIdentifier());
                }
            });
        }
    }

    public static class TextHolder extends RecyclerView.ViewHolder /* implements View.OnClickListener */ {

        private RelativeLayout textRelay;
        private EditText editText;
        private TextView text;
        private TextView textRequired;


        public TextHolder(View itemView) {
            super(itemView);
//            HQ.getInstance().positionExtrasAdapterMain++;
            Log.v("XYZ_FUTURE::: ", "TextHolder");

            textRelay = (RelativeLayout) itemView.findViewById(R.id.recyc_element_extras_text_row_relay);
            editText = (EditText) itemView.findViewById(R.id.recyc_element_text_edit_text);
            text = (TextView) itemView.findViewById(R.id.recyc_element_extras_text_text);
            textRequired = (TextView) itemView.findViewById(R.id.recyc_element_extras_text_required);

            editText.setTypeface(HQ.getInstance().sstFontRoman);
            textRequired.setTypeface(HQ.getInstance().sstFontRoman);

//            final String localQuestion = (String) text.getText();

            editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override public void onFocusChange(View v, boolean hasFocus) {
                    Log.v("XYZ_FUTURE::: ", "editText onFocusChange focus?: " + hasFocus);
                    if (hasFocus) {
                        editText.setText("");
                    } else if (!hasFocus) {
                        if (editText.getText().toString().matches("")) {
                            editText.setText(HQ.getInstance().extraOptions.get(getAdapterPosition()).getIdentifier());
                        } else {
                            // answer is here
                            String answerText = editText.getText().toString();

                            for (int i = 0; i < HQ.getInstance().extrasAnswersArray.size(); i++) {
                                HashMap<String, String> hash = HQ.getInstance().extrasAnswersArray.get(i);
                                if (hash.containsValue(HQ.getInstance().extraOptions.get(getAdapterPosition()).getIdentifier())) {
                                    HQ.getInstance().extrasAnswersArray.remove(i);
                                }
                            }
                            HQ.getInstance().extrasAnswersHash.put("question", HQ.getInstance().extraOptions.get(getAdapterPosition()).getIdentifier());
                            HQ.getInstance().extrasAnswersHash.put("answer", answerText);
                            HQ.getInstance().extrasAnswersArray.add(HQ.getInstance().extrasAnswersHash);
                            Log.v("XYZ_FUTURE::: ", "extrasAnswersArray is: " + HQ.getInstance().extrasAnswersArray.toString());

                            HQ.getInstance().extrasAnswers.clear();
                            HQ.getInstance().extrasAnswers.put("answers", HQ.getInstance().extrasAnswersArray);
                            JSONObject json = new JSONObject(HQ.getInstance().extrasAnswers);
                            HQ.getInstance().theUser.setExtras(json.toString());
                            Log.v("XYZ_FUTURE::: ", "extrasAnswers json is: " + json);
                        }


                    }
                }
            });
        }
    }


    public ExtrasAdapter(Context context, ArrayList data) {
//        this.inflater.from(context);

        this.dataSet = data;
        this.mContext = context;
        total_types = dataSet.size();
    }

    public interface ItemClickCallback {
        void onItemClick(int p);
//        void onAvatarClick(int p);
    }

    public void setItemClickCallback(final ExtrasAdapter.ItemClickCallback itemClickCallback) {
        this.itemClickCallback = itemClickCallback;
    }



    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        switch (viewType) {
            case 1://ExtrasRecycModel.DROPDOWN_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyc_element_extras_dropdown, parent, false);
                return new DropDownHolder(view);
            case 2://ExtrasRecycModel.TEXT_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyc_element_extras_text, parent, false);
                return new TextHolder(view);
        }
        return null;
    }


    @Override public int getItemViewType(int position) {

        switch (dataSet.get(position).getType()) {
            case "DropDown":
                return 1;//ExtrasRecycModel.DROPDOWN_TYPE;
            case "Text":
                return 2;//ExtrasRecycModel.TEXT_TYPE;
            default:
                return -1;
        }
    }



    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        HQ.getInstance().positionExtrasAdapterMain = position;
        // here is where we make our List of elements to be used later //


        ExtraOption object = dataSet.get(position);


//        indexedQuestions.put("" + position, object.getIdentifier());

        Log.v("XYZ_FUTURE::: ", "onBindViewHolder object ident: " + object.getIdentifier() + "\n position is: " + position);

        if (object != null) {
            switch (object.getType()) {
                case "Text":
//                    ((TextHolder)holder).text.setText("This is text text");
                    ((TextHolder)holder).editText.setText(object.getIdentifier());

                    break;
                case "DropDown":
                    ((DropDownHolder)holder).dropDownText.setText("");
                    // Spinner Drop down elements
                    HQ.getInstance().spinnerDropDownElements = HQ.getInstance().getExtrasValues(position);

                    // Creating adapter for spinner
                    MySpinnerAdapter dataAdapter = new MySpinnerAdapter(((DropDownHolder) holder).dropDown.getContext(), android.R.layout.simple_spinner_item, HQ.getInstance().spinnerDropDownElements);

                    // Drop down layout style - list view with radio button
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    // attaching data adapter to spinner
                    ((DropDownHolder)holder).dropDown.setAdapter(dataAdapter);

//                    ((DropDownHolder)holder).dropDown.setPrompt(dataSet.get(HQ.getInstance().positionExtrasAdapterMain).getIdentifier());

                    break;
            }
        }

    }

    @Override public int getItemCount() {
        return dataSet.size();
    }


    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }


    private static class MySpinnerAdapter extends ArrayAdapter<String> {
        // Initialise custom font, for example:
//        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/SST-Roman.ttf");

        private MySpinnerAdapter(Context context, int resource, List<String> items) {
            super(context, resource, items);
        }

        // Affects default (closed) state of the spinner
        @Override public View getView(int position, View convertView, ViewGroup parent) {

            TextView view = (TextView) super.getView(position, convertView, parent);
            view.setTypeface(HQ.getInstance().sstFontRoman);
            view.setTextSize(12);
            view.setTextColor(getContext().getResources().getColor(R.color.colorPrimaryDark, null));

//            Log.v("XYZ_FUTURE::: ", "getView in myspinnerAdapter position: " + position + " ;:: and dropChosen hack is: " + HQ.getInstance().dropChosen);

            if (HQ.getInstance().dropChosen == true) {
                view.setText(HQ.getInstance().spinnerDropDownElements.get(position));

            } else {
                Log.v("XYZ_FUTURE::: ", "getView identifier: " + HQ.getInstance().extraOptions.get(HQ.getInstance().positionExtrasAdapterMain).getIdentifier());
                view.setText(HQ.getInstance().extraOptions.get(HQ.getInstance().positionExtrasAdapterMain).getIdentifier());
            }

            // this is a hack but it works... kinda//
            HQ.getInstance().dropChosen = false;



            return view;
        }

        // Affects opened state of the spinner
        @Override public View getDropDownView(int position, View convertView, ViewGroup parent) {

            TextView view = (TextView) super.getDropDownView(position, convertView, parent);
            view.setTypeface(HQ.getInstance().sstFontRoman);
            view.setTextColor(getContext().getResources().getColor(R.color.colorPrimaryDark, null));
            view.setTextSize(12);
            return view;
        }

    }

}


