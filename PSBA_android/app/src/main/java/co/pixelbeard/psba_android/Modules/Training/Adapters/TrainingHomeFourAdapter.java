package co.pixelbeard.psba_android.Modules.Training.Adapters;

import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import co.pixelbeard.psba_android.HQ;
import co.pixelbeard.psba_android.Modules.Training.Models.TrainingModule;
import co.pixelbeard.psba_android.R;
import co.pixelbeard.psba_android.TrainingItemClickListener;

/**
 * Created by AE1 on 8/29/17.
 */

public class TrainingHomeFourAdapter extends RecyclerView.Adapter<TrainingHomeFourAdapter.TrainingHomeFourHolder> {

    private final TrainingItemClickListener trainingItemClickListener;
    private ArrayList<TrainingModule> trainingItems;

    public TrainingHomeFourAdapter(ArrayList<TrainingModule> trainingItems, TrainingItemClickListener trainingItemClickListener) {
        this.trainingItems = trainingItems;
        this.trainingItemClickListener = trainingItemClickListener;
    }
    @Override public TrainingHomeFourAdapter.TrainingHomeFourHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyc_element_training_home_four, parent, false);
        return new TrainingHomeFourAdapter.TrainingHomeFourHolder(view);
    }

    @Override public int getItemCount() {
        return HQ.getInstance().trainingModulesDigitalContent.size();
    }

    @Override public void onBindViewHolder(final TrainingHomeFourAdapter.TrainingHomeFourHolder holder, int position) {
        final TrainingModule trainingItem = trainingItems.get(position);

        holder.title.setText(HQ.getInstance().trainingModulesDigitalContent.get(position).getName());
        holder.points.setText(HQ.getInstance().trainingModulesDigitalContent.get(position).getPoints() + "pts");

        // 120 dp is width of card //
        float totalQuestions = HQ.getInstance().trainingModulesDigitalContent.get(position).getUserProgressRefTotalQuestions();
        float numberCorrect = HQ.getInstance().trainingModulesDigitalContent.get(position).getUserProgressRefCorrectAnswers();;
        if (totalQuestions != 0) {
            float ratio = numberCorrect / totalQuestions;
            Log.v("XYZ_FUTURE::: ", "questions and number correct are: " + totalQuestions +"::"+ numberCorrect + "\n and ratio is: " + ratio);
            holder.progressBar.getLayoutParams().width = (int)(HQ.getInstance().dpToPx(120) * ratio);
        } else {
            holder.progressBar.getLayoutParams().width = 0;
        }
        holder.progressBar.requestLayout();

        Picasso.with(holder.itemView.getContext())
                .load("https://c1.staticflickr.com/1/188/417924629_6832e79c98_z.jpg?zz=1")//HQ.getInstance().theAvatarURL + "default-avatar.jpg")
                .fit()
                .centerCrop()
                .into(holder.trainingItemImage);
        ViewCompat.setTransitionName(holder.trainingItemImage, trainingItem.getName());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                trainingItemClickListener.onTrainingItemClick(holder.getAdapterPosition(), trainingItem, holder.trainingItemImage, "four");
            }
        });


    }

    public class TrainingHomeFourHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public CardView cardView;
        public ImageView trainingItemImage;
        public View progressBar;
        public TextView title;
        public TextView points;

        public TrainingHomeFourHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.recyc_training_home_four_cardview);
            trainingItemImage = (ImageView) itemView.findViewById(R.id.recyc_training_home_four_image);
            progressBar = itemView.findViewById(R.id.recyc_training_home_four_progress_bar);
            title = (TextView) itemView.findViewById(R.id.recyc_training_home_four_text_title);
            points = (TextView) itemView.findViewById(R.id.recyc_training_home_four_text_points);
        }
        @Override public void onClick(View v) {
            if (v.getId() == R.id.recyc_training_home_four_cardview) {
                Log.v("XYZ", "recyc_store_main_cardview");

            } else {
                Log.v("XYZ", "NOT on card");
            }
        }
    }
}