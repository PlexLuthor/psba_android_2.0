package co.pixelbeard.psba_android.Modules.Creds.Models;

/**
 * Created by aaroneckhart on 8/9/17.
 */

public class ExtrasRecycModel {
    public static final int DROPDOWN_TYPE = 0;
    public static final int TEXT_TYPE = 1;

    public int type;
    public int data;
    public String text;

    public ExtrasRecycModel(int type, String text, int data) {
        this.type = type;
        this.data = data;
        this.text = text;
    }
}