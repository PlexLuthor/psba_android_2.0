package co.pixelbeard.psba_android.Modules.Training.Acts;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import co.pixelbeard.psba_android.Modules.Training.Frags.TrainingModuleFragment;
import co.pixelbeard.psba_android.Modules.Training.Models.TrainingModule;
import co.pixelbeard.psba_android.R;

public class TrainingModuleDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_module_detail);

        supportPostponeEnterTransition();

        Bundle extras = getIntent().getExtras();
        TrainingModule trainingItem = extras.getParcelable(TrainingModuleFragment.EXTRA_TRAINING_ITEM);

        ImageView imageView = (ImageView) findViewById(R.id.image_training_detail_main);
//        TextView textView = (TextView) findViewById(R.id.animal_detail_text);
//        textView.setText(trainingItem.detail);

        String imageUrl = "https://c1.staticflickr.com/1/188/417924629_6832e79c98_z.jpg?zz=1";//trainingItem.getImage();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String imageTransitionName = extras.getString(TrainingModuleFragment.EXTRA_TRAINING_IMAGE_TRANSITION_NAME);
            imageView.setTransitionName(imageTransitionName);
        }

        Picasso.with(this).load(imageUrl).noFade().into(imageView, new Callback() {
                    @Override public void onSuccess() {
                        supportStartPostponedEnterTransition();
                    }

                    @Override public void onError() {
                        supportStartPostponedEnterTransition();
                    }
                });
    }
}
