package co.pixelbeard.psba_android.Modules.Creds.Acts;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import co.pixelbeard.psba_android.HQ;
import co.pixelbeard.psba_android.MainActivity;
import co.pixelbeard.psba_android.Modules.Creds.Models.Company;
import co.pixelbeard.psba_android.Modules.Profile.Models.User;
import co.pixelbeard.psba_android.R;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import pl.droidsonroids.gif.GifImageView;


public class CredentialsActivity extends AppCompatActivity implements TextView.OnEditorActionListener {

    private ImageView imageViewBrandTitle;
    private Button buttonLogin;
    private TextView buttonLoginText;
    private Button buttonSignUp;
    private View viewLoginUnderline;
    private View viewSignupUnderline;
    private View viewMovingUnderline;
    // bottom elements //
    private Button buttonMainCreds;
    private Button buttonBackCreds;
    private Button buttonForgotDetails;
    private TextView textForMainButton;

    // login relay //
    private RelativeLayout relayLogin;

    private TextView textViewStepPrompt;
    private TextView textViewStepInfo;

    private EditText editTextOne;
    private TextView textViewOne;
    private TextView inputIconOne;
    private TextView textRequiredOne;
    private TextView textInputInfoOne;

    private EditText editTextTwo;
    private TextView textViewTwo;
    private TextView inputIconTwo;
    private TextView textRequiredTwo;
    private TextView textInputInfoTwo;

    // first last relay sign up 1 //
    private RelativeLayout relaySignUp1;

    private TextView textViewStepPromptSignUp1;
    private TextView textViewStepInfoSignUp1;

    private EditText editTextOneSignUp1;
//    private TextView textViewOneSignUp1;
    private TextView inputIconOneSignUp1;
    private TextView textRequiredOneSignUp1;
    private TextView textInputInfoOneSignUp1;

    private EditText editTextTwoSignUp1;
    private TextView textViewTwoSignUp1;
    private TextView inputIconTwoSignUp1;
    private TextView textRequiredTwoSignUp1;
    private TextView textInputInfoTwoSignUp1;

    // user psn relay sign up 2 //
    private RelativeLayout relaySignUp2;

    private TextView textViewStepPromptSignUp2;
    private TextView textViewStepInfoSignUp2;

    private EditText editTextOneSignUp2;
    private TextView textViewOneSignUp2;
    private TextView inputIconOneSignUp2;
    private TextView textRequiredOneSignUp2;
    private TextView textInputInfoOneSignUp2;

    private EditText editTextTwoSignUp2;
    private TextView textViewTwoSignUp2;
    private ImageView inputIconTwoSignUp2;
    private TextView textRequiredTwoSignUp2;
    private TextView textInputInfoTwoSignUp2;

    // email relay sign up 3 //
    private RelativeLayout relaySignUp3;

    private TextView textViewStepPromptSignUp3;
    private TextView textViewStepInfoSignUp3;

    private EditText editTextOneSignUp3;
    private TextView textViewOneSignUp3;
    private TextView inputIconOneSignUp3;
    private TextView textRequiredOneSignUp3;
    private TextView textInputInfoOneSignUp3;

    private EditText editTextTwoSignUp3;
    private TextView textViewTwoSignUp3;
    private TextView inputIconTwoSignUp3;
    private TextView textRequiredTwoSignUp3;
    private TextView textInputInfoTwoSignUp3;

    // password relay sign up 4 //
    private RelativeLayout relaySignUp4;

    private TextView textViewStepPromptSignUp4;
    private TextView textViewStepInfoSignUp4;

    private EditText editTextOneSignUp4;
    private TextView textViewOneSignUp4;
    private TextView inputIconOneSignUp4;
    private TextView textRequiredOneSignUp4;
    private TextView textInputInfoOneSignUp4;

    private EditText editTextTwoSignUp4;
    private TextView textViewTwoSignUp4;
    private TextView inputIconTwoSignUp4;
    private TextView textRequiredTwoSignUp4;
    private TextView textInputInfoTwoSignUp4;

    // store relay sign up 5 //
    private RelativeLayout relaySignUp5;

    private TextView textViewStepPromptSignUp5;
    private TextView textViewStepInfoSignUp5;

    private EditText editTextOneSignUp5;
    private TextView textViewOneSignUp5;
    private TextView inputIconOneSignUp5;
    private TextView textRequiredOneSignUp5;
    private TextView textInputInfoOneSignUp5;

    private EditText editTextTwoSignUp5;
    private TextView textViewTwoSignUp5;
    private TextView inputIconTwoSignUp5;
    private TextView textRequiredTwoSignUp5;
    private TextView textInputInfoTwoSignUp5;

    // occupation relay sign up 6 //
    private RelativeLayout relaySignUp6;

    private TextView textViewStepPromptSignUp6;
    private TextView textViewStepInfoSignUp6;

    private EditText editTextOneSignUp6;
    private TextView textViewOneSignUp6;
    private TextView inputIconOneSignUp6;
    private TextView textRequiredOneSignUp6;
    private TextView textInputInfoOneSignUp6;


    private static final String POSITION_LEFT = "left";
    private static final String POSITION_RIGHT = "right";
    private static final String POSITION_MIDDLE = "middle";


    private RelativeLayout psbaAlert;
    private View psbaAlertOverlay;
    private GifImageView psbaAlertGif;
    private TextView psbaAlertTitle;
    private TextView psbaAlertBody;

    int heightPixels;
    int widthPixels;

    float mainButtonScaleDownValue;
    float backButtonWidth;

    boolean isFlagFromAvatar = false;
    boolean isRegiSuccess = false;

    private int credState;
    private int editTextFocus;
    private boolean isKeyboardDisplayed;


    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credentials);

//        addKeyboardWizard();

        // pixels, dpi //
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        heightPixels = metrics.heightPixels;
        widthPixels = metrics.widthPixels;
        int densityDpi = metrics.densityDpi;
        float xdpi = metrics.xdpi;
        float ydpi = metrics.ydpi;
        Log.v("XYZ_FUTURE:::", "credentials widthPixels  = " + widthPixels);
        Log.v("XYZ_FUTURE:::", "credentials heightPixels = " + heightPixels);


        findViews();
        setInitialValues();
        setInitialUIPosAndSize();
        setCustomFont();

        credUIState0();

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View buttonLogin) {
                HQ.getInstance().customAnimationForButton(buttonLogin);
                slideRelay(relayLogin, POSITION_MIDDLE);
                switch (credState) {
                    case 0:
                        break;
                    case 1:
                        Log.v("XYZ_FUTURE::: ", "buttonLogin case 1");
                        slideRelay(relaySignUp1, POSITION_RIGHT);
                        relaySignUp2.setX(widthPixels);
                        relaySignUp3.setX(widthPixels);
                        relaySignUp4.setX(widthPixels);
                        relaySignUp5.setX(widthPixels);
                        relaySignUp6.setX(widthPixels);
                        break;
                    case 2:
                        slideRelay(relaySignUp2, POSITION_RIGHT);
                        relaySignUp1.setX(widthPixels);
                        relaySignUp3.setX(widthPixels);
                        relaySignUp4.setX(widthPixels);
                        relaySignUp5.setX(widthPixels);
                        relaySignUp6.setX(widthPixels);
                        break;
                    case 3:
                        slideRelay(relaySignUp3, POSITION_RIGHT);
                        relaySignUp1.setX(widthPixels);
                        relaySignUp2.setX(widthPixels);
                        relaySignUp4.setX(widthPixels);
                        relaySignUp5.setX(widthPixels);
                        relaySignUp6.setX(widthPixels);
                        break;
                    case 4:
                        slideRelay(relaySignUp4, POSITION_RIGHT);
                        relaySignUp1.setX(widthPixels);
                        relaySignUp2.setX(widthPixels);
                        relaySignUp3.setX(widthPixels);
                        relaySignUp5.setX(widthPixels);
                        relaySignUp6.setX(widthPixels);
                        break;
                    case 5:
                        slideRelay(relaySignUp5, POSITION_RIGHT);
                        relaySignUp1.setX(widthPixels);
                        relaySignUp2.setX(widthPixels);
                        relaySignUp3.setX(widthPixels);
                        relaySignUp4.setX(widthPixels);
                        relaySignUp6.setX(widthPixels);
                        break;
                    case 6:
                        slideRelay(relaySignUp6, POSITION_RIGHT);
                        relaySignUp1.setX(widthPixels);
                        relaySignUp2.setX(widthPixels);
                        relaySignUp3.setX(widthPixels);
                        relaySignUp4.setX(widthPixels);
                        relaySignUp5.setX(widthPixels);
                        break;
                    default:
                        break;
                }
                credUIState0();
            }
        });

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View buttonLogin) {
                switch (credState) {
                    case 0:
                        credUIState1();
                        psbaAPIGetCompanies();
                        break;
                    case 1:case 2:case 3:case 4:case 5:
                        break;
                    default:
                        break;
                }
            }
        });

        buttonBackCreds.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                switch (credState) {
                    case 0:
                        psbaAPILogin();
                        break;
                    case 1:
                        credUIState0();
                        break;
                    case 2:
                        credUIState1();
                        break;
                    case 3:
                        credUIState2();
                        break;
                    case 4:
                        credUIState3();
                        break;
                    case 5:
                        credUIState4();
                        break;
                    case 6:
                        HQ.getInstance().setUserCompanyInfoToNil();
                        credUIState5();
                        break;
                    default:
                        break;
                }
            }
        });

        buttonForgotDetails.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                HQ.getInstance().customAnimationForButton(v);
                Intent intentReqCode = new Intent(CredentialsActivity.this, RequestActivationCodeActivity.class);
                startActivity(intentReqCode);
                overridePendingTransition(R.anim.slide_in_right, R.anim.fade_out);
            }
        });

        buttonMainCreds.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Log.v("XYZ_FUTURE::: ", "alpha of main creds button is:" + buttonMainCreds.getAlpha());
                if (buttonMainCreds.getAlpha() < 0.9) {
                    switch (credState) {
                        case 0:
                            slideInAlert("Cannot Continue", "Please fill in both forms in order to login.", true, 2000);
                            break;
                        case 1:
                            slideInAlert("Cannot Continue", "Please fill in both first and last name in order to proceed.", true, 2000);
                            break;
                        case 2:
                            slideInAlert("Cannot Continue", "We need your username in order to proceed.", true, 2000);
                            break;
                        case 3:
                            slideInAlert("Cannot Continue", "The emails do not match.", true, 2000);
                            break;
                        case 4:
                            slideInAlert("Cannot Continue", "The passwords do not match.", true, 2000);
                            break;
                        case 5:
                            slideInAlert("Cannot Continue", "Please complete your company details", true, 2000);
                            break;
                        case 6:
                            slideInAlert("Cannot Continue", "Please enter your job title", true, 2000);
                            break;
                        default:
                            break;
                    }
                } else {
                    switch (credState) {
                        case 0:
                            Log.v("XYZ", "buttonMainCreds credState1");
                            psbaAPILogin();
                            break;
                        case 1:
                            credUIState2();
                            Log.v("XYZ", "buttonMainCreds credState2");
                            break;
                        case 2:
                            credUIState3();
                            Log.v("XYZ", "buttonMainCreds credState3");
                            break;
                        case 3:
                            credUIState4();
                            break;
                        case 4:
                            credUIState5();
                            break;
                        case 5:
                            credUIState6();
                            break;
                        case 6:
                            psbaAPIRegisterUser();
                            break;
                        default:
                            break;
                    }
                }
            }
        });

        setupEditTextOneListeners();
        setupEditTextTwoListeners();

        setupEditTextOneSignUp1Listeners();
        setupEditTextTwoSignUp1Listeners();

        setupEditTextOneSignUp2Listeners();
        setupEditTextTwoSignUp2Listeners();

        setupEditTextOneSignUp3Listeners();
        setupEditTextTwoSignUp3Listeners();

        setupEditTextOneSignUp4Listeners();
        setupEditTextTwoSignUp4Listeners();

        setupEditTextOneSignUp5Listeners();
        setupEditTextTwoSignUp5Listeners();

        setupEditTextOneSignUp6Listeners();


        editTextOne.setOnEditorActionListener(this);
        editTextTwo.setOnEditorActionListener(this);
//        editTextOneSignUp1.setOnEditorActionListener(this);
        editTextTwoSignUp1.setOnEditorActionListener(this);
        editTextOneSignUp2.setOnEditorActionListener(this);
        editTextTwoSignUp2.setOnEditorActionListener(this);
        editTextOneSignUp3.setOnEditorActionListener(this);
        editTextTwoSignUp3.setOnEditorActionListener(this);
        editTextOneSignUp4.setOnEditorActionListener(this);
        editTextTwoSignUp4.setOnEditorActionListener(this);
        editTextOneSignUp5.setOnEditorActionListener(this);
        editTextTwoSignUp5.setOnEditorActionListener(this);
        editTextOneSignUp6.setOnEditorActionListener(this);

        editTextOneSignUp1.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN)
                    if ((keyCode == KeyEvent.KEYCODE_DPAD_CENTER) || (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        Log.v("XYZ_FUTURE::: ", "editTextOneSignUp1 onKey");
                        editTextTwoSignUp1.requestFocus();
                        return true;
                    }
                return false;
            }
        });


        SharedPreferences userPrefs = getSharedPreferences("theUser", Context.MODE_PRIVATE);
        boolean isLoggedIn = userPrefs.getBoolean("isLoggedIn", false);

        boolean fromLogin = getIntent().getBooleanExtra("fromLogin", false);

        Log.v("XYZ_FUTURE::: ", "fromLogin is: " + fromLogin + " :: isLoggedIn: " + isLoggedIn);

        if (isLoggedIn) {
            HQ.getInstance().populateTheUserObject();
            Intent alreadyLoggedIntent = new Intent(CredentialsActivity.this, MainActivity.class);
//            alreadyLoggedIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(alreadyLoggedIntent);

        } else {

        }
    }

    @Override protected void onDestroy() { super.onDestroy(); }

    @Override protected void onRestart() {
        super.onRestart();
        Log.v("XYZ_FUTURE::: ", "credentials onReStart");
    }

    @Override protected void onStart() {
        super.onStart();
        Log.v("XYZ_FUTURE::: ", "credentials onStart");
    }

    @Override protected void onResume() {
        super.onResume();

        boolean buttonAlphaFlag = true;

        Log.v("XYZ_FUTURE::: ", "credentials onResume company name is: " + HQ.getInstance().theUser.getCompanyName());


        if (HQ.getInstance().theUser.getCompanyName() != "" && HQ.getInstance().theUser.getCompanyName() != null) {
            textViewOneSignUp5.setText(HQ.getInstance().theUser.getCompanyName());
        } else {
            textViewOneSignUp5.setText("Search Company");
            buttonAlphaFlag = false;
        }

        if (HQ.getInstance().theUser.getBranchName() != "" && HQ.getInstance().theUser.getBranchName() != null) {
            textViewTwoSignUp5.setText(HQ.getInstance().theUser.getBranchName());
            buttonMainCreds.setAlpha(1.0f);
        } else {
            textViewTwoSignUp5.setText("Search Branch");
            buttonAlphaFlag = false;
        }

        if (buttonAlphaFlag == true) {
            buttonMainCreds.setAlpha(1.0f);
        } else {
            buttonMainCreds.setAlpha(0.3f);
        }

    }

    @Override protected void onStop() { super.onStop(); }

    @Override protected void onPause() { super.onPause(); }

    public void credUIState0() {
        imageViewBrandTitle.setAlpha(1.0f);

//        buttonForgotDetails.setAlpha(1.0f);
        buttonForgotDetails.setText(R.string.forgot_details);
        buttonForgotDetails.setClickable(true);

        buttonLogin.setAlpha(1.0f);
        buttonLogin.setText("");

        buttonSignUp.setAlpha(1.0f);
        buttonSignUp.setText(getResources().getString(R.string.sign_up_caps));

        //string
        buttonLoginText.setText(getResources().getString(R.string.login_caps));

        viewMovingUnderline.animate().translationX(dpToPx(35));

        buttonMainCreds.setAlpha(0.3f);
        if (credState != 0) {
            buttonLoginText.setX(dpToPx(35));
            buttonMainCreds.animate().xBy((-backButtonWidth * 0.5f) - dpToPx(6));
            textForMainButton.animate().xBy(-backButtonWidth * 0.5f - dpToPx(6));
        }
        textForMainButton.setText(getResources().getString(R.string.login_caps));

        buttonMainCreds.animate().scaleX(1);

        buttonBackCreds.setClickable(true);
        ScaleAnimation anim = new ScaleAnimation(1.0f, 0.0f, 1.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        anim.setDuration(200);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation) {
                buttonBackCreds.setAlpha(0.0f);
            }
            @Override public void onAnimationRepeat(Animation animation) {}
        });
        buttonBackCreds.startAnimation(anim);

        buttonMainCreds.setAlpha(0.3f);

        //set back to untouched
        editTextOne.setText("");
        editTextOne.setBackground(getResources().getDrawable(R.drawable.edit_text_border_gray));
        inputIconOne.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        textViewOne.setText(getResources().getString(R.string.username_caps));
        textViewOne.setAlpha(1.0f);
        textInputInfoOne.setText("");
        editTextTwo.setText("");
        editTextTwo.setBackground(getResources().getDrawable(R.drawable.edit_text_border_gray));
        textViewTwo.setText(getResources().getString(R.string.password_caps));
        textViewTwo.setAlpha(1.0f);

        credState = 0;
    }

    public void credUIState1() {
        Log.v("XYZ_FUTURE::: ", "credUIState1()");

//        buttonForgotDetails.setAlpha(0.0f);
        buttonForgotDetails.setText("");
        buttonForgotDetails.setClickable(false);

        imageViewBrandTitle.setAlpha(1.0f);

        buttonLogin.setAlpha(1.0f);
        buttonLogin.setText(getResources().getString(R.string.login_caps));

        buttonSignUp.setAlpha(1.0f);
        buttonSignUp.setText("");

        buttonLoginText.setText(getResources().getString(R.string.sign_up_caps));
        buttonLoginText.setX((widthPixels * 0.5f) + dpToPx(6));

        viewMovingUnderline.animate().translationX((widthPixels * 0.5f) + dpToPx(6));

        buttonMainCreds.setAlpha(0.3f);
        buttonMainCreds.setText("");



        buttonBackCreds.setAlpha(1.0f);
        buttonBackCreds.setClickable(true);

        if (credState == 0) {
            ScaleAnimation animeForBackButt = new ScaleAnimation(0.0f, 1.0f, 1.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f);
            animeForBackButt.setDuration(300);
            buttonBackCreds.startAnimation(animeForBackButt);

            buttonMainCreds.animate().xBy((backButtonWidth * 0.5f) + dpToPx(6));
            buttonMainCreds.animate().scaleX(mainButtonScaleDownValue);

            textForMainButton.animate().xBy(backButtonWidth * 0.5f + dpToPx(6));

            slideRelay(relayLogin, POSITION_LEFT);
            slideRelay(relaySignUp1, POSITION_MIDDLE);
        } else if (credState == 2) {
            slideRelay(relaySignUp2, POSITION_RIGHT);
            slideRelay(relaySignUp1, POSITION_MIDDLE);
        }

        //string
        textForMainButton.setText(getResources().getString(R.string.next_caps));

        //set back to untouched
        editTextOneSignUp1.setText("");
        editTextOneSignUp1.setBackground(getResources().getDrawable(R.drawable.edit_text_border_gray));
//        textViewOneSignUp1.setText(getResources().getString(R.string.first_name_caps));
//        textViewOneSignUp1.setAlpha(1.0f);
        editTextTwoSignUp1.setText("");
        editTextTwoSignUp1.setBackground(getResources().getDrawable(R.drawable.edit_text_border_gray));
        textViewTwoSignUp1.setText(getResources().getString(R.string.last_name_caps));
        textViewTwoSignUp1.setAlpha(1.0f);

        credState = 1;
    }

    public void credUIState2() {
        if (credState == 1) {
            slideRelay(relaySignUp1, POSITION_LEFT);
            slideRelay(relaySignUp2, POSITION_MIDDLE);
        } else if (credState == 3) {
            slideRelay(relaySignUp3, POSITION_RIGHT);
            slideRelay(relaySignUp2, POSITION_MIDDLE);
        }


        imageViewBrandTitle.setAlpha(1.0f);

        buttonLogin.setAlpha(1.0f);

        //string
        buttonLogin.setText(getResources().getString(R.string.login_caps));

        buttonSignUp.setAlpha(1.0f);
        buttonSignUp.setText("");

        //string
        buttonLoginText.setText(getResources().getString(R.string.sign_up_caps));
        buttonLoginText.setX((widthPixels * 0.5f) + dpToPx(6));

        viewMovingUnderline.animate().translationX((widthPixels * 0.5f) + dpToPx(6));


        buttonBackCreds.setAlpha(1.0f);
        buttonBackCreds.setClickable(true);

        //string
        textForMainButton.setText(getResources().getString(R.string.next_caps));
        buttonMainCreds.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        buttonMainCreds.setAlpha(0.3f);


        //set back to untouched
        editTextOneSignUp2.setText("");
        editTextOneSignUp2.setBackground(getResources().getDrawable(R.drawable.edit_text_border_gray));
        textViewOneSignUp2.setText(getResources().getString(R.string.username_caps));
        textViewOneSignUp2.setAlpha(1.0f);
        editTextTwoSignUp2.setText("");
        editTextTwoSignUp2.setBackground(getResources().getDrawable(R.drawable.edit_text_border_gray));
        textViewTwoSignUp2.setText(getResources().getString(R.string.psn_id_caps));
        textViewTwoSignUp2.setAlpha(1.0f);

        credState = 2;
    }

    public void credUIState3() {
        if (credState == 2) {
            slideRelay(relaySignUp2, POSITION_LEFT);
            slideRelay(relaySignUp3, POSITION_MIDDLE);
        } else if (credState == 4) {
            slideRelay(relaySignUp4, POSITION_RIGHT);
            slideRelay(relaySignUp3, POSITION_MIDDLE);
        }


        //set back to untouched
        textForMainButton.setText("NEXT");
        buttonMainCreds.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        buttonMainCreds.setAlpha(0.3f);
        editTextOneSignUp3.setText("");
        editTextOneSignUp3.setBackground(getResources().getDrawable(R.drawable.edit_text_border_gray));
        textViewOneSignUp3.setText(getResources().getString(R.string.email_caps));
        textViewOneSignUp3.setAlpha(1.0f);
        editTextTwoSignUp3.setText("");
        editTextTwoSignUp3.setBackground(getResources().getDrawable(R.drawable.edit_text_border_gray));
        textViewTwoSignUp3.setText(getResources().getString(R.string.confirm_email_caps));
        textViewTwoSignUp3.setAlpha(1.0f);

        credState = 3;
    }

    public void credUIState4() {
        if (credState == 3) {
            slideRelay(relaySignUp3, POSITION_LEFT);
            slideRelay(relaySignUp4, POSITION_MIDDLE);
        } else if (credState == 5) {
            slideRelay(relaySignUp5, POSITION_RIGHT);
            slideRelay(relaySignUp4, POSITION_MIDDLE);
        }
        //set back to untouched
        textForMainButton.setText("NEXT");
        buttonMainCreds.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        buttonMainCreds.setAlpha(0.3f);
        editTextOneSignUp4.setText("");
        editTextOneSignUp4.setBackground(getResources().getDrawable(R.drawable.edit_text_border_gray));
        textViewOneSignUp4.setText(getResources().getString(R.string.password_caps));
        textViewOneSignUp4.setAlpha(1.0f);
        editTextTwoSignUp4.setText("");
        editTextTwoSignUp4.setBackground(getResources().getDrawable(R.drawable.edit_text_border_gray));
        textViewTwoSignUp4.setText(getResources().getString(R.string.confirm_password_caps));
        textViewTwoSignUp4.setAlpha(1.0f);

        credState = 4;
    }

    public void credUIState5() {

        if (credState == 4) {
            slideRelay(relaySignUp4, POSITION_LEFT);
            slideRelay(relaySignUp5, POSITION_MIDDLE);
        } else if (credState == 6) {
            slideRelay(relaySignUp6, POSITION_RIGHT);
            slideRelay(relaySignUp5, POSITION_MIDDLE);
        }

        //set back to untouched
        textForMainButton.setText("NEXT");
        buttonMainCreds.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        buttonMainCreds.setAlpha(0.3f);
        editTextOneSignUp5.setText("");
        editTextOneSignUp5.setBackground(getResources().getDrawable(R.drawable.edit_text_border_gray));
        textViewOneSignUp5.setText(getResources().getString(R.string.search_store));
        textViewOneSignUp5.setAlpha(1.0f);

        editTextTwoSignUp5.setText("");
        editTextTwoSignUp5.setBackground(getResources().getDrawable(R.drawable.edit_text_border_gray));
        textViewTwoSignUp5.setText("Search Branch");
        textViewTwoSignUp5.setAlpha(1.0f);

        credState = 5;
    }

    public void credUIState6() {

        if (credState == 5) {
            slideRelay(relaySignUp5, POSITION_LEFT);
            slideRelay(relaySignUp6, POSITION_MIDDLE);
        } else if (credState == 7) {
//            slideRelay(relaySignUp7, POSITION_RIGHT);
            slideRelay(relaySignUp6, POSITION_MIDDLE);
        }


        //set back to untouched
        textForMainButton.setText("COMPLETE");
        buttonMainCreds.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        buttonMainCreds.setAlpha(0.3f);
        editTextOneSignUp6.setText("");
        editTextOneSignUp6.setBackground(getResources().getDrawable(R.drawable.edit_text_border_gray));
        textViewOneSignUp6.setText(getResources().getString(R.string.occupation_caps));
        textViewOneSignUp6.setAlpha(1.0f);

        credState = 6;
    }

//
//    public void addKeyboardWizard() {
//        KeyboardWizard.addKeyboardToggleListener(this, new KeyboardWizard.SoftKeyboardToggleListener() {
//            public void onToggleSoftKeyboard(boolean isVisible) {
////                Log.v("XYZ", "keyboard visible: " + isVisible);
//                isKeyboardDisplayed = isVisible;
//
//                if (isVisible == true) {
//
//                } else if (isVisible == false) {
////                    Log.v("XYZ", "focus is on : " + editTextFocus);
//
////                    if (editTextFocus == 1) {
////                        editTextOne.animate().translationY(inputHeightSlotOne);
////                        editTextOne.clearFocus();
////                    } else if (editTextFocus == 2) {
////                        editTextTwo.animate().translationY(inputHeightSlotTwo);
////                        editTextTwo.clearFocus();
////                        if (credState == 1) {
////                            Log.v("XYZ", "check username");
////                            psbaAPICheckUsername();
////                        }
////                    } else if (editTextFocus == 3) {
////                        editTextThree.animate().translationY(inputHeightSlotThree);
////                        editTextThree.clearFocus();
////                    }
//
////                    editTextFocus = 0;
//
//                    if (!editTextOne.getText().toString().matches("") && !editTextTwo.getText().toString().matches("")) {
//                        Log.v("XYZ", "maybe force login now???");
//                    }
//                }
//            }
//        });
//    }

    // login relay //
    public void setupEditTextOneListeners() {
        editTextOne.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v1, boolean hasFocus) {
                if (hasFocus) {
                    editTextOne.setCursorVisible(true);
                    editTextOne.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                    inputIconOne.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    textViewOne.setAlpha(0.0f);
                    textInputInfoOne.setText("");

                } else if (!hasFocus) {
                    if (editTextOne.getText().toString().matches("")) {
                        editTextOne.setCursorVisible(false);
                        editTextOne.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border_gray));
                        textViewOne.setAlpha(1.0f);
                        buttonMainCreds.setAlpha(0.3f);
                    } else {
                        psbaAPICheckUsername();
                        editTextOne.setCursorVisible(false);
                        editTextOne.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                    }

                    // if all fields filled can move on //
                    if (!editTextOne.getText().toString().matches("") && !editTextTwo.getText().toString().matches("")) {
                        buttonMainCreds.setAlpha(1.0f);
                    }
                }
            }
        });
    }
    public void setupEditTextTwoListeners() {
        editTextTwo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v2, boolean hasFocus) {
                if (hasFocus) {
                    editTextTwo.setCursorVisible(true);
                    editTextTwo.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                    textViewTwo.setAlpha(0.0f);

                } else if (!hasFocus) {
                    if (editTextTwo.getText().toString().matches("")) {
                        editTextTwo.setCursorVisible(false);
                        editTextTwo.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border_gray));
                        textViewTwo.setAlpha(1.0f);
                        buttonMainCreds.setAlpha(0.3f);
                    } else {
                        editTextTwo.setCursorVisible(false);
                        editTextTwo.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                    }

                    // if all fields filled can move on //
                    if (!editTextOne.getText().toString().matches("") && !editTextTwo.getText().toString().matches("")) {
                        buttonMainCreds.setAlpha(1.0f);
                    }
                }
            }
        });
    }

    // first last relay sign up 1 //
    public void setupEditTextOneSignUp1Listeners() {
        editTextOneSignUp1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v1, boolean hasFocus) {
                if (hasFocus) {
                    editTextOneSignUp1.setCursorVisible(true);
                    editTextOneSignUp1.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
//                    textViewOneSignUp1.setAlpha(0.0f);

                } else if (!hasFocus) {
                    if (editTextOneSignUp1.getText().toString().matches("")) {
                        editTextOneSignUp1.setCursorVisible(false);
                        editTextOneSignUp1.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border_gray));
//                        textViewOneSignUp1.setAlpha(1.0f);
                        buttonMainCreds.setAlpha(0.3f);
                    } else {
                        editTextOneSignUp1.setCursorVisible(false);
                        editTextOneSignUp1.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                        HQ.getInstance().theUser.setFirstname(editTextOneSignUp1.getText().toString());
                        Log.v("XYZ_FUTURE::: ", "users first name changed to: " + HQ.getInstance().theUser.getFirstname());
                    }

                    // if all fields filled can move on //
                    if (!editTextOneSignUp1.getText().toString().matches("") && !editTextTwoSignUp1.getText().toString().matches("")) {
                        buttonMainCreds.setAlpha(1.0f);
                    }
                }
            }
        });
    }
    public void setupEditTextTwoSignUp1Listeners() {
        editTextTwoSignUp1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v2, boolean hasFocus) {
                if (hasFocus) {
                    editTextTwoSignUp1.setCursorVisible(true);
                    editTextTwoSignUp1.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                    textViewTwoSignUp1.setAlpha(0.0f);

                } else if (!hasFocus) {
                    if (editTextTwoSignUp1.getText().toString().matches("")) {
                        editTextTwoSignUp1.setCursorVisible(false);
                        editTextTwoSignUp1.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border_gray));
                        textViewTwoSignUp1.setAlpha(1.0f);
                        buttonMainCreds.setAlpha(0.3f);
                    } else {
                        editTextTwoSignUp1.setCursorVisible(false);
                        editTextTwoSignUp1.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                        HQ.getInstance().theUser.setLastname(editTextTwoSignUp1.getText().toString());
                        Log.v("XYZ_FUTURE::: ", "users last name changed to: " + HQ.getInstance().theUser.getLastname());
                    }

                    // if all fields filled can move on //
                    if (!editTextOneSignUp1.getText().toString().matches("") && !editTextTwoSignUp1.getText().toString().matches("")) {
                        buttonMainCreds.setAlpha(1.0f);
                    }
                }
            }
        });
    }

    // user psn relay sign up 2 //
    public void setupEditTextOneSignUp2Listeners() {
        editTextOneSignUp2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v1, boolean hasFocus) {
                if (hasFocus) {
                    editTextOneSignUp2.setCursorVisible(true);
                    editTextOneSignUp2.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                    textViewOneSignUp2.setAlpha(0.0f);

                } else if (!hasFocus) {
                    if (editTextOneSignUp2.getText().toString().matches("")) {
                        editTextOneSignUp2.setCursorVisible(false);
                        editTextOneSignUp2.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border_gray));
                        textViewOneSignUp2.setAlpha(1.0f);
                        inputIconOneSignUp2.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        textInputInfoOneSignUp2.setText("");
                        buttonMainCreds.setAlpha(0.3f);

                    } else {
                        editTextOneSignUp2.setCursorVisible(false);
                        editTextOneSignUp2.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                        buttonMainCreds.setAlpha(1.0f);
                        HQ.getInstance().theUser.setUsername(editTextOneSignUp2.getText().toString());
                        Log.v("XYZ_FUTURE::: ", "users username changed to: " + HQ.getInstance().theUser.getUsername());
                        psbaAPICheckUsername();
                    }
                }
            }
        });
    }
    public void setupEditTextTwoSignUp2Listeners() {
        editTextTwoSignUp2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v2, boolean hasFocus) {
                if (hasFocus) {
                    editTextTwoSignUp2.setCursorVisible(true);
                    editTextTwoSignUp2.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                    textViewTwoSignUp2.setAlpha(0.0f);

                } else if (!hasFocus) {
                    if (editTextTwoSignUp2.getText().toString().matches("")) {
                        editTextTwoSignUp2.setCursorVisible(false);
                        editTextTwoSignUp2.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border_gray));
                        textViewTwoSignUp2.setAlpha(1.0f);
                    } else {
                        editTextTwoSignUp2.setCursorVisible(false);
                        editTextTwoSignUp2.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                        HQ.getInstance().theUser.setPsnid(editTextTwoSignUp2.getText().toString());
                        Log.v("XYZ_FUTURE::: ", "users psnid changed to: " + HQ.getInstance().theUser.getPsnid());
                    }
                }
            }
        });
    }

    // email relay sign up 3 //
    public void setupEditTextOneSignUp3Listeners() {
        editTextOneSignUp3.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v1, boolean hasFocus) {
                if (hasFocus) {
                    editTextOneSignUp3.setCursorVisible(true);
                    editTextOneSignUp3.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                    textViewOneSignUp3.setAlpha(0.0f);
                    textInputInfoOneSignUp3.setText("");
                    inputIconOneSignUp3.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                } else if (!hasFocus) {
                    if (editTextOneSignUp3.getText().toString().matches("")) {
                        editTextOneSignUp3.setCursorVisible(false);
                        editTextOneSignUp3.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border_gray));
                        textViewOneSignUp3.setAlpha(1.0f);
                        buttonMainCreds.setAlpha(0.3f);
                    } else {
                        editTextOneSignUp3.setCursorVisible(false);
                        editTextOneSignUp3.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                        psbaAPICheckEmail();
                    }

                    // if all fields filled can move on //
                    if (!editTextOneSignUp3.getText().toString().matches("") && !editTextTwoSignUp3.getText().toString().matches("") && editTextOneSignUp3.getText().toString().matches(editTextTwoSignUp3.getText().toString())) {
                        buttonMainCreds.setAlpha(1.0f);
                        HQ.getInstance().theUser.setEmail(editTextOneSignUp3.getText().toString());
                        Log.v("XYZ_FUTURE::: ", "users email changed to: " + HQ.getInstance().theUser.getEmail());
                    }
                    if (!editTextOneSignUp3.getText().toString().matches(editTextTwoSignUp3.getText().toString()) && !editTextOneSignUp3.getText().toString().matches("") && !editTextTwoSignUp3.getText().toString().matches("")) {
                        textInputInfoTwoSignUp3.setText("Emails do NOT match");
                        textInputInfoTwoSignUp3.setTextColor(getResources().getColor(R.color.colorLava));
                        editTextTwoSignUp3.setBackground(getResources().getDrawable(R.drawable.edit_text_border_red));
                        inputIconTwoSignUp3.setTextColor(getResources().getColor(R.color.colorMagnesium));
                        buttonMainCreds.setAlpha(0.3f);
                    }
                }
            }
        });
    }
    public void setupEditTextTwoSignUp3Listeners() {
        editTextTwoSignUp3.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v2, boolean hasFocus) {
                if (hasFocus) {
                    editTextTwoSignUp3.setCursorVisible(true);
                    editTextTwoSignUp3.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                    textViewTwoSignUp3.setAlpha(0.0f);
                    textInputInfoTwoSignUp3.setText("");
                    inputIconTwoSignUp3.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                } else if (!hasFocus) {
                    if (editTextTwoSignUp3.getText().toString().matches("")) {
                        editTextTwoSignUp3.setCursorVisible(false);
                        editTextTwoSignUp3.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border_gray));
                        textViewTwoSignUp3.setAlpha(1.0f);
                        buttonMainCreds.setAlpha(0.3f);
                    } else {
                        editTextTwoSignUp3.setCursorVisible(false);
                        editTextTwoSignUp3.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                    }

                    // if all fields filled can move on //
                    if (!editTextOneSignUp3.getText().toString().matches("") && !editTextTwoSignUp3.getText().toString().matches("") && editTextOneSignUp3.getText().toString().matches(editTextTwoSignUp3.getText().toString())) {
                        buttonMainCreds.setAlpha(1.0f);
                        HQ.getInstance().theUser.setEmail(editTextOneSignUp3.getText().toString());
                        Log.v("XYZ_FUTURE::: ", "users email changed to: " + HQ.getInstance().theUser.getEmail());
                    }
                    if (!editTextOneSignUp3.getText().toString().matches(editTextTwoSignUp3.getText().toString()) && !editTextOneSignUp3.getText().toString().matches("") && !editTextTwoSignUp3.getText().toString().matches("")) {
                        textInputInfoTwoSignUp3.setText("Emails do NOT match");
                        textInputInfoTwoSignUp3.setTextColor(getResources().getColor(R.color.colorLava));
                        editTextTwoSignUp3.setBackground(getResources().getDrawable(R.drawable.edit_text_border_red));
                        inputIconTwoSignUp3.setTextColor(getResources().getColor(R.color.colorMagnesium));
                        buttonMainCreds.setAlpha(0.3f);
                    }

                }
            }
        });
    }

    // password relay sign up 4 //
    public void setupEditTextOneSignUp4Listeners() {
        editTextOneSignUp4.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v1, boolean hasFocus) {

                String onePass = editTextOneSignUp4.getText().toString();

                if (hasFocus) {
                    editTextOneSignUp4.setCursorVisible(true);
                    editTextOneSignUp4.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                    textViewOneSignUp4.setAlpha(0.0f);
                    textInputInfoOneSignUp4.setText("");
                    inputIconOneSignUp4.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                } else if (!hasFocus) {



//                    editTextFocus = 0;
                    if (editTextOneSignUp4.getText().toString().matches("")) {
                        editTextOneSignUp4.setCursorVisible(false);
                        editTextOneSignUp4.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border_gray));
                        textViewOneSignUp4.setAlpha(1.0f);
                        buttonMainCreds.setAlpha(0.3f);
                    } else {
                        editTextOneSignUp4.setCursorVisible(false);
                        editTextOneSignUp4.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                    }

                    // if all fields filled can move on // YES
                    if (!editTextOneSignUp4.getText().toString().matches("") && !editTextTwoSignUp4.getText().toString().matches("") && editTextOneSignUp4.getText().toString().matches(editTextTwoSignUp4.getText().toString())) {
                        buttonMainCreds.setAlpha(1.0f);
                        HQ.getInstance().theUser.setPasswd(editTextOneSignUp4.getText().toString());
                        Log.v("XYZ_FUTURE::: ", "users password changed to: " + HQ.getInstance().theUser.getPasswd());
                    }
                    if (!editTextOneSignUp4.getText().toString().matches(editTextTwoSignUp4.getText().toString()) && !editTextOneSignUp4.getText().toString().matches("") && !editTextTwoSignUp4.getText().toString().matches("")) {
                        textInputInfoTwoSignUp4.setText("Passwords do NOT match");
                        textInputInfoTwoSignUp4.setTextColor(getResources().getColor(R.color.colorLava));
                        editTextTwoSignUp4.setBackground(getResources().getDrawable(R.drawable.edit_text_border_red));
                        inputIconTwoSignUp4.setTextColor(getResources().getColor(R.color.colorMagnesium));
                        buttonMainCreds.setAlpha(0.3f);
                    }



                    // this is ridiculous but no time, TODO //
                    if (editTextOneSignUp4.getText().toString().length() < 6) {
                        editTextOneSignUp4.setBackground(getResources().getDrawable(R.drawable.edit_text_border_red));
                        inputIconOneSignUp4.setTextColor(getResources().getColor(R.color.colorMagnesium));
                        buttonMainCreds.setAlpha(0.3f);
                    }

                    boolean hasUppercase = !onePass.equals(onePass.toLowerCase());
//                    boolean hasLowercase = !password.equals(password.toUpperCase());

                    if (!hasUppercase) {
                        Log.v("XYZ_FUTURE::: ", "Must have an uppercase Character");
                        slideInAlert("Cannot continue", "Password must contain at least one uppercase letter and one number.", true, 2000);

                        editTextTwoSignUp4.setBackground(getResources().getDrawable(R.drawable.edit_text_border_red));
                        inputIconTwoSignUp4.setTextColor(getResources().getColor(R.color.colorMagnesium));
                        buttonMainCreds.setAlpha(0.3f);
                    }
//                    if(!hasLowercase)Log.v("XYZ_FUTURE::: ", "Must have a lowercase Character");


                    if(onePass.matches(".*\\d.*")){
                        // contains a number
                    } else{
                        // does not contain a number
                        slideInAlert("Cannot continue", "Password must contain at least one uppercase letter and one number.", true, 2000);

                        editTextTwoSignUp4.setBackground(getResources().getDrawable(R.drawable.edit_text_border_red));
                        inputIconTwoSignUp4.setTextColor(getResources().getColor(R.color.colorMagnesium));
                        buttonMainCreds.setAlpha(0.3f);
                    }

                }
            }
        });
    }
    public void setupEditTextTwoSignUp4Listeners() {
        editTextTwoSignUp4.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v2, boolean hasFocus) {

                String twoPass = editTextTwoSignUp4.getText().toString();


                if (hasFocus) {
                    editTextTwoSignUp4.setCursorVisible(true);
                    editTextTwoSignUp4.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                    textViewTwoSignUp4.setAlpha(0.0f);
                    textInputInfoTwoSignUp4.setText("");
                    inputIconTwoSignUp4.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                } else if (!hasFocus) {
                    if (editTextTwoSignUp4.getText().toString().matches("")) {
                        editTextTwoSignUp4.setCursorVisible(false);
                        editTextTwoSignUp4.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border_gray));
                        textViewTwoSignUp4.setAlpha(1.0f);
                        buttonMainCreds.setAlpha(0.3f);
                    } else {
                        editTextTwoSignUp4.setCursorVisible(false);
                        editTextTwoSignUp4.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                    }

                    // if all fields filled can move on //
                    if (!editTextOneSignUp4.getText().toString().matches("") && !editTextTwoSignUp4.getText().toString().matches("") && editTextOneSignUp4.getText().toString().matches(editTextTwoSignUp4.getText().toString())) {
                        buttonMainCreds.setAlpha(1.0f);
                        HQ.getInstance().theUser.setPasswd(editTextOneSignUp4.getText().toString());
                        Log.v("XYZ_FUTURE::: ", "users password changed to: " + HQ.getInstance().theUser.getPasswd());
                    }
                    if (!editTextOneSignUp4.getText().toString().matches(editTextTwoSignUp4.getText().toString()) && !editTextOneSignUp4.getText().toString().matches("") && !editTextTwoSignUp4.getText().toString().matches("")) {
                        textInputInfoTwoSignUp4.setText("Passwords do NOT match");
                        textInputInfoTwoSignUp4.setTextColor(getResources().getColor(R.color.colorLava));
                        editTextTwoSignUp4.setBackground(getResources().getDrawable(R.drawable.edit_text_border_red));
                        inputIconTwoSignUp4.setTextColor(getResources().getColor(R.color.colorMagnesium));
                        buttonMainCreds.setAlpha(0.3f);
                    }




                    // this is ridiculous but no time, TODO //
                    if (editTextTwoSignUp4.getText().toString().length() < 6) {
                        editTextTwoSignUp4.setBackground(getResources().getDrawable(R.drawable.edit_text_border_red));
                        inputIconTwoSignUp4.setTextColor(getResources().getColor(R.color.colorMagnesium));
                        buttonMainCreds.setAlpha(0.3f);
                    }

                    boolean hasUppercase = !twoPass.equals(twoPass.toLowerCase());
//                    boolean hasLowercase = !password.equals(password.toUpperCase());

                    if (!hasUppercase) {
                        Log.v("XYZ_FUTURE::: ", "Must have an uppercase Character");
                        slideInAlert("Cannot continue", "Password must be at least 6 characters long and contain at least one uppercase letter and one number.", true, 2000);

                        editTextTwoSignUp4.setBackground(getResources().getDrawable(R.drawable.edit_text_border_red));
                        inputIconTwoSignUp4.setTextColor(getResources().getColor(R.color.colorMagnesium));
                        buttonMainCreds.setAlpha(0.3f);
                    }
//                    if(!hasLowercase)Log.v("XYZ_FUTURE::: ", "Must have a lowercase Character");

                    if(twoPass.matches(".*\\d.*")){
                        // contains a number
                    } else{
                        // does not contain a number
                        slideInAlert("Cannot continue", "Password must be at least 6 characters long and contain at least one uppercase letter and one number.", true, 2000);

                        editTextTwoSignUp4.setBackground(getResources().getDrawable(R.drawable.edit_text_border_red));
                        inputIconTwoSignUp4.setTextColor(getResources().getColor(R.color.colorMagnesium));
                        buttonMainCreds.setAlpha(0.3f);
                    }
                }
            }
        });
    }

    // store relay sign up 5 //
    public void setupEditTextOneSignUp5Listeners() {
        editTextOneSignUp5.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v1, boolean hasFocus) {
                if (hasFocus) {

                    Log.v("XYZ_FUTURE::: ", "editTextOneSignUp5 hasFocus");
                    editTextOneSignUp5.clearFocus();
                    Intent intentOptions = new Intent(CredentialsActivity.this, OptionsActivity.class);
                    startActivity(intentOptions);
                    overridePendingTransition(R.anim.slide_in_up, R.anim.fade_out);
//                    editTextOneSignUp5.setCursorVisible(true);
//                    editTextOneSignUp5.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
//                    textViewOneSignUp5.setAlpha(0.0f);

                } else if (!hasFocus) {
//                    editTextFocus = 0;
//                    if (editTextOneSignUp5.getText().toString().matches("")) {
//                        editTextOneSignUp5.setCursorVisible(false);
//                        editTextOneSignUp5.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border_gray));
//                        textViewOneSignUp5.setAlpha(1.0f);
//                    } else {
//                        editTextOneSignUp5.setCursorVisible(false);
//                        editTextOneSignUp5.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
//                    }
                }
            }
        });
    }
    public void setupEditTextTwoSignUp5Listeners() {
        editTextTwoSignUp5.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v1, boolean hasFocus) {
                if (hasFocus) {

                    Log.v("XYZ_FUTURE::: ", "editTextTwoSignUp5 hasFocus");
                    editTextTwoSignUp5.clearFocus();
                    Intent intentOptions = new Intent(CredentialsActivity.this, OptionsActivity.class);
                    startActivity(intentOptions);
                    overridePendingTransition(R.anim.slide_in_up, R.anim.fade_out);
//                    editTextOneSignUp5.setCursorVisible(true);
//                    editTextOneSignUp5.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
//                    textViewOneSignUp5.setAlpha(0.0f);

                } else if (!hasFocus) {
//                    editTextFocus = 0;
//                    if (editTextOneSignUp5.getText().toString().matches("")) {
//                        editTextOneSignUp5.setCursorVisible(false);
//                        editTextOneSignUp5.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border_gray));
//                        textViewOneSignUp5.setAlpha(1.0f);
//                    } else {
//                        editTextOneSignUp5.setCursorVisible(false);
//                        editTextOneSignUp5.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
//                    }
                }
            }
        });
    }

    // occupation relay sign up 6 //
    public void setupEditTextOneSignUp6Listeners() {
        editTextOneSignUp6.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v1, boolean hasFocus) {
                if (hasFocus) {
                    editTextOneSignUp6.setCursorVisible(true);
                    editTextOneSignUp6.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                    textViewOneSignUp6.setAlpha(0.0f);
                    buttonMainCreds.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                    buttonMainCreds.setAlpha(0.3f);


                } else if (!hasFocus) {
//                    editTextFocus = 0;
                    if (editTextOneSignUp6.getText().toString().matches("")) {
                        editTextOneSignUp6.setCursorVisible(false);
                        editTextOneSignUp6.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border_gray));
                        buttonMainCreds.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                        buttonMainCreds.setAlpha(0.3f);

                    } else {
                        editTextOneSignUp6.setCursorVisible(false);
                        editTextOneSignUp6.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                        HQ.getInstance().theUser.setOccupation(editTextOneSignUp6.getText().toString());
                        Log.v("XYZ_FUTURE::: ", "theUser changed occupation to: " + HQ.getInstance().theUser.getOccupation());
                        buttonMainCreds.setAlpha(1.0f);
                        buttonMainCreds.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));

                    }
                }
            }
        });
    }



    // misc //
    private int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
    private int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }


    @Override public boolean dispatchTouchEvent(MotionEvent event) {
//        Log.v("XYZ_FUTURE::: ", "dispatchTouchEvent");
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
//        long downTime = SystemClock.uptimeMillis();
//        long eventTime = SystemClock.uptimeMillis() + 100;
//        float x = 60.0f;
//        float y = 60.0f;
//        // List of meta states found here:     developer.android.com/reference/android/view/KeyEvent.html#getMetaState()
//        int metaState = 0;
//        MotionEvent motionEvent = MotionEvent.obtain(downTime, eventTime, MotionEvent.ACTION_DOWN, x, y, metaState);
//
//        // Dispatch touch event to view
//        this.dispatchTouchEvent(motionEvent);

        return true;
    }


    public void setInitialValues() {
        backButtonWidth = widthPixels * 0.5f - dpToPx(38);
        mainButtonScaleDownValue = (backButtonWidth) / (widthPixels - dpToPx(64));
    }

    public void setCustomFont() {
        buttonLogin.setTypeface(HQ.getInstance().sstFontRoman);
        buttonLoginText.setTypeface(HQ.getInstance().sstFontRoman);
        buttonSignUp.setTypeface(HQ.getInstance().sstFontRoman);

        // login relay //
        textViewStepPrompt.setTypeface(HQ.getInstance().sstFontBold);
        textViewStepInfo.setTypeface(HQ.getInstance().sstFontRoman);

        editTextOne.setTypeface(HQ.getInstance().sstFontRoman);
        inputIconOne.setTypeface(HQ.getInstance().fontAwesome);
        textViewOne.setTypeface(HQ.getInstance().sstFontRoman);
        textRequiredOne.setTypeface(HQ.getInstance().sstFontRoman);
        textInputInfoOne.setTypeface(HQ.getInstance().sstFontRoman);

        editTextTwo.setTypeface(HQ.getInstance().sstFontRoman);
        textViewTwo.setTypeface(HQ.getInstance().sstFontRoman);
        inputIconTwo.setTypeface(HQ.getInstance().fontAwesome);
        textRequiredTwo.setTypeface(HQ.getInstance().sstFontRoman);
        textInputInfoTwo.setTypeface(HQ.getInstance().sstFontRoman);


        // first last relay sign up 1 //
        textViewStepPromptSignUp1.setTypeface(HQ.getInstance().sstFontBold);
        textViewStepInfoSignUp1.setTypeface(HQ.getInstance().sstFontRoman);

        editTextOneSignUp1.setTypeface(HQ.getInstance().sstFontRoman);
        inputIconOneSignUp1.setTypeface(HQ.getInstance().fontAwesome);
//        textViewOneSignUp1.setTypeface(HQ.getInstance().sstFontRoman);
        textRequiredOneSignUp1.setTypeface(HQ.getInstance().sstFontRoman);
        textInputInfoOneSignUp1.setTypeface(HQ.getInstance().sstFontRoman);

        editTextTwoSignUp1.setTypeface(HQ.getInstance().sstFontRoman);
        textViewTwoSignUp1.setTypeface(HQ.getInstance().sstFontRoman);
        inputIconTwoSignUp1.setTypeface(HQ.getInstance().fontAwesome);
        textRequiredTwoSignUp1.setTypeface(HQ.getInstance().sstFontRoman);
        textInputInfoTwoSignUp1.setTypeface(HQ.getInstance().sstFontRoman);

        // user psn relay sign up 2 //
        textViewStepPromptSignUp2.setTypeface(HQ.getInstance().sstFontBold);
        textViewStepInfoSignUp2.setTypeface(HQ.getInstance().sstFontRoman);

        editTextOneSignUp2.setTypeface(HQ.getInstance().sstFontRoman);
        inputIconOneSignUp2.setTypeface(HQ.getInstance().fontAwesome);
        textViewOneSignUp2.setTypeface(HQ.getInstance().sstFontRoman);
        textRequiredOneSignUp2.setTypeface(HQ.getInstance().sstFontRoman);
        textInputInfoOneSignUp2.setTypeface(HQ.getInstance().sstFontRoman);

        editTextTwoSignUp2.setTypeface(HQ.getInstance().sstFontRoman);
        textViewTwoSignUp2.setTypeface(HQ.getInstance().sstFontRoman);
//        inputIconTwoSignUp2.setTypeface(HQ.getInstance().fontAwesome);
        textRequiredTwoSignUp2.setTypeface(HQ.getInstance().sstFontRoman);
        textInputInfoTwoSignUp2.setTypeface(HQ.getInstance().sstFontRoman);

        // email relay sign up 3 //
        textViewStepPromptSignUp3.setTypeface(HQ.getInstance().sstFontBold);
        textViewStepInfoSignUp3.setTypeface(HQ.getInstance().sstFontRoman);

        editTextOneSignUp3.setTypeface(HQ.getInstance().sstFontRoman);
        inputIconOneSignUp3.setTypeface(HQ.getInstance().fontAwesome);
        textViewOneSignUp3.setTypeface(HQ.getInstance().sstFontRoman);
        textRequiredOneSignUp3.setTypeface(HQ.getInstance().sstFontRoman);
        textInputInfoOneSignUp3.setTypeface(HQ.getInstance().sstFontRoman);

        editTextTwoSignUp3.setTypeface(HQ.getInstance().sstFontRoman);
        textViewTwoSignUp3.setTypeface(HQ.getInstance().sstFontRoman);
        inputIconTwoSignUp3.setTypeface(HQ.getInstance().fontAwesome);
        textRequiredTwoSignUp3.setTypeface(HQ.getInstance().sstFontRoman);
        textInputInfoTwoSignUp3.setTypeface(HQ.getInstance().sstFontRoman);

        // password relay sign up 4 //
        textViewStepPromptSignUp4.setTypeface(HQ.getInstance().sstFontBold);
        textViewStepInfoSignUp4.setTypeface(HQ.getInstance().sstFontRoman);

        editTextOneSignUp4.setTypeface(HQ.getInstance().sstFontRoman);
        inputIconOneSignUp4.setTypeface(HQ.getInstance().fontAwesome);
        textViewOneSignUp4.setTypeface(HQ.getInstance().sstFontRoman);
        textRequiredOneSignUp4.setTypeface(HQ.getInstance().sstFontRoman);
        textInputInfoOneSignUp4.setTypeface(HQ.getInstance().sstFontRoman);

        editTextTwoSignUp4.setTypeface(HQ.getInstance().sstFontRoman);
        textViewTwoSignUp4.setTypeface(HQ.getInstance().sstFontRoman);
        inputIconTwoSignUp4.setTypeface(HQ.getInstance().fontAwesome);
        textRequiredTwoSignUp4.setTypeface(HQ.getInstance().sstFontRoman);
        textInputInfoTwoSignUp4.setTypeface(HQ.getInstance().sstFontRoman);

        // store relay sign up 5 //
        textViewStepPromptSignUp5.setTypeface(HQ.getInstance().sstFontBold);
        textViewStepInfoSignUp5.setTypeface(HQ.getInstance().sstFontRoman);

        editTextOneSignUp5.setTypeface(HQ.getInstance().sstFontRoman);
        inputIconOneSignUp5.setTypeface(HQ.getInstance().fontAwesome);
        textViewOneSignUp5.setTypeface(HQ.getInstance().sstFontRoman);
        textRequiredOneSignUp5.setTypeface(HQ.getInstance().sstFontRoman);
        textInputInfoOneSignUp5.setTypeface(HQ.getInstance().fontAwesome);

        editTextTwoSignUp5.setTypeface(HQ.getInstance().sstFontRoman);
        inputIconTwoSignUp5.setTypeface(HQ.getInstance().fontAwesome);
        textViewTwoSignUp5.setTypeface(HQ.getInstance().sstFontRoman);
        textRequiredTwoSignUp5.setTypeface(HQ.getInstance().sstFontRoman);
        textInputInfoTwoSignUp5.setTypeface(HQ.getInstance().fontAwesome);

        // occupation relay sign up 6 //
        textViewStepPromptSignUp6.setTypeface(HQ.getInstance().sstFontBold);
        textViewStepInfoSignUp6.setTypeface(HQ.getInstance().sstFontRoman);

        editTextOneSignUp6.setTypeface(HQ.getInstance().sstFontRoman);
        inputIconOneSignUp6.setTypeface(HQ.getInstance().fontAwesome);
        textViewOneSignUp6.setTypeface(HQ.getInstance().sstFontRoman);
        textRequiredOneSignUp6.setTypeface(HQ.getInstance().sstFontRoman);




        buttonMainCreds.setTypeface(HQ.getInstance().sstFontRoman);
        buttonBackCreds.setTypeface(HQ.getInstance().sstFontRoman);
        buttonForgotDetails.setTypeface(HQ.getInstance().sstFontRoman);
        textForMainButton.setTypeface(HQ.getInstance().sstFontRoman);
        psbaAlertTitle.setTypeface(HQ.getInstance().sstFontBold);
        psbaAlertBody.setTypeface(HQ.getInstance().sstFontRoman);
    }

    public void findViews() {
        relayLogin = (RelativeLayout) findViewById(R.id.creds_relay_login);
        relaySignUp1 = (RelativeLayout) findViewById(R.id.creds_relay_sign_up_1);
        relaySignUp2 = (RelativeLayout) findViewById(R.id.creds_relay_sign_up_2);
        relaySignUp3 = (RelativeLayout) findViewById(R.id.creds_relay_sign_up_3);
        relaySignUp4 = (RelativeLayout) findViewById(R.id.creds_relay_sign_up_4);
        relaySignUp5 = (RelativeLayout) findViewById(R.id.creds_relay_sign_up_5);
        relaySignUp6 = (RelativeLayout) findViewById(R.id.creds_relay_sign_up_6);

        imageViewBrandTitle = (ImageView) findViewById(R.id.creds_brand_title_image_view);
        buttonLogin = (Button) findViewById(R.id.creds_login_button);
        buttonLoginText = (TextView) findViewById(R.id.creds_login_button_text);
        buttonSignUp = (Button) findViewById(R.id.creds_sign_up_button);
        viewLoginUnderline = findViewById(R.id.creds_login_underline);
        viewSignupUnderline = findViewById(R.id.creds_signup_underline);
        viewMovingUnderline = findViewById(R.id.creds_moving_underline);

        // login relay //
        textViewStepPrompt = (TextView) findViewById(R.id.creds_text_view_step_prompt);
        textViewStepInfo = (TextView) findViewById(R.id.creds_text_view_step_info);

        editTextOne = (EditText) findViewById(R.id.creds_edit_text_one);
        inputIconOne = (TextView) findViewById(R.id.creds_input_icon_1);
        textViewOne = (TextView) findViewById(R.id.creds_text_view_one);
        textRequiredOne = (TextView) findViewById(R.id.creds_required_text_one);
        textInputInfoOne = (TextView) findViewById(R.id.creds_input_info_text_one);

        editTextTwo = (EditText) findViewById(R.id.creds_edit_text_two);
        inputIconTwo = (TextView) findViewById(R.id.creds_input_icon_2);
        textViewTwo = (TextView) findViewById(R.id.creds_text_view_two);
        textRequiredTwo = (TextView) findViewById(R.id.creds_required_text_two);
        textInputInfoTwo = (TextView) findViewById(R.id.creds_input_info_text_two);

        // first last relay sign up 1 //
        textViewStepPromptSignUp1 = (TextView) findViewById(R.id.creds_text_view_step_prompt_sign_up_1);
        textViewStepInfoSignUp1 = (TextView) findViewById(R.id.creds_text_view_step_info_sign_up_1);

        editTextOneSignUp1 = (EditText) findViewById(R.id.creds_edit_text_one_sign_up_1);
        inputIconOneSignUp1 = (TextView) findViewById(R.id.creds_input_icon_1_sign_up_1);
//        textViewOneSignUp1 = (TextView) findViewById(R.id.creds_text_view_one_sign_up_1);
        textRequiredOneSignUp1 = (TextView) findViewById(R.id.creds_required_text_one_sign_up_1);
        textInputInfoOneSignUp1 = (TextView) findViewById(R.id.creds_input_info_text_one_sign_up_1);

        editTextTwoSignUp1 = (EditText) findViewById(R.id.creds_edit_text_two_sign_up_1);
        inputIconTwoSignUp1 = (TextView) findViewById(R.id.creds_input_icon_2_sign_up_1);
        textViewTwoSignUp1 = (TextView) findViewById(R.id.creds_text_view_two_sign_up_1);
        textRequiredTwoSignUp1 = (TextView) findViewById(R.id.creds_required_text_two_sign_up_1);
        textInputInfoTwoSignUp1 = (TextView) findViewById(R.id.creds_input_info_text_two_sign_up_1);

        // user psn relay sign up 2 //
        textViewStepPromptSignUp2 = (TextView) findViewById(R.id.creds_text_view_step_prompt_sign_up_2);
        textViewStepInfoSignUp2 = (TextView) findViewById(R.id.creds_text_view_step_info_sign_up_2);

        editTextOneSignUp2 = (EditText) findViewById(R.id.creds_edit_text_one_sign_up_2);
        inputIconOneSignUp2 = (TextView) findViewById(R.id.creds_input_icon_1_sign_up_2);
        textViewOneSignUp2 = (TextView) findViewById(R.id.creds_text_view_one_sign_up_2);
        textRequiredOneSignUp2 = (TextView) findViewById(R.id.creds_required_text_one_sign_up_2);
        textInputInfoOneSignUp2 = (TextView) findViewById(R.id.creds_input_info_text_one_sign_up_2);

        editTextTwoSignUp2 = (EditText) findViewById(R.id.creds_edit_text_two_sign_up_2);
        inputIconTwoSignUp2 = (ImageView) findViewById(R.id.creds_input_icon_2_sign_up_2);
        textViewTwoSignUp2 = (TextView) findViewById(R.id.creds_text_view_two_sign_up_2);
        textRequiredTwoSignUp2 = (TextView) findViewById(R.id.creds_required_text_two_sign_up_2);
        textInputInfoTwoSignUp2 = (TextView) findViewById(R.id.creds_input_info_text_two_sign_up_2);

        // email relay sign up 3 //
        textViewStepPromptSignUp3 = (TextView) findViewById(R.id.creds_text_view_step_prompt_sign_up_3);
        textViewStepInfoSignUp3 = (TextView) findViewById(R.id.creds_text_view_step_info_sign_up_3);

        editTextOneSignUp3 = (EditText) findViewById(R.id.creds_edit_text_one_sign_up_3);
        inputIconOneSignUp3 = (TextView) findViewById(R.id.creds_input_icon_1_sign_up_3);
        textViewOneSignUp3 = (TextView) findViewById(R.id.creds_text_view_one_sign_up_3);
        textRequiredOneSignUp3 = (TextView) findViewById(R.id.creds_required_text_one_sign_up_3);
        textInputInfoOneSignUp3 = (TextView) findViewById(R.id.creds_input_info_text_one_sign_up_3);

        editTextTwoSignUp3 = (EditText) findViewById(R.id.creds_edit_text_two_sign_up_3);
        inputIconTwoSignUp3 = (TextView) findViewById(R.id.creds_input_icon_2_sign_up_3);
        textViewTwoSignUp3 = (TextView) findViewById(R.id.creds_text_view_two_sign_up_3);
        textRequiredTwoSignUp3 = (TextView) findViewById(R.id.creds_required_text_two_sign_up_3);
        textInputInfoTwoSignUp3 = (TextView) findViewById(R.id.creds_input_info_text_two_sign_up_3);

        // password relay sign up 4 //
        textViewStepPromptSignUp4 = (TextView) findViewById(R.id.creds_text_view_step_prompt_sign_up_4);
        textViewStepInfoSignUp4 = (TextView) findViewById(R.id.creds_text_view_step_info_sign_up_4);

        editTextOneSignUp4 = (EditText) findViewById(R.id.creds_edit_text_one_sign_up_4);
        inputIconOneSignUp4 = (TextView) findViewById(R.id.creds_input_icon_1_sign_up_4);
        textViewOneSignUp4 = (TextView) findViewById(R.id.creds_text_view_one_sign_up_4);
        textRequiredOneSignUp4 = (TextView) findViewById(R.id.creds_required_text_one_sign_up_4);
        textInputInfoOneSignUp4 = (TextView) findViewById(R.id.creds_input_info_text_one_sign_up_4);

        editTextTwoSignUp4 = (EditText) findViewById(R.id.creds_edit_text_two_sign_up_4);
        inputIconTwoSignUp4 = (TextView) findViewById(R.id.creds_input_icon_2_sign_up_4);
        textViewTwoSignUp4 = (TextView) findViewById(R.id.creds_text_view_two_sign_up_4);
        textRequiredTwoSignUp4 = (TextView) findViewById(R.id.creds_required_text_two_sign_up_4);
        textInputInfoTwoSignUp4 = (TextView) findViewById(R.id.creds_input_info_text_two_sign_up_4);

        // store relay sign up 5 //
        textViewStepPromptSignUp5 = (TextView) findViewById(R.id.creds_text_view_step_prompt_sign_up_5);
        textViewStepInfoSignUp5 = (TextView) findViewById(R.id.creds_text_view_step_info_sign_up_5);

        editTextOneSignUp5 = (EditText) findViewById(R.id.creds_edit_text_one_sign_up_5);
        inputIconOneSignUp5 = (TextView) findViewById(R.id.creds_input_icon_1_sign_up_5);
        textViewOneSignUp5 = (TextView) findViewById(R.id.creds_text_view_one_sign_up_5);
        textRequiredOneSignUp5 = (TextView) findViewById(R.id.creds_required_text_one_sign_up_5);
        textInputInfoOneSignUp5 = (TextView) findViewById(R.id.creds_input_info_text_one_sign_up_5);

        editTextTwoSignUp5 = (EditText) findViewById(R.id.creds_edit_text_two_sign_up_5);
        inputIconTwoSignUp5 = (TextView) findViewById(R.id.creds_input_icon_2_sign_up_5);
        textViewTwoSignUp5 = (TextView) findViewById(R.id.creds_text_view_two_sign_up_5);
        textRequiredTwoSignUp5 = (TextView) findViewById(R.id.creds_required_text_two_sign_up_5);
        textInputInfoTwoSignUp5 = (TextView) findViewById(R.id.creds_input_info_text_two_sign_up_5);

        // occupation relay sign up 6 //
        textViewStepPromptSignUp6 = (TextView) findViewById(R.id.creds_text_view_step_prompt_sign_up_6);
        textViewStepInfoSignUp6 = (TextView) findViewById(R.id.creds_text_view_step_info_sign_up_6);

        editTextOneSignUp6 = (EditText) findViewById(R.id.creds_edit_text_one_sign_up_6);
        inputIconOneSignUp6 = (TextView) findViewById(R.id.creds_input_icon_1_sign_up_6);
        textViewOneSignUp6 = (TextView) findViewById(R.id.creds_text_view_one_sign_up_6);
        textRequiredOneSignUp6 = (TextView) findViewById(R.id.creds_required_text_one_sign_up_6);


        // bottom elements //
        buttonMainCreds = (Button) findViewById(R.id.creds_main_button);
        buttonBackCreds = (Button) findViewById(R.id.creds_back_button);
        buttonForgotDetails = (Button) findViewById(R.id.creds_button_forgot_details);
        textForMainButton = (TextView) findViewById(R.id.creds_main_button_text);

        psbaAlertOverlay = findViewById(R.id.psba_alert_overlay);
        psbaAlert = (RelativeLayout) findViewById(R.id.psba_alert);
        psbaAlertGif = (GifImageView) findViewById(R.id.psba_alert_gif);
        psbaAlertTitle = (TextView) findViewById(R.id.psba_alert_title);
        psbaAlertBody = (TextView) findViewById(R.id.psba_alert_body);
    }

    public void setInitialUIPosAndSize() {

        relaySignUp1.setX(widthPixels);
        relaySignUp2.setX(widthPixels);
        relaySignUp3.setX(widthPixels);
        relaySignUp4.setX(widthPixels);
        relaySignUp5.setX(widthPixels);
        relaySignUp6.setX(widthPixels);

        buttonLogin.getLayoutParams().width = (int) (widthPixels * 0.5) - (dpToPx(41));
        buttonLogin.requestLayout();
        //set text to "" so we can see the blue
        buttonLogin.setText("");

        buttonLoginText.getLayoutParams().width = (int) (widthPixels * 0.5) - (dpToPx(41));
        buttonLoginText.requestLayout();

        buttonSignUp.getLayoutParams().width = (int) (widthPixels * 0.5) - (dpToPx(41));
        buttonSignUp.requestLayout();

        viewLoginUnderline.getLayoutParams().width = (int) (widthPixels * 0.5) - (dpToPx(41));
        viewSignupUnderline.getLayoutParams().width = (int) (widthPixels * 0.5) - (dpToPx(41));
        viewMovingUnderline.getLayoutParams().width = (int) (widthPixels * 0.5) - (dpToPx(41));
        viewMovingUnderline.setX(dpToPx(35));

        buttonMainCreds.getLayoutParams().width = widthPixels - dpToPx(64);
        buttonMainCreds.requestLayout();
        buttonMainCreds.setX(dpToPx(32));
        buttonMainCreds.setAlpha(0.3f);

        buttonBackCreds.getLayoutParams().width = (int) backButtonWidth;
        buttonBackCreds.requestLayout();
        buttonBackCreds.setX(dpToPx(32));
        buttonBackCreds.setAlpha(0.0f);

        textForMainButton.getLayoutParams().width = (int) backButtonWidth;
        textForMainButton.setX(widthPixels * 0.5f - backButtonWidth * 0.5f);
        textForMainButton.setZ(100);
        textForMainButton.setText(getResources().getString(R.string.login_caps));

        psbaAlert.getLayoutParams().width = widthPixels - dpToPx(50);
        psbaAlert.setY(100);
        psbaAlert.setX(widthPixels);
        psbaAlert.setZ(102);

        psbaAlertOverlay.setZ(101);

    }

    public void slideRelay(RelativeLayout relay, String whereTo) {
        switch (whereTo) {
            case POSITION_LEFT:
                relay.animate().translationX(-widthPixels);
                break;
            case POSITION_RIGHT:
                relay.animate().translationX(widthPixels);
                break;
            case POSITION_MIDDLE:
                relay.animate().translationX(0);
                break;
            default:
                break;
        }
    }

    public void slideInAlert(String title, String body, final boolean bgVisible, int timeOpen) {
        if (bgVisible == true) {
            psbaAlertOverlay.animate().alpha(0.4f);
            psbaAlertOverlay.setClickable(true);
        }

        psbaAlert.animate().translationX(widthPixels - psbaAlert.getWidth());
        psbaAlertTitle.setText(title);
        psbaAlertBody.setText(body);

        Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {
                slideOutAlert(bgVisible);
            }
        };
        handler.postDelayed(r, timeOpen);
    }

    public void slideOutAlert(boolean bgVisible) {
        if (bgVisible == true) {
            psbaAlertOverlay.animate().alpha(0.0f);
            psbaAlertOverlay.setClickable(false);
        }
        if (isRegiSuccess == true) {
            HQ.getInstance().populateUserPrefsWithLogin();
            Intent intent = new Intent(CredentialsActivity.this, MainActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_up, R.anim.fade_out);
            isRegiSuccess = false;
        }
        psbaAlert.animate().translationX(widthPixels);
    }


//    public void addKeyboardWizard() {
//        KeyboardWizard.addKeyboardToggleListener(this, new KeyboardWizard.SoftKeyboardToggleListener() {
//            public void onToggleSoftKeyboard(boolean isVisible) {
//                Log.v("XYZ", "keyboard visible: " + isVisible);
//                isKeyboardDisplayed = isVisible;
//
//                if (isVisible == true) {
//
//                } else if (isVisible == false) {
//                    if (getCurrentFocus() != null) {
//                        getCurrentFocus().clearFocus();
//                    }
//
//                }
//            }
//        });
//    }

    // API //
    public void psbaAPIRegisterUser() {

//        HQ.getInstance().removeNullOnUser();

        AsyncTask asyncTask = new AsyncTask() {
            @Override protected Object doInBackground(Object[] params) {
                OkHttpClient client = new OkHttpClient();

                RequestBody formBody = new FormBody.Builder()
                        .add("firstname", HQ.getInstance().theUser.getFirstname())
                        .add("lastname", HQ.getInstance().theUser.getLastname())
                        .add("username", HQ.getInstance().theUser.getUsername())
                        .add("psnid", HQ.getInstance().theUser.getPsnid())
                        .add("email", HQ.getInstance().theUser.getEmail())
                        .add("password", HQ.getInstance().theUser.getPasswd())
                        .add("company", HQ.getInstance().theUser.getCompanyId())
                        .add("branchname", HQ.getInstance().theUser.getBranchName())
                        .add("addressone", HQ.getInstance().theUser.getAddress1())
                        .add("addresstwo", HQ.getInstance().theUser.getAddress2())
                        .add("town", HQ.getInstance().theUser.getTown())
                        .add("county", HQ.getInstance().theUser.getCountry())
                        .add("postcode", HQ.getInstance().theUser.getPostCode())
                        .add("occupation", HQ.getInstance().theUser.getOccupation())
                        .add("branch_id", HQ.getInstance().theUser.getBranch_id())
                        .add("answers", HQ.getInstance().theUser.getExtras())
                        .build();
                Request request = new Request.Builder()
                        .url("http://psba.pixelbeard.co/api/users/register")
                        .post(formBody)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    JSONObject responseJSON = new JSONObject(response.body().string());

                    if (responseJSON.getString("message").matches("Account successfully registered")) {
                        Log.v("XYZ_FUTURE::: ", "REGI SUCCESS!!!!!!!!!!!!!!!!!!" + HQ.getInstance().theUser.toString());
                        return "success";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return "fail";
            }

            @Override protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                if (o.equals("success")) {
                    isRegiSuccess = true;
                    slideInAlert("SUCCESS", "You have been successfully registered!", true, 2000);
                    psbaAPILogin();
                } else {
                    slideInAlert("FAILURE", "We could not register an account with the information provided.", true, 2000);
                }
            }
        };
        asyncTask.execute();
    }

    public void psbaAPILogin() {
        String use = "";
        String pass = "";

        final String username = String.valueOf(editTextOne.getText());
        final String password = String.valueOf(editTextTwo.getText());
        final String usernameFromUserObject = HQ.getInstance().theUser.getUsername();
        final String passwordFromUserObject = HQ.getInstance().theUser.getPasswd();

        Log.v("XYZ_FUTURE::: ", "userpass from object: " + usernameFromUserObject + passwordFromUserObject);

        if (usernameFromUserObject == null || usernameFromUserObject.matches("")) {
            use = username;
        } else {
            use = usernameFromUserObject;
        }
        if (passwordFromUserObject == null || passwordFromUserObject.matches("")) {
            pass = password;
        } else {
            pass = passwordFromUserObject;
        }
        final String finalUse = use;
        final String finalPass = pass;

        Log.v("XYZ_FUTURE::: ", "final userpass: " + finalUse + finalPass);

        AsyncTask asyncTask = new AsyncTask() {
            @Override protected Object doInBackground(Object[] params) {
                OkHttpClient client = new OkHttpClient();

                RequestBody formBody = new FormBody.Builder()
                        .add("username", finalUse)
                        .add("password", finalPass)
                        .build();
                Request request = new Request.Builder()
                        .url("http://psba.pixelbeard.co/api/users/login")
                        .addHeader("Content-type", "application/x-www-form-urlencoded")
                        .post(formBody)
                        .build();
                try {

                    Response response = client.newCall(request).execute();
                    Log.v("XYZ_FUTURE::: ", "response from login is: " + response.toString());
                    JSONObject responseJSON = new JSONObject(response.body().string());

                    if (responseJSON.getString("success").matches("true")) {

                        String userJSONObject = responseJSON.getString("user");
                        HashMap<String, String> userHash = HQ.getInstance().jsonToMap(userJSONObject.toString());
                        Log.v("XYZ_FUTURE::: ", "user hash is: " + userHash);

                        HQ.getInstance().theUser = new User(
                                userHash.get("firstname"),
                                userHash.get("lastname"),
                                userHash.get("username"),
                                userHash.get("psnid"),
                                userHash.get("email"),
                                userHash.get("company"),
                                userHash.get("branch"),
                                userHash.get("occupation"),
                                userHash.get("points"),
                                userHash.get("avatar"),
                                userHash.get("user_id"),
                                userHash.get("modulescompleted"),
                                userHash.get("company_name"));

                        HQ.getInstance().populateUserPrefsWithLogin();

                        return "success";
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return "fail";
            }

            @Override protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                if (o.equals("success")) {
//                    Toast toastLogin = Toast.makeText(CredentialsActivity.this, "You are logged in.", Toast.LENGTH_LONG);
//                    toastLogin.show();

                    Intent intent = new Intent(CredentialsActivity.this, MainActivity.class);
                    intent.putExtra("fromLogin", true);
                    startActivity(intent);
//                    overridePendingTransition(R.anim.fade_in, R.anim.slide_out_down);

                } else {
                    slideInAlert("FAILURE", "We were unable to log you in with those details.", true, 3000);
                }
            }
        };
        asyncTask.execute();
    }

    public void psbaAPICheckUsername() {

        Log.v("XYZ_FUTURE::: ", "psbaAPICheckUsername");

        final String username = String.valueOf(editTextOne.getText());
        final String username2 = String.valueOf(editTextOneSignUp2.getText());

        AsyncTask asyncTask = new AsyncTask() {

            @Override protected Object doInBackground(Object[] params) {

                String usernayme;
                if (credState == 0) {
                    usernayme = username;
                } else {
                    usernayme = username2;
                }

                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://psba.pixelbeard.co/api/users/check/username/" + usernayme)
                        .build();
                try {
                    okhttp3.Response response = client.newCall(request).execute();
                    JSONObject testJSON = new JSONObject(response.body().string());
                    HashMap<String, String> fetchUserResponse;
                    fetchUserResponse = HQ.getInstance().jsonToMap(testJSON.toString());
                    if (fetchUserResponse.get("success").matches("false")) {
                        return "not avail";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return "avail";
            }
            @Override protected void onPostExecute(Object o) {
                if (o.equals("avail")) {
                    switch (credState) {
                        case 0:
                            editTextOne.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border_red));
                            inputIconOne.setTextColor(getResources().getColor(R.color.colorMagnesium));
                            textInputInfoOne.setTextColor(getResources().getColor(R.color.colorLava));
                            textInputInfoOne.setText(getResources().getString(R.string.username_does_not_exist));
                            break;
                        case 1:
                            break;
                        case 2:
                            Log.v("XYZ_FUTURE::: ", "case 2 check: avail");
                            editTextOneSignUp2.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border));
                            inputIconOneSignUp2.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                            textInputInfoOneSignUp2.setTextColor(getResources().getColor(R.color.colorLava));
                            textInputInfoOneSignUp2.setText("");
                            break;
                        case 3:
                            break;
                        case 4:
                            break;
                        case 5:
                            break;
                        default:
                            break;
                    }
                }

                if (o.equals("not avail")) {
                    switch (credState) {
                        case 0:
                            break;
                        case 1:
                            break;
                        case 2:
                            Log.v("XYZ_FUTURE::: ", "case 2 check: NOT avail");
                            editTextOneSignUp2.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border_red));
                            inputIconOneSignUp2.setTextColor(getResources().getColor(R.color.colorMagnesium));
                            textInputInfoOneSignUp2.setTextColor(getResources().getColor(R.color.colorLava));
                            textInputInfoOneSignUp2.setText(getResources().getString(R.string.username_not_avail));
                            break;
                        case 3:
                            break;
                        case 4:
                            break;
                        case 5:
                            break;
                        default:
                            break;
                    }
                }
            }
        };
        asyncTask.execute();
    }

    public void psbaAPICheckEmail() {

        final String email = String.valueOf(editTextOneSignUp3.getText());

        AsyncTask asyncTask = new AsyncTask() {
            @Override protected Object doInBackground(Object[] params) {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://psba.pixelbeard.co/api/users/check/email/" + email)
                        .build();
                try {
                    okhttp3.Response response = client.newCall(request).execute();
                    JSONObject testJSON = new JSONObject(response.body().string());
                    HashMap<String, String> fetchUserResponse;
                    fetchUserResponse = HQ.getInstance().jsonToMap(testJSON.toString());
                    if (fetchUserResponse.get("success").matches("false")) {
                        return "not avail";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return "avail";
            }
            @Override protected void onPostExecute(Object o) {

                if (o.equals("avail")) {

                }

                if (o.equals("not avail")) {
                    editTextOneSignUp3.setBackground(ContextCompat.getDrawable(CredentialsActivity.this, R.drawable.edit_text_border_red));
                    inputIconOneSignUp3.setTextColor(getResources().getColor(R.color.colorMagnesium));
                    textInputInfoOneSignUp3.setTextColor(getResources().getColor(R.color.colorLava));
                    textInputInfoOneSignUp3.setText(getResources().getString(R.string.email_not_avail));
                }

            }
        };
        asyncTask.execute();
    }

    public void psbaAPIGetCompanies() {
        Log.v("XYZ_FUTURE::: ", "psbaAPIGetCompanies");
        AsyncTask asyncTask = new AsyncTask() {

            @Override protected Object doInBackground(Object[] params) {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://psba.pixelbeard.co/api/companies")
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    JSONObject responseJSON = new JSONObject(response.body().string());
                    String success = responseJSON.getString("success");

                    if (success.matches("true")) {
                        JSONArray companyList = responseJSON.getJSONArray("companies");
//                        Log.v("XYZ_FUTURE::: ", "test companyList: " + companyList);

                        for (int i = 0; i < companyList.length(); i++) {
                            JSONObject individualCompany = (JSONObject) companyList.get(i);
                            HashMap<String, String> singleCoHash = HQ.getInstance().jsonToMap(individualCompany.toString());
                            Company co = new Company();
                            co.setCompanyName(singleCoHash.get("name"));
                            co.setCompanyId(singleCoHash.get("id"));
                            co.setCompanyActive(singleCoHash.get("active"));
                            co.setCompanyOrder(singleCoHash.get("order"));
                            co.setCompanyImage(singleCoHash.get("image"));
                            HQ.getInstance().companies.add(co);
//                            Log.v("XYZ_FUTURE::: ", "getCompanyNameList: " + HQ.getInstance().getCompanyNameList());
                        }

                        return "success";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return "fail";
            }
            @Override protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                if (o.equals("success")) {
//                    Toast toastLogin = Toast.makeText(CredentialsActivity.this, "got companies", Toast.LENGTH_LONG);
//                    toastLogin.show();


                } else {
//                    slideInAlert("FAILURE", "You have no network connection, please try again later.", true, 3000);
                }
            }
        };
        asyncTask.execute();
    }
}
