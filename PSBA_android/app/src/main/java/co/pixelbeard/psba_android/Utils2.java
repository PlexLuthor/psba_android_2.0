package co.pixelbeard.psba_android;

import android.content.Context;

import java.util.ArrayList;

import co.pixelbeard.psba_android.Modules.Training.Models.TrainingModule;

/**
 * Created by AE1 on 8/29/17.
 */

public class Utils2 {

    public static ArrayList<TrainingModule> generateTrainingItems(Context context) {
        ArrayList<TrainingModule> trainingItems = new ArrayList<>();
        trainingItems.add(new TrainingModule("Dog", "https://c1.staticflickr.com/1/188/417924629_6832e79c98_z.jpg?zz=1"));
        trainingItems.add(new TrainingModule("Cat", "https://c1.staticflickr.com/1/188/417924629_6832e79c98_z.jpg?zz=1"));
        trainingItems.add(new TrainingModule("Horse", "https://c1.staticflickr.com/1/188/417924629_6832e79c98_z.jpg?zz=1"));
        return trainingItems;
    }
}