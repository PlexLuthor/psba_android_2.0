package co.pixelbeard.psba_android.Modules.Training.Adapters;

import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import co.pixelbeard.psba_android.HQ;
import co.pixelbeard.psba_android.Modules.Training.Models.TrainingModule;
import co.pixelbeard.psba_android.R;
import co.pixelbeard.psba_android.TrainingItemClickListener;

/**
 * Created by AE1 on 8/29/17.
 */

public class TrainingHomeTwoAdapter extends RecyclerView.Adapter<TrainingHomeTwoAdapter.TrainingHomeTwoHolder> {

    private final TrainingItemClickListener trainingItemClickListener;
    private ArrayList<TrainingModule> trainingItems;

    public TrainingHomeTwoAdapter(ArrayList<TrainingModule> trainingItems, TrainingItemClickListener trainingItemClickListener) {
        this.trainingItems = trainingItems;
        this.trainingItemClickListener = trainingItemClickListener;
    }
    @Override public TrainingHomeTwoAdapter.TrainingHomeTwoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyc_element_training_home_two, parent, false);
        return new TrainingHomeTwoAdapter.TrainingHomeTwoHolder(view);
    }

    @Override public int getItemCount() {
        return HQ.getInstance().trainingModulesSoftware.size();
    }

    @Override public void onBindViewHolder(final TrainingHomeTwoHolder holder, int position) {

        final TrainingModule trainingItem = trainingItems.get(position);

        holder.title.setText(HQ.getInstance().trainingModulesSoftware.get(position).getName());
        holder.points.setText(HQ.getInstance().trainingModulesSoftware.get(position).getPoints() + "pts");

        // 120 dp is width of card //
        float totalQuestions = HQ.getInstance().trainingModulesSoftware.get(position).getUserProgressRefTotalQuestions();
        float numberCorrect = HQ.getInstance().trainingModulesSoftware.get(position).getUserProgressRefCorrectAnswers();;
        if (totalQuestions != 0) {
            float ratio = numberCorrect / totalQuestions;
            Log.v("XYZ_FUTURE::: ", "questions and number correct are: " + totalQuestions +"::"+ numberCorrect + "\n and ratio is: " + ratio);
            holder.progressBar.getLayoutParams().width = (int)(HQ.getInstance().dpToPx(120) * ratio);
        } else {
            holder.progressBar.getLayoutParams().width = 0;
        }
        holder.progressBar.requestLayout();

        Picasso.with(holder.itemView.getContext())
                .load("https://c1.staticflickr.com/1/188/417924629_6832e79c98_z.jpg?zz=1")//HQ.getInstance().theAvatarURL + "default-avatar.jpg")
                .fit()
                .centerCrop()
                .into(holder.trainingItemImage);
        ViewCompat.setTransitionName(holder.trainingItemImage, trainingItem.getName());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                trainingItemClickListener.onTrainingItemClick(holder.getAdapterPosition(), trainingItem, holder.trainingItemImage, "two");
            }
        });
    }

    public class TrainingHomeTwoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public CardView cardView;
        public ImageView trainingItemImage;
        public View progressBar;
        public TextView title;
        public TextView points;

        public TrainingHomeTwoHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.recyc_training_home_two_cardview);
            trainingItemImage = (ImageView) itemView.findViewById(R.id.recyc_training_home_two_image);
            progressBar = itemView.findViewById(R.id.recyc_training_home_two_progress_bar);
            title = (TextView) itemView.findViewById(R.id.recyc_training_home_two_text_title);
            points = (TextView) itemView.findViewById(R.id.recyc_training_home_two_text_points);
        }
        @Override public void onClick(View v) {
            if (v.getId() == R.id.recyc_training_home_two_cardview) {
                Log.v("XYZ", "recyc_store_main_cardview");

            } else {
                Log.v("XYZ", "NOT on card");
            }
        }
    }
}