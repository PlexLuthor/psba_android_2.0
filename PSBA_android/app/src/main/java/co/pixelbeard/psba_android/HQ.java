package co.pixelbeard.psba_android;

import android.animation.Animator;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import co.pixelbeard.psba_android.Modules.Creds.Models.Branch;
import co.pixelbeard.psba_android.Modules.Creds.Models.Company;
import co.pixelbeard.psba_android.Modules.Creds.Models.ExtraOption;
import co.pixelbeard.psba_android.Modules.Profile.Models.User;
import co.pixelbeard.psba_android.Modules.Training.Models.TrainingModule;
import co.pixelbeard.psba_android.Modules.Training.Models.UserModuleProgressModel;


public class HQ extends Application {

    private static HQ instance;
    public static String theApiURL = "http://psba.pixelbeard.co/api/";
    public static String theAvatarURL = "http://www.psba.pixelbeard.co/psba-images/uploads/user-avatars/";
    public static String theStoreLogoURL = "http://www.psba.pixelbeard.co/psba-images/uploads/store-logos/";

    public ArrayList<Company> companies;
    public ArrayList<Branch> branches;
    public ArrayList<ExtraOption> extraOptions;

    public HashMap<String, ArrayList> extrasAnswers;
    public ArrayList<HashMap<String, String>> extrasAnswersArray;
    public HashMap<String, String> extrasAnswersHash;

    public ArrayList<TrainingModule> trainingModules;
    public ArrayList<TrainingModule> trainingModulesProgress;
    public ArrayList<TrainingModule> trainingModulesSoftware;
    public ArrayList<TrainingModule> trainingModulesEssentials;
    public ArrayList<TrainingModule> trainingModulesDigitalContent;
    public ArrayList<UserModuleProgressModel> userModuleProgressArray;

    public List<String> spinnerDropDownElements;
    public User theUser;

    public int positionExtrasAdapterMain = 0;

    public Typeface sstFontLight;
    public Typeface sstFontRoman;
    public Typeface sstFontHeavy;
    public Typeface sstFontBold;
    public Typeface sstFontCondensed;
    public Typeface sstFontCondensedBd;
    public Typeface fontAwesome;

    public boolean dropChosen;

//    DBWizard dbWizard;

    public static synchronized HQ getInstance() {
        return instance;
    }

    @Override public void onCreate() {
        super.onCreate();
        instance = this;
        Log.v("XYZ_FUTURE::: ", "singleton onCreate");
        companies = new ArrayList(Arrays.asList());
        branches = new ArrayList(Arrays.asList());
        extraOptions = new ArrayList(Arrays.asList());
        spinnerDropDownElements = new ArrayList(Arrays.asList());
        trainingModules = new ArrayList(Arrays.asList());
        trainingModulesProgress = new ArrayList(Arrays.asList());
        trainingModulesSoftware = new ArrayList(Arrays.asList());
        trainingModulesEssentials = new ArrayList(Arrays.asList());
        trainingModulesDigitalContent = new ArrayList(Arrays.asList());
        userModuleProgressArray = new ArrayList(Arrays.asList());

        extrasAnswersArray = new ArrayList(Arrays.asList());
        extrasAnswers = new HashMap<>();
        extrasAnswersHash = new HashMap<>();
        extrasAnswers.put("answers", extrasAnswersArray);

        branches.add(new Branch("other", "other"));

        theUser = new User("","","","","","","","","","","","","","","","","","","","","","","","","","","","","","default-avatar.jpg");

//        dbWizard = new DBWizard(getApplicationContext());

        // fonts //
        sstFontLight = Typeface.createFromAsset(getAssets(), "fonts/SST-Light.ttf");
        sstFontRoman = Typeface.createFromAsset(getAssets(), "fonts/SST-Roman.ttf");
        sstFontHeavy = Typeface.createFromAsset(getAssets(), "fonts/SST-Heavy.ttf");
        sstFontBold = Typeface.createFromAsset(getAssets(), "fonts/SST-Bold.ttf");
        sstFontCondensed = Typeface.createFromAsset(getAssets(), "fonts/SST-Condensed.ttf");
        sstFontCondensedBd = Typeface.createFromAsset(getAssets(), "fonts/SST-CondensedBd.ttf");
        fontAwesome = Typeface.createFromAsset(getAssets(), "fonts/fontawesome-webfont.ttf");
    }


    public List<String> getCompanyNameList() {
        List<String> coNames = new ArrayList<>();
        for (int i = 0; i < companies.size(); i++) {
            coNames.add(companies.get(i).getCompanyName());
        }
        return coNames;
    }

    public List<String> getCompanyImageList() {
        List<String> coImages = new ArrayList<>();
        for (int i = 0; i < companies.size(); i++) {
            coImages.add(companies.get(i).getCompanyImage());
        }
        return coImages;
    }

    public List<String> getBranchNameList() {
        List<String> branchNames = new ArrayList<>();
        for (int i = 0; i < branches.size(); i++) {
            branchNames.add(branches.get(i).getBranchName());
        }
//        Log.v("XYZ_FUTURE::: ", "get branchnames is: " + branchNames);
        return branchNames;
    }

    public List<String> getExtrasValues(int position) {
        ExtraOption singleOptionFromPosition = extraOptions.get(position);
        List<String> valueArrayOfHashs = singleOptionFromPosition.getValues();

//        HashMap<String, String> valuesHash = new HashMap<>();
        List<String> ttttt = new ArrayList<>();
        for (int i = 0; i < valueArrayOfHashs.size(); i++) {
            String hash = valueArrayOfHashs.get(i);
            try {
                JSONObject jsonResponse = new JSONObject(hash);
                ttttt.add(jsonResponse.getString("value"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return ttttt;
    }

    public static HashMap<String, String> jsonToMap(String jsonString) throws JSONException {
        HashMap<String, String> map = new HashMap<>();
        JSONObject jObject = new JSONObject(jsonString);
        Iterator<?> keys = jObject.keys();
        while (keys.hasNext()) {
            String key = (String)keys.next();
            String value = jObject.getString(key);
            map.put(key, value);
        }
        return map;
    }


    public void populateUserPrefsWithLogin() {
        SharedPreferences userPrefs = getSharedPreferences("theUser", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = userPrefs.edit();
        editor.putBoolean("isLoggedIn", true);
        editor.putString("firstname", theUser.getFirstname());
        editor.putString("lastname", theUser.getLastname());
        editor.putString("username", theUser.getUsername());
        editor.putString("psnid", theUser.getPsnid());
        editor.putString("email", theUser.getEmail());
        editor.putString("company_id", theUser.getCompanyId());
        editor.putString("branch_name", theUser.getBranchName());
        editor.putString("occupation", theUser.getOccupation());
        editor.putString("points", theUser.getPoints());
        editor.putString("avatar", theUser.getAvatar());
        editor.putString("user_id", theUser.getUser_id());
        editor.putString("modulescompleted", theUser.getModulescompleted());
        editor.putString("company_name", theUser.getCompanyName());
        editor.apply();
    }

    public void populateTheUserObject() {
        SharedPreferences userPrefs = getSharedPreferences("theUser", Context.MODE_PRIVATE);
        theUser.setFirstname(userPrefs.getString("firstname", ""));
        theUser.setLastname(userPrefs.getString("lastname", ""));
        theUser.setUsername(userPrefs.getString("username", ""));
        theUser.setPsnid(userPrefs.getString("psnid", ""));
        theUser.setEmail(userPrefs.getString("email", ""));
        theUser.setCompanyId(userPrefs.getString("company_id", ""));
        theUser.setBranchName(userPrefs.getString("branch_name", ""));
        theUser.setOccupation(userPrefs.getString("occupation", ""));
        theUser.setPoints(userPrefs.getString("points", ""));
        theUser.setAvatar(userPrefs.getString("avatar", ""));
        theUser.setUser_id(userPrefs.getString("user_id", ""));
        theUser.setModulescompleted(userPrefs.getString("modulescompleted", ""));
        theUser.setCompanyName(userPrefs.getString("company_name", ""));
    }


    public void editProfilePopulatePrefs() {
        SharedPreferences userPrefs = getSharedPreferences("theUser", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = userPrefs.edit();
        editor.putString("username", theUser.getUsername());
        editor.putString("psnid", theUser.getPsnid());
        editor.putString("email", theUser.getEmail());
    }

    public void logoutUser(Context context) {
        SharedPreferences userPrefs = getSharedPreferences("theUser", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = userPrefs.edit();
        editor.putBoolean("isLoggedIn", false);
        editor.apply();

        boolean isLoggedIn = userPrefs.getBoolean("isLoggedIn", false);
        Log.v("XYZ_FUTURE::: ", "isLoggedIn" + isLoggedIn);
        theUser.setFirstname("");
        theUser.setLastname("");
        theUser.setUsername("");
        theUser.setPsnid("");
        theUser.setEmail("");
        theUser.setPoints("");
        theUser.setAvatar("");
        theUser.setUser_id("");
        theUser.setModulescompleted("");
        theUser.setCompanyName("");
        setUserCompanyInfoToNil();
    }

    public void setUserCompanyInfoToNil() {
        theUser.setBranch_id("");
        theUser.setBranchName("");
        theUser.setCompanyName("");
        theUser.setCompanyId("");
        theUser.setOccupation("");
        theUser.setAddress1("");
        theUser.setAddress2("");
        theUser.setCountry("");
        theUser.setTown("");
        theUser.setPostCode("");
        theUser.setExtras("");
    }

    public void customAnimationForButton(final View view) {
        view.animate().alpha(0.0f).setDuration(100).setListener(new Animator.AnimatorListener() {
            @Override public void onAnimationStart(Animator animation) {}
            @Override public void onAnimationEnd(Animator animation) {
                view.animate().alpha(1.0f);
//                view.setBackgroundColor(getResources().getColor(R.color.colorClear));
            }
            @Override public void onAnimationCancel(Animator animation) {}
            @Override public void onAnimationRepeat(Animator animation) {}
        });
    }

    public int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }
    public int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public void categorizeTrainingModules() {
        Log.v("XYZ_FUTURE::: ", "CATEGORIZING");
        trainingModulesSoftware.clear();
        trainingModulesEssentials.clear();
        trainingModulesDigitalContent.clear();
        for (int i = 0; i < trainingModules.size(); i++) {

//            if (trainingModules.get(i).getUserProgressRefCorrectAnswers()


            String cat = trainingModules.get(i).getCategory_name();
            switch (cat) {
                case "Software":
                    trainingModulesSoftware.add(trainingModules.get(i));
                    break;
                case "Essentials":
                    trainingModulesEssentials.add(trainingModules.get(i));
                    break;
                case "Digital Content":
                    trainingModulesDigitalContent.add(trainingModules.get(i));
                    break;
                default:
                    break;
            }
        }
    }

}
