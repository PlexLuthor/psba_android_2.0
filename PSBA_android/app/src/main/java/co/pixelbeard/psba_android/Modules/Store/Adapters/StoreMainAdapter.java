package co.pixelbeard.psba_android.Modules.Store.Adapters;

import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import co.pixelbeard.psba_android.Modules.Store.Models.ProductItem;
import co.pixelbeard.psba_android.ProductItemClickListener;
import co.pixelbeard.psba_android.R;

/**
 * Created by aaroneckhart on 8/22/17.
 */

public class StoreMainAdapter extends RecyclerView.Adapter<StoreMainAdapter.StoreMainHolder> {

    private final ProductItemClickListener productItemClickListener;
    private ArrayList<ProductItem> productItems;

    public StoreMainAdapter(ArrayList<ProductItem> productItems, ProductItemClickListener productItemClickListener) {
        this.productItems = productItems;
        this.productItemClickListener = productItemClickListener;
    }
    @Override public StoreMainHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyc_element_main_store_object, parent, false);
        return new StoreMainAdapter.StoreMainHolder(view);
    }

    @Override public int getItemCount() {
        return 8;
    }

    @Override public void onBindViewHolder(final StoreMainAdapter.StoreMainHolder holder, int position) {
        final ProductItem productItem = productItems.get(position);

        Picasso.with(holder.itemView.getContext())
                .load("https://c1.staticflickr.com/1/188/417924629_6832e79c98_z.jpg?zz=1")//HQ.getInstance().theAvatarURL + "default-avatar.jpg")
                .fit()
                .centerCrop()
                .into(holder.productImage);

        ViewCompat.setTransitionName(holder.productImage, productItem.name);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                productItemClickListener.onProductItemClick(holder.getAdapterPosition(), productItem, holder.productImage);
            }
        });


    }

    public class StoreMainHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public CardView cardView;
        public ImageView productImage;

        public StoreMainHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.recyc_store_main_cardview);
            productImage = (ImageView) itemView.findViewById(R.id.recyc_element_store_main_image);
        }
        @Override public void onClick(View v) {
            if (v.getId() == R.id.recyc_store_main_cardview) {
                Log.v("XYZ", "recyc_store_main_cardview");

            } else {
                Log.v("XYZ", "NOT on card");
            }
        }
    }
}