package co.pixelbeard.psba_android.Modules.Creds.Models;

/**
 * Created by aaroneckhart on 8/8/17.
 */

public class Branch {
    String branchId;
    String branchName;

    public Branch(String branchId, String branchName) {
        this.branchId = branchId;
        this.branchName = branchName;
    }

    public Branch() {

    }

    public void setBranchId(String id) {
        this.branchId = id;
    }
    public void setBranchName(String name) {
        this.branchName = name;
    }

    public String getBranchId() {
        return this.branchId;
    }
    public String getBranchName() {
        return this.branchName;
    }
}
