package co.pixelbeard.psba_android.Modules.Creds.Acts;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import co.pixelbeard.psba_android.R;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import pl.droidsonroids.gif.GifImageView;

public class RequestActivationCodeActivity extends AppCompatActivity {

    int heightPixels;
    int widthPixels;

    float inputHeightOnFocus;
    float inputHeightSlotOne;
    float inputHeightSlotTwo;
    float inputHeightSlotThree;
    float inputHintInX;
    float inputHintOutX;
    float inputHintAboveHeight;
    float editTextHeight;
    float editTextWidth;
    float loginButtonheight;
    float mainButtonHeight;
    float mainButtonSlot;
    float titleSlot;
    float mainButtonScaleDownValue;
    float hintTextViewWidth;
    float backButtonWidth;

    private Button buttonBack;
    private TextView textNavTitle;
    private TextView textInstructionsTitle;
    private TextView textInstructionsBody;
    private EditText editTextOne;
    private TextView textViewOne;
    private TextView inputIconOne;
    private EditText editTextTwo;
    private TextView textViewTwo;
    private TextView inputIconTwo;
    private Button buttonEnterActCode;
    private Button buttonSendRecoveryEmail;

    private Typeface sstFontLight;
    private Typeface sstFontRoman;
    private Typeface sstFontHeavy;
    private Typeface sstFontBold;
    private Typeface sstFontCondensed;
    private Typeface sstFontCondensedBd;
    private Typeface fontAwesome;

    private int editTextFocus;

    Spanned lockIcon = Html.fromHtml("&#xf023;");
    Spanned envelopeIcon = Html.fromHtml("&#xf0e0;");
    Spanned profileIcon = Html.fromHtml("&#xf007;");
    Spanned xIcon = Html.fromHtml("&#xf00d;");
    Spanned groupProfileIcon = Html.fromHtml("&#xf0c0;");
    Spanned briefcaseIcon = Html.fromHtml("&#xf0f2;");

    String lockIconString = " " + lockIcon + " ";
    String envelopeIconString = "" + envelopeIcon + " ";
    String profileIconString = " " + profileIcon + " ";
    String xIconString = " " + xIcon + " ";
    String groupProfileIconString = "" + groupProfileIcon + " ";
    String briefcaseIconString = "" + briefcaseIcon + " ";

    private RelativeLayout psbaAlert;
    private View psbaAlertOverlay;
    private GifImageView psbaAlertGif;
    private TextView psbaAlertTitle;
    private TextView psbaAlertBody;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_activation_code);

        // pixels, dpi //
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        heightPixels = metrics.heightPixels;
        widthPixels = metrics.widthPixels;

        findViews();
        setInitialValues();
        setCustomFont();
        setInitialPosition();

        setupEditTextOneListeners();
        setupEditTextTwoListeners();

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.list_slide_out);
            }
        });


        editTextOne.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE) {
                    Log.v("XYZ_FUTURE::: ", "CHECK CHECK");
                    View v = getCurrentFocus();
                    if ( v instanceof EditText) {
                        Rect outRect = new Rect();
                        v.getGlobalVisibleRect(outRect);
                        v.clearFocus();
                        editTextFocus = 0;
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }

                return false;
            }
        });
        editTextTwo.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE) {
                    Log.v("XYZ_FUTURE::: ", "CHECK CHECK");
                    View v = getCurrentFocus();
                    if ( v instanceof EditText) {
                        Rect outRect = new Rect();
                        v.getGlobalVisibleRect(outRect);
                        v.clearFocus();
                        editTextFocus = 0;
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }

                return false;
            }
        });
        buttonEnterActCode.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Intent resetPasswordIntent = new Intent(RequestActivationCodeActivity.this, ResetPasswordActivity.class);
                startActivity(resetPasswordIntent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.fade_out);
            }
        });
        buttonSendRecoveryEmail.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                psbaAPIRecoverAccountDetails();
            }
        });
    }


    public void setupEditTextOneListeners() {
        editTextOne.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v1, boolean hasFocus) {
                if (hasFocus) {
                    Log.v("XYZ_FUTURE::: ", "EditTextTwo has focus");
                    editTextFocus = 1;

                    editTextOne.setCursorVisible(true);
//                    editTextOne.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
                    textViewOne.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotOne - (inputHintAboveHeight)).translationX(-textViewOne.getWidth() * 0.5f * 0.35f);

                } else if (!hasFocus) {
                    Log.v("XYZ_FUTURE::: ", "EditTextTwo NO focus and text is: " + editTextOne.getText().toString());
                    editTextFocus = 0;

                    if (editTextOne.getText().toString().matches("")) {
                        editTextOne.setCursorVisible(false);
//                        editTextOne.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border_gray));
                        textViewOne.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotOne).translationX(inputHintInX);
                    } else {
                        editTextOne.setCursorVisible(false);
//                        editTextOne.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
                        textViewOne.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotOne - (inputHintAboveHeight)).translationX(-textViewOne.getWidth() * 0.5f * 0.35f);
                    }
                    if (editTextTwo.getText().toString().matches("")) {
                        editTextTwo.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotTwo);
                        textViewTwo.animate().scaleX(1.0f).scaleY(1.0f);
                    } else {
                        editTextTwo.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotTwo);
                        textViewTwo.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotTwo - (inputHintAboveHeight)).translationX(-textViewTwo.getWidth() * 0.5f * 0.35f);
                    }

                }
            }
        });
    }

    public void setupEditTextTwoListeners() {
        editTextTwo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v2, boolean hasFocus) {
                if (hasFocus) {
                    Log.v("XYZ_FUTURE::: ", "EditTextTwo has focus");
                    editTextFocus = 2;
//                    focusOnInput(editTextFocus);

                    editTextTwo.setCursorVisible(true);
//                    editTextTwo.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
                    textViewTwo.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotTwo - (inputHintAboveHeight)).translationX(-textViewOne.getWidth() * 0.5f * 0.35f);

                } else if (!hasFocus) {
                    Log.v("XYZ_FUTURE::: ", "EditTextTwo NO focus");
                    editTextFocus = 0;

                    if (editTextTwo.getText().toString().matches("")) {
                        editTextTwo.setCursorVisible(false);
//                        editTextTwo.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border_gray));
                        textViewTwo.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotTwo).translationX(inputHintInX);
                    } else {
                        editTextTwo.setCursorVisible(false);
//                        editTextTwo.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
                        textViewTwo.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotTwo - (inputHintAboveHeight)).translationX(-textViewTwo.getWidth() * 0.5f * 0.35f);
                    }


                    if (editTextTwo.getText().toString().matches("")) {
                        editTextTwo.setCursorVisible(false);
//                        editTextTwo.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border_gray));
                        textViewTwo.animate().scaleX(1.0f).scaleY(1.0f).translationY(inputHeightSlotTwo).translationX(inputHintInX);
                    } else {
                        editTextTwo.setCursorVisible(false);
//                        editTextTwo.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
                        textViewTwo.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightSlotTwo - (inputHintAboveHeight)).translationX(-textViewTwo.getWidth() * 0.5f * 0.35f);
                    }
                }
            }
        });
    }

    public void focusOnInput(int inputNumber) {
        EditText theInputField = null;
        TextView theHintTextView = null;
        TextView theInputIcon = null;

        EditText theOtherInputField = null;
        TextView theOtherHintTextView = null;
        TextView theOtherInputIcon = null;

        if (inputNumber == 1) {
            theInputField = editTextOne;
            theHintTextView = textViewOne;
            theInputIcon = inputIconOne;

            theOtherInputField = editTextTwo;
            theOtherHintTextView = textViewTwo;
            theOtherInputIcon = inputIconTwo;
        } else if (inputNumber == 2) {
            theInputField = editTextTwo;
            theHintTextView = textViewTwo;
            theInputIcon = inputIconTwo;

            theOtherInputField = editTextOne;
            theOtherHintTextView = textViewOne;
            theOtherInputIcon = inputIconOne;
        }

//        theOtherInputField.animate().scaleX(0.0f).scaleY(0.0f);
//        theOtherHintTextView.animate().scaleX(0.0f).scaleY(0.0f);
//        theOtherInputIcon.animate().scaleX(0.0f).scaleY(0.0f);

        theInputField.setCursorVisible(true);
        theInputField.setBackground(ContextCompat.getDrawable(RequestActivationCodeActivity.this, R.drawable.edit_text_border));
//        theInputField.animate().translationY(inputHeightOnFocus);
//        theInputIcon.animate().translationY(inputHeightOnFocus);
        theInputIcon.setTextColor(ContextCompat.getColor(RequestActivationCodeActivity.this, R.color.colorPrimaryDark));
        theHintTextView.animate().scaleX(0.65f).scaleY(0.65f).translationY(inputHeightOnFocus - (inputHintAboveHeight)).translationX(-theHintTextView.getWidth() * 0.5f * 0.35f);
    }

    public void findViews() {
        buttonBack = (Button) findViewById(R.id.req_act_code_button_back);
        textNavTitle = (TextView) findViewById(R.id.req_act_code_nav_title_creds);
        textInstructionsTitle = (TextView) findViewById(R.id.req_act_code_instructions_title);
        textInstructionsBody = (TextView) findViewById(R.id.req_act_code_instructions_body);
        editTextOne = (EditText) findViewById(R.id.req_act_code_edit_text_one);
        textViewOne = (TextView) findViewById(R.id.req_act_code_text_view_one);
        inputIconOne = (TextView) findViewById(R.id.req_act_code_input_icon_1);
        editTextTwo = (EditText) findViewById(R.id.req_act_code_edit_text_two);
        textViewTwo = (TextView) findViewById(R.id.req_act_code_text_view_two);
        inputIconTwo = (TextView) findViewById(R.id.req_act_code_input_icon_2);
        buttonEnterActCode = (Button) findViewById(R.id.req_act_code_button_enter_activation_code);
        buttonSendRecoveryEmail = (Button) findViewById(R.id.req_act_code_button_send_recovery_email);

//        psbaAlertOverlay = findViewById(R.id.psba_alert_overlay_req_actcode);
//        psbaAlert = (RelativeLayout) findViewById(R.id.psba_alert_req_actcode);
//        psbaAlertGif = (GifImageView) findViewById(R.id.psba_alert_gif_req_actcode);
//        psbaAlertTitle = (TextView) findViewById(R.id.psba_alert_title_req_actcode);
//        psbaAlertBody = (TextView) findViewById(R.id.psba_alert_body_req_actcode);
        psbaAlertOverlay = findViewById(R.id.psba_alert_overlay);
        psbaAlert = (RelativeLayout) findViewById(R.id.psba_alert);
        psbaAlertGif = (GifImageView) findViewById(R.id.psba_alert_gif);
        psbaAlertTitle = (TextView) findViewById(R.id.psba_alert_title);
        psbaAlertBody = (TextView) findViewById(R.id.psba_alert_body);
    }

    public void setInitialValues() {
        titleSlot = heightPixels * 0.005f;
        inputHeightOnFocus = heightPixels * 0.38f;
        inputHeightSlotOne = heightPixels * 0.3f;
        inputHeightSlotTwo = heightPixels * 0.41f;
        inputHeightSlotThree = heightPixels * 0.6048f;
        editTextHeight = heightPixels * 0.079f;
        editTextWidth = widthPixels - dpToPx(64);
        loginButtonheight = heightPixels * 0.069f;
        inputHintInX = widthPixels - (widthPixels - dpToPx(48));
        inputHintOutX = -dpToPx(14);
        inputHintAboveHeight = editTextHeight * 0.65f;
        mainButtonHeight = heightPixels * 0.071f;
        mainButtonSlot = heightPixels * 0.74f;
        backButtonWidth = widthPixels * 0.5f - dpToPx(38);
        mainButtonScaleDownValue = (backButtonWidth) / (widthPixels - dpToPx(64));
        hintTextViewWidth = widthPixels - (int)(inputHintInX * 2) - (dpToPx(96));


        psbaAlert.getLayoutParams().width = widthPixels - dpToPx(50);
        psbaAlert.setY(100);
        psbaAlert.setX(widthPixels);
        psbaAlert.setZ(102);

        psbaAlertOverlay.setZ(101);
    }

    public void setCustomFont() {
        // fonts //
        sstFontLight = Typeface.createFromAsset(getAssets(), "fonts/SST-Light.ttf");
        sstFontRoman = Typeface.createFromAsset(getAssets(), "fonts/SST-Roman.ttf");
        sstFontHeavy = Typeface.createFromAsset(getAssets(), "fonts/SST-Heavy.ttf");
        sstFontBold = Typeface.createFromAsset(getAssets(), "fonts/SST-Bold.ttf");
        sstFontCondensed = Typeface.createFromAsset(getAssets(), "fonts/SST-Condensed.ttf");
        sstFontCondensedBd = Typeface.createFromAsset(getAssets(), "fonts/SST-CondensedBd.ttf");
        fontAwesome = Typeface.createFromAsset(getAssets(), "fonts/fontawesome-webfont.ttf");

        textNavTitle.setTypeface(sstFontBold);
        textInstructionsTitle.setTypeface(sstFontBold);
        textInstructionsBody.setTypeface(sstFontRoman);
        editTextOne.setTypeface(sstFontRoman);
        textViewOne.setTypeface(sstFontRoman);
        inputIconOne.setTypeface(fontAwesome);
        editTextTwo.setTypeface(sstFontRoman);
        textViewTwo.setTypeface(sstFontRoman);
        inputIconTwo.setTypeface(fontAwesome);
        buttonEnterActCode.setTypeface(sstFontBold);

        psbaAlertTitle.setTypeface(sstFontBold);
        psbaAlertBody.setTypeface(sstFontRoman);
    }

    public void setInitialPosition() {
        editTextOne.getLayoutParams().height = (int) editTextHeight;
        editTextOne.requestLayout();
        editTextOne.setY(inputHeightSlotOne);

        textViewOne.getLayoutParams().height = (int) editTextHeight;
        textViewOne.getLayoutParams().width = (int) hintTextViewWidth;
        textViewOne.requestLayout();
        textViewOne.setX(inputHintInX);
        textViewOne.setY(inputHeightSlotOne);

        inputIconOne.getLayoutParams().height = (int) editTextHeight;
        inputIconOne.getLayoutParams().width = (int) editTextWidth;
        inputIconOne.requestLayout();
        inputIconOne.setText(profileIconString);
        inputIconOne.setY(inputHeightSlotOne);

        editTextTwo.getLayoutParams().height = (int) editTextHeight;
        editTextTwo.requestLayout();
        editTextTwo.setY(inputHeightSlotTwo);

        textViewTwo.getLayoutParams().height = (int) editTextHeight;
        textViewTwo.getLayoutParams().width = (int) hintTextViewWidth;
        textViewTwo.requestLayout();
        textViewTwo.setX(inputHintInX);
        textViewTwo.setY(inputHeightSlotTwo);

        inputIconTwo.getLayoutParams().height = (int) editTextHeight;
        inputIconTwo.getLayoutParams().width = (int) editTextWidth;
        inputIconTwo.requestLayout();
        inputIconTwo.setText(envelopeIconString);
        inputIconTwo.setY(inputHeightSlotTwo);

        buttonEnterActCode.setY(inputHeightSlotTwo);
    }

    private int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
    private int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static HashMap<String, String> jsonToMap(String jsonString) throws JSONException {
        HashMap<String, String> map = new HashMap<String, String>();
        JSONObject jObject = new JSONObject(jsonString);
        Iterator<?> keys = jObject.keys();

        while (keys.hasNext()) {
            String key = (String)keys.next();
            String value = jObject.getString(key);
            map.put(key, value);
        }
//        Log.v("XYZ", "json : "+jObject);
//        Log.v("XYZ", "map : "+map);
        return map;
    }

    @Override public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    editTextFocus = 0;
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }


    public void psbaAPIRecoverAccountDetails() {

        final String username = editTextOne.getText().toString();
        final String email = editTextTwo.getText().toString();

        slideInAlert("Contacting Server", "One moment while we contact the server...", true, -1);

        AsyncTask asyncTask = new AsyncTask() {
            @Override protected Object doInBackground(Object[] params) {
                OkHttpClient client = new OkHttpClient();

                RequestBody formBody = new FormBody.Builder()
                        .add("username", username)
                        .add("email", email)
                        .build();
                Request request = new Request.Builder()
                        .url("http://psba.pixelbeard.co/api/users/recover")
                        .post(formBody)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    JSONObject JSONResponse = new JSONObject(response.body().string());

                    if (JSONResponse.getString("success").matches("true")) {
                        Log.v("XYZ_FUTURE::: ", "success true");

                        return "success";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return "fail";
            }
            @Override protected void onPostExecute(Object o) {
                if (o.equals("success")) {

                    slideInAlert("SUCCESS", "Please check your email account for your recovery email.", true, 2000);

                } else {
                    slideInAlert("FAILURE", "We were unable to find an existing account with the information provided.", true, 2000);

                }
            }
        };
        asyncTask.execute();
    }


    public void slideInAlert(String title, String body, final boolean bgVisible, int timeOpen) {
        if (bgVisible == true) {
            psbaAlertOverlay.animate().alpha(0.4f);
            psbaAlertOverlay.setClickable(true);
        }

        psbaAlert.animate().translationX(widthPixels - psbaAlert.getWidth());
        psbaAlertTitle.setText(title);
        psbaAlertBody.setText(body);

        if (timeOpen != -1) {
            Handler handler = new Handler();
            Runnable r = new Runnable() {
                public void run() {
                    slideOutAlert(bgVisible);
                }
            };
            handler.postDelayed(r, timeOpen);
        }
    }

    public void slideOutAlert(boolean bgVisible) {
        if (bgVisible == true) {
            psbaAlertOverlay.animate().alpha(0.0f);
            psbaAlertOverlay.setClickable(false);
        }
        psbaAlert.animate().translationX(widthPixels);
    }

}
